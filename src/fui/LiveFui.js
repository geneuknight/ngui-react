import React from 'react';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import MainBar from '../components/MainBar';
import ChannelType from './controls/ChannelType';
import InputMapping from './controls/InputMapping';
import history from "../history"
import {getFuiPages, saveFuiPage, deleteFuiPage} from '../components/utils/httpauth';
import {makeFuiPage} from './utilsFui';
import {cl} from '../components/utils/utils';
import {globs} from '../components/utils/utils';
import {dbVals, init} from '../components/utils/http';
import {checkLogin} from '../components/utils/httpauth';

class LiveFui extends React.Component{
  /* need to load the fui pages,
  get this page
  get all the site data
  parse the z-u-c-i field
  the create a default control,
  connect it to the pid info,
  and show it.
  */
  constructor(props) {
    super(props);
    // cl(globs)
    // cl(this.props.match.params)
    this.state = {
      loaded: false,
      values: [1, 2, 3, 4, 5]
    }
    // this.setState({loaded: true});
    // cl("state set")
    // let fpPromise = getFuiPages();
    this.checkLoggedIn();
    // Promise.all([clP, fpP]).then(r=>{
    //   cl("returned");
    //   this.setState({loaded: true});
    // });
    // let selPage = "_sel";
    // var selPage;
    globs.backMsg = {type: "fui", pageType: this.props.match.params.pageType};
  }
  page = {};
  mounted = false;

  checkLoggedIn=(fpPromise)=>{
    if ((globs.token === "") || (!dbVals.gotSite)){
      checkLogin().then(r=>{
        init().then(r=>{// now, init does fuiPages, too
          if ((dbVals.siteAuthorized) &&
            (r[0].command === "gotcursite00")){
            this.loadFuiPage();
            this.setState({loaded: true});
          } else {
            this.setState({loadMsg: "Not Authorized"})
          }
        });
      });
    }else{
      this.loadFuiPage();
      this.state.loaded = true;
      // cl("set")
    }
  }

  loadFuiPage=()=>{
    // cl("load fui")
    // cl(globs)
    let pageType = this.props.match.params.pageType;
    let zuci = this.props.match.params.zuci;
    let parts = zuci.split('-');
    this.zone = parts[0]*1;
    this.unit = parts[1]*1;
    this.chan = parts[2]*1;
    this.indx = parts[3]*1;
    // cl(parts)
    // cl(zuci)
    if (globs.fuiPages[pageType] === undefined) {
      makeFuiPage(pageType);
    }
    this.page = Object.assign({}, globs.fuiPages[pageType]);
    // cl(this.page)
    this.getControlValues();
    // cl(this.page)
  }

  showTitle=()=>{
    return(
      <h3>{this.page.title}</h3>
    );
  }

  onTextChange=(e)=>{
    let indx = e.target.id.split('-')[1]*1;
    let values = this.state.values.slice(0);
    values[indx] = e.target.value;
    cl("set state")
    this.setState({values: values})
  }

  getZValue=(z, c, i)=>{
    let d = null;
    try{
      let zone = dbVals.z[z];
      try{
        let chan = zone[c];
        try{
          return chan[i];
        }catch{}
      }catch{}
    }catch{}
  }

  getControlValue=(i, z, u, c, idx, pid, type, k)=>{
    // cl([i, z, u, c, idx, pid, type, k])
    var z1, c1, i1;
    switch(type){
      case 0:
        z1 = z;
        c1 = 240 + u; // zone wide
        i1 = pid;
        break;
      case 1:// snap chan data
        z1 = z;
        c1 = 40 * u + c; // zone wide
        i1 = pid;
        break;
      case 2:// snap ecph
        z1 = z;
        c1 = 192 + 8 * u + idx; // zone wide
        i1 = pid;
        break;
      case 3:// conf ecph sensors - idx is tank
        z1 = z;
        c1 = 192 + 8 * u + idx; // zone wide
        i1 = pid;
        break;
      case 4:// zone wide, let setpoints
        z1 = z;
        c1 = 255; // zone wide
        i1 = pid;
        break;
      case 5:// sitewide
        z1 = 255;
        c1 = 0;
        i1 = pid;
        break;
      case 6:// zone type
        z1 = z;
        c1 = 255; // zone wide
        i1 = pid;
        break;
      case 7:// controller settings
        z1 = z;
        c1 = 240 + u; // zone wide
        i1 = pid;
        break;
      default:
        break;
    }
    // cl([z1, c1, i1])
    // cl(240 + "0");
    // cl(240 + u)
    // cl(typeof(u));
    let d = this.getZValue(z1, c1, i1);
    // cl([z1, c1, i1, d])
    // cl(d);
    return d;
  }

  getControlValues=()=>{
/*use the z, u, c, i, pid, k, and tableType to get the
current value*/
    // cl(this.state)
    // cl(this.page);
    let values = this.state.values.slice(0);
    this.page.controls.forEach((c, i)=>{
      // cl(c)
      let val = this.getControlValue(i, this.zone, this.unit,
        this.chan, this.indx, c.pid, c.tableType, c.k);
      values[i] = val;
      // cl(c);
    });
    // cl("set")
    if (this.mounted){
      this.setState({values: values})
    }else{
      this.state.values = values;
    }

  }

  // values=[1, 2, 3, 4, 5];

  showOneText=(c, i)=>{
    // cl(i)
    return(
      <div>
      {c.title}<br/>
      <input
      type="text"
      value={this.state.values[i]}
      onChange={this.onTextChange}
      id={"t-" + i}
      />
      </div>
    );

  }

  onChange=(e, t)=>{
    cl(e, t);
    let zuci = this.props.match.params.zuci;
    let url = "/fui/live/" + t + "/" + zuci;
    // history.pop();
    history.replace(url)
    this.page.title = e;
    // cl(this.props.match.params.pageType)
  }

  showChannelType=(c, i)=>{
    cl("chan")
    cl(this.props.match.params.pageType)
    return (
      <ChannelType key={i} cont={c} ind={i}
        type={this.props.match.params.pageType}
        zuci={this.props.match.params.zuci}
        onChange={this.onChange}/>
    );
  }

  onChange=(e)=>{
    cl(e.target.value)

  }

  showOneControl=(c, i)=>{
/* use zuci, and the information in the control
to get the parameter and put it in a text control*/
  if(c.type === "channelType") return this.showChannelType(c, i);
  cl(c.type)
  cl(c)
  let controls ={"inputMapping": InputMapping};
  let Cont = controls[c.type];
  if (Cont !== undefined){
    cl("here")
    return (
      <Cont key={i} cont={c} ind={i}
        value={this.state.values[i]}
        type={this.props.match.params.pageType}
        zuci={this.props.match.params.zuci}
        onChange={this.onChange}/>
    );
  }
    return (
      <div key={i}>{this.showOneText(c, i)}</div>
    );
  }

  showControls=()=>{
    // cl(this.page.controls);
    return(
      this.page.controls.map((c, i)=>{
        return this.showOneControl(c, i);
      })
    );
  }

  barClick = (e)=>{
    cl(e.currentTarget.id);
    let pageType = this.props.match.params.pageType ; // "zone_Fallback";
    switch (e.currentTarget.id){
      case "home":
        history.push("/sa")
        break;
      case "edit":
        history.push("/fui/" + pageType); //  + "/0-0-0-0"
        break;
      default:
        break;
    }
  }

  render(){
    // cl(this.state)
    this.mounted = true;
    if(this.state.loaded){
      return(
        <div>
        <MainBar home edit editColor={"inherit"}
        menu click={this.barClick} title="FUI Page" />
        {this.showTitle()}
        {this.showControls()}
        </div>
      );
    }else{
      return(
        <div>
        <MainBar home settings
        menu click={this.barClick} title="FUI Page" />
        {this.state.loadMsg}
        </div>
      );

    }
  }

}

/*
These are all the kinds of FUI pages, just about 60 of 'em':
zone_Stages
zone_Fallback
zone_Output
zone_History
zone_Units
zone_Irrigation
zone_Lighting
zone_Alarms
zone_Smartcool
zone_H-C_Demand
zone_Setpoints
zone_SP_Drive_to_Avg
zone_SP_Influence_Factors
zone_SP_Retractable_Greenhouse
zone_Hum_DeHum
zone_Aux_Controls
zone_Pump_Schedule
zone_Sensors
unit_Input_Mapping
unit_Analog_Temp_Mapping
unit_Irrigation_Sensor_Mapping
unit_Vent_Position_Mapping
unit_Mixing_Tanks
unit_Generic_Mapping
unit_Network_Sensors
unit_Accumulator
unit_Input_Calibration
unit_Analog_Temp_Calibration
unit_Soil_Moisture_Calibration
unit_Vent_Position_Calibration
unit_Mixing_Tank_Calibration
unit_Generic_Calibration
unit_Input_Multipliers
unit_Miscellaneous
channel_Irrigation_Scheduled
channel_Irrigation_Accumulated_Light
channel_Irrigation_Cycle
channel_Irrigation_Trigger
channel_Irrigation_Soil_Triger
channel_Irrigation_VPD
channel_CO2
channel_Light_Supplemental
channel_Light_Scheduled
channel_Light_Cyclic
channel_Light_DLI
channel_Microzone
channel_Supply_Pump
channel_Peristaltic_Batch_Pump
channel_Peristaltic_Recirculating_Pump
channel_Peristaltic_Balance_Pump
channel_Fill_Valve
channel_Vent_Roof
channel_Vent_Retractable_Roof
channel_Vent_Side_Wall
channel_Curtain
channel_Mix_Valve
channel_Proportional_Microzone
channel_PID
channel_Variable_Out

*/

export default LiveFui ;
