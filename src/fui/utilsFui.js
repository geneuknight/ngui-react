import {pb, pInd, pi, tableIds, tableBases2, offsetTables} from "../components/utils/paramIds";//p,
import {globs} from '../components/utils/utils';
import {cl} from "../components/utils/utils";

var makeParmTabs=()=>{
  // base: 0
  // cols: [id: 1, zoneIndex: 2, unitIndex: 3, unix_timestamp(created): 4, unix_timestamp(unitTime): 5, …]
  // indCnt: 0
  // indSize: 0
  // name: "snapshots"
  // type: 0
  let tableList = [];
  Object.keys(pb).forEach((e, i)=>{
    tableList[tableIds[i]]={
      name: e, base: pInd[e][0],
      id: tableIds[i],
      type: pInd[e][1],
      indSize: pInd[e][2],
      indCnt: pInd[e][3],
      cols: pi[e],
    }
  });
  return tableList;
}

var fixTypeOffset=(ofs, tabType, indSize)=>{
  // cl(ofs);
  switch(tabType){
    case 6:// controller settings
      ofs = (ofs - 1) / indSize;
      break;
    case 7:
      ofs = (ofs - 1) / indSize;
      break;
    default:
      break;
  }
  // cl(ofs);
  return ofs;
}

var paramIdToTableColumn=(pid)=>{
/*I messed up for table types 6 and 7: the controller
and zone configuration tables, since these
store values by rows, not columns*/
  for (let i = 0 ; i < tableBases2.length ; i++){
    if (tableBases2[i + 1][0] >= pid){
      // cl(tableBases2[i]);
      let tabType = tableBases2[i][1];
      let indSize = tableBases2[i][2];
      let tab = tableIds[i];
      let base = tableBases2[i][0];
      let ofs = pid - base; // (pid < 1024) ? pid :
      let ofsTab = offsetTables[i];
      let offsetKeys = Object.keys(ofsTab);
      ofs = fixTypeOffset(ofs, tabType, indSize);
      // cl([pid, tab])
      // cl(offsetKeys)
      // cl(ofsTab)

      for (let j = 0 ; j < offsetKeys.length ; j++){
        if (ofsTab[offsetKeys[j]]*1 === ofs){
          // cl(tab, offsetKeys[j])
          return [tab, offsetKeys[j]];
        }
      }
      return [-1, -1];
    }
  }
}

var makeFuiPage=(pageType)=>{
  // cl(globs.fuiPages)
  let page = {
    controls: [],
    description: "Insert Description Here",
    name: "Insert Name Here",
    title: "Insert Title for " + pageType + " Here",
    type: pageType,
  }
  globs.fuiPages[pageType] = page;
  // cl(pageType)
}

export {makeParmTabs, paramIdToTableColumn, makeFuiPage}
