import React from 'react';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
// import ShowArray from './ShowArray00';
// import SiteArrayMenu from './SiteArrayMenu';
 import {cl, globs} from '../../components/utils/utils';
// import {dbVals, init} from './utils/http';
// import {checkLogin, saveConfigs} from './utils/httpauth';
// import {p, pi} from './utils/paramIds';
// import MainBar from './MainBar';
// import history from "../history"

class ChannelType extends React.Component{


  constructor(props) {
    super(props);
    this.state={
      type: props.type,
    }
  }

  makeChannelTypeSelect=()=>{
    let chTypes = [{id: "0", t: "channel_None", tx: "None"},
{id: "10", t: "channel_On_Off", tx: "On Off"},
{id: "20", t: "channel_Irrigation_Scheduled", tx: "Irrigation Scheduled"},
{id: "21", t: "channel_Irrigation_Accumulated_Light", tx: "Irrigation Accumulated Light"},
{id: "22", t: "channel_Irrigation_Cycle", tx: "Irrigation Cycle"},
{id: "23", t: "channel_Irrigation_Trigger", tx: "Irrigation Trigger"},
{id: "24", t: "channel_Irrigation_Soil_Trigger", tx: "Irrigation Soil Trigger"},
{id: "25", t: "channel_Irrigation_VPD", tx: "Irrigation VPD"},
{id: "30", t: "channel_CO2", tx: "CO2"},
{id: "40", t: "channel_Light_Supplemental", tx: "Light Supplemental"},
{id: "41", t: "channel_Light_Scheduled", tx: "Light Scheduled"},
{id: "42", t: "channel_Light_Cyclic", tx: "Light Cyclic"},
{id: "43", t: "channel_Light_DLI", tx: "Light DLI"},
{id: "50", t: "channel_Microzone", tx: "Microzone"},
{id: "60", t: "channel_Supply_Pump", tx: "Supply Pump"},
{id: "61", t: "channel_Peristaltic_Recirculating_Pump", tx: "Peristaltic Recirculating Pump"},
{id: "62", t: "channel_Peristaltic_Batch_Pump", tx: "Peristaltic Batch Pump"},
{id: "63", t: "channel_Peristaltic_Balance_Pump", tx: "Peristaltic Balance Pump"},
{id: "70", t: "channel_Fill_Valve", tx: "Fill Valve"},
{id: "80", t: "channel_Vent_Roof", tx: "Vent Roof"},
{id: "81", t: "channel_Vent_Retractable_Roof", tx: "Vent Retractable Roof"},
{id: "82", t: "channel_Vent_Side_Wall", tx: "Vent Side Wall"},
{id: "90", t: "channel_Curtain", tx: "Curtain"},
{id: "100", t: "channel_Mix_Valve", tx: "Mix Valve"},
{id: "110", t: "channel_Proportional_Microzone", tx: "Proportional Microzone"},
{id: "120", t: "channel_PID", tx: "PID"},
{id: "180", t: "channel_Variable_Out", tx: "Variable Out"},]
return chTypes.map((c, i)=>{
  // cl(c.t)
        return(
            <MenuItem key={i} value={c.t}>{c.tx}</MenuItem>
        )
    });


  }

  onChange=(e)=>{
    cl("change")
    // cl(e.target.value)
    let type = e.target.value;
    this.setState({type: e.target.value})
    let title = "Insert Title for " + e.target.value + " Here";
    cl(globs.fuiPages)
    if (globs.fuiPages[type] !== undefined){
      title = globs.fuiPages[type].title;
      cl("got " + title)
    }
    cl(title)
    this.props.onChange(title, type);
  }

  render(){
    cl(this.props.type)
    return(
      <div style={{margin: 20}}>
        <FormControl className={"formControl"}>
          <InputLabel htmlFor="age-simple">Channel&nbsp;Type</InputLabel>
          <Select
            value={this.state.type}
            onChange={this.onChange}
            inputProps={{
              name: 'Mixing Tank',
              id: 'mix_tank_select',
            }}
            >
            {this.makeChannelTypeSelect()}
          </Select>
        </FormControl>
      </div>
    );
  }
}


  export default ChannelType ;
