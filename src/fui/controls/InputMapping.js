import React from 'react';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
 import {cl, globs} from '../../components/utils/utils';

class InputMapping extends React.Component{


  constructor(props) {
    super(props);
    this.state={
      type: props.type,
      value: props.value,
    }
  }

  makeInputMappingSelect=()=>{
    let inputs = [{t: "NONE", v: 0},
    {t: "ANALOG SS1", v: 1},
    {t: "ANALOG SS2", v: 2},
    {t: "ANALOG 1", v: 3},
    {t: "ANALOG 2", v: 4},
    {t: "ANALOG 3", v: 5},
    {t: "ANALOG 4", v: 6},
    {t: "ANALOG 5", v: 7},
    {t: "ANALOG 6", v: 8},
    {t: "ANALOG 7", v: 9},
    {t: "ANALOG 8", v: 10},
    {t: "FDIN 1", v: 11},
    {t: "FDIN 2", v: 12},
    {t: "FDIN 3", v: 13},
    {t: "DIGITAL 1", v: 14},
    {t: "DIGITAL 2", v: 15},
    {t: "DIGITAL 3", v: 16},
    {t: "DIGITAL 4", v: 17},
    {t: "DIGITAL 5", v: 18},
    {t: "DIGITAL 6", v: 19},
    {t: "DIGITAL 7", v: 20},
    {t: "DIGITAL 8", v: 21},
    {t: "SOFTWARE", v: 22},
    {t: "REMOTE", v: 23},
    {t: "EXB ANALOG 1", v: 24},
    {t: "EXB ANALOG 2", v: 25},
    {t: "RS232 1", v: 26},
    {t: "SENSMOD BKUP", v: 27},
    {t: "ZONE 1", v: 100},
    {t: "ZONE 2", v: 101},
    {t: "ZONE 3", v: 102},
    {t: "ZONE 4", v: 103},
    {t: "ZONE 5", v: 104},
    {t: "ZONE 6", v: 105},
    {t: "ZONE 7", v: 106},
    {t: "ZONE 8", v: 107},
    {t: "ZONE 9", v: 108},
    {t: "ZONE 10", v: 109},
    {t: "ZONE 11", v: 110},
    {t: "ZONE 12", v: 111},
    {t: "ZONE 13", v: 112},
    {t: "ZONE 14", v: 113},
    {t: "ZONE 15", v: 114},
    {t: "ZONE 16", v: 115},
    {t: "ZONE 17", v: 116},
    {t: "ZONE 18", v: 117},
    {t: "ZONE 19", v: 118},
    {t: "ZONE 20", v: 119},
    {t: "ZONE 21", v: 120},
    {t: "ZONE 22", v: 121},
    {t: "ZONE 23", v: 122},
    {t: "ZONE 24", v: 123},
    {t: "ZONE 25", v: 124},
    {t: "ZONE 26", v: 125},
    {t: "ZONE 27", v: 126},
    {t: "ZONE 28", v: 127},
    {t: "ZONE 29", v: 128},
    {t: "ZONE 30", v: 129},
    {t: "ZONE 31", v: 130},
    {t: "ZONE 32", v: 131}];
    return inputs.map((c, i)=>{
      return(
        <MenuItem key={i} value={c.v}>{c.t}</MenuItem>
      )
    });
  }

  onChange=(e)=>{
    cl(e.target.value)
    this.setState({value: e.target.value})

  }

  render(){
    cl(this.props)
    return(
      <div style={{margin: 10}}>
        <FormControl
        style={{minWidth: 150}}
        >
          <InputLabel htmlFor="age-simple">{this.props.cont.title}</InputLabel>
          <Select
            value={this.state.value*1}
            onChange={this.onChange}
            inputProps={{
              name: 'Mixing Tank',
              id: 'mix_tank_select',
            }}
            >
            {this.makeInputMappingSelect()}
          </Select>
        </FormControl>
      </div>
    );
  }

}


  export default InputMapping ;
