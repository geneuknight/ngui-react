import React from 'react';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import MainBar from '../components/MainBar';
import history from "../history"
import {getFuiPages, saveFuiPage, deleteFuiPage} from '../components/utils/httpauth';
import {cl} from '../components/utils/utils';
import {globs} from '../components/utils/utils';

class EditFui extends React.Component{
  constructor(props) {
    super(props);
    // cl(props.match.params.pageType)
    // cl(globs.fuiPages);
    this.state = {
      loaded: false,
      loadMsg: "loading",
      title: "",
      controls: [],
      // controls: ["one", "two", "three"],
      selControl: -1,
    };
    let gfp = getFuiPages().then(r=>{
      // cl(r);
      // let arr = [];
      // r.pages.forEach(p=>{
      //   arr[p.type] = p;
      // });
      // cl(arr);
      let page = r[props.match.params.pageType];
      let controls = page.controls.slice(0);// a copy
      // cl(controls.length)
      // cl(controls)
      this.setState({
        page: page,
        title: page.title,
        controls: controls,
        loaded: true})
      // cl(page.controls)
    });

  }

  onChange=(e)=>{
    let id = e.target.id;
    let val = e.target.value;
    switch(id){
      case "title":
      this.setState({title: val})
      break;
      default:
      break;
    }
    // let cmds={title: this.getTitle(id)}
    // cmds
    // cl(e.target.id)

  }

  showOneText=(lab, id, place, val, multi)=>{
    return(
      <div style={{margin: 10}}>
        <TextField
          label={lab}
          id={id}
          value={val}
          multiline={multi}
          onChange={this.onChange}
          placeholder={place}
        />
      </div>
    );
  }

  createControl=()=>{
    // cl("docreate")
    let url = "/fui/controledit/" + this.props.match.params.pageType;
    history.push(url);
  }

  deleteControl=()=>{
    cl("del")
    if(this.state.selControl >= 0){
      let n = this.state.selControl;
      let controls = this.state.controls.slice(0);
      controls.splice(n, 1);
      cl(controls)
      this.setState({controls: controls})
    }
    cl(this.state.selControl)
  }

  saveCurPage=()=>{
/* put the title and the controls into the globs page structure,
and save that page to the db*/
    let key = this.props.match.params.pageType;
    let page = Object.assign({}, this.state.page);
    page.controls=this.state.controls;
    page.title = this.state.title;
    globs.fuiPages[key] = page;
    this.setState({
      page: page,
    })
    saveFuiPage(key, page);
  }

  upControl=()=>{
    let sel = this.state.selControl;
    let controls = this.state.controls.slice(0);
    if ((sel >= 1) && (sel < controls.length)){
      let tmp = controls[sel];
      controls[sel] = controls[sel-1];
      controls[sel-1] = tmp;
      this.setState({
        controls: controls,
        selControl: sel-1,
      })
    }
  }

  downControl=()=>{
    let sel = this.state.selControl*1;
    let controls = this.state.controls.slice(0);
    if ((sel >= 0) && (sel < controls.length - 1)){
      let tmp = controls[sel];
      controls[sel] = controls[sel+1];
      controls[sel+1] = tmp;
      this.setState({
        controls: controls,
        selControl: sel+1,
      })
    }
  }

  onClick=(e)=>{
    switch(e){
      case "Save":
        // cl(this.state)
        globs.backMsg = {type: "fui", pageType: this.props.match.params.pageType};
        this.saveCurPage();
        history.goBack();
        break ;
      case "Cancel":
        globs.backMsg = {type: "fui", pageType: this.props.match.params.pageType};
        history.goBack();
        break;
      case "Edit":
        let sel = this.state.selControl;
        if ((sel >= 0) && (sel < this.state.controls.length)){
          let url = "/fui/controledit/" + this.props.match.params.pageType +
          "/" + this.state.selControl;
          history.push(url);
        }
        // cl(url)
        break;
      case "Create":
        this.createControl();
        break
      case "Delete":
        this.deleteControl();
        break;
      case "Up":
        this.upControl();
        break;
      case "Down":
        this.downControl();
        break;
      default:
        break;
    }
    // cl(e);
  }

  showButton=(name)=>{
    return(
      <Button onClick={
        ()=>this.onClick(name)}
      style={{margin: 5}}
      variant="contained" color="primary" className={"button"}>
        {name}
      </Button>
    );
  }

  onControlClick=(e)=>{
    // cl(e.target.id)
    this.setState({selControl: e.target.id})
  }

  showControls=()=>{
    return(
      <>
      <h3>Controls</h3>
      <ul style={{listStyleType: "none"}}>
      {this.state.controls.map((c, i)=>{
        // cl(c);
        let col = (i===this.state.selControl*1) ? "lightblue" : "white";
        if ((c.name === undefined) || (c.name === "")) c.name = "none";
        return (
          <li
          id={i}
          style={{cursor: "pointer",
          backgroundColor: col}}
          onClick={this.onControlClick}
          key={i}>{c.name}</li>
        );
      })}
      </ul>
      </>
    );
  }

  barClick = (e)=>{
    cl(e.currentTarget.id);
    switch (e.currentTarget.id){
      case "home":
        history.push("/sa")
        break;
      default:
        break;
    }
  }

  render(){
    // cl(this.state)
    if(this.state.loaded){
      return(
        <div>
        <MainBar home settings
        menu click={this.barClick} title="FUI Page Editor" />
        <div>
        {this.showOneText("Title", "title", "Enter Title", this.state.title, false)}
        {this.showControls()}
          <div style={{margin: 5}}>{this.state.message}</div>
          <table><tbody>
          <tr>
            <td>{this.showButton("Create")}</td>
            <td>{this.showButton("Delete")}</td>
            <td>{this.showButton("Edit")}</td>
          </tr>
          <tr>
            <td>{this.showButton("Up")}</td>
            <td>{this.showButton("Down")}</td>
            <td></td>
          </tr>
          <tr>
            <td>{this.showButton("Save")}</td>
            <td>{this.showButton("Cancel")}</td>
            <td></td>
          </tr>
          </tbody></table>
        </div>
        </div>
      );
    }else{
      return(
        <div>
        <MainBar home settings 
        menu click={this.barClick} title="FUI Page Selector" />
        {this.state.loadMsg}</div>
      );
    }
  }

}

export default EditFui ;
