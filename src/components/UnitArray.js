import React from 'react';
import ShowArray from './ShowArray00';
import SiteArrayMenu from './SiteArrayMenu';
import {cl, globs} from './utils/utils';
import {dbVals, init} from './utils/http';
import {checkLogin, saveConfigs} from './utils/httpauth';
import MainBar from './MainBar';
import history from "../history"

class UnitArray extends React.Component{


  constructor(props) {
    super(props);
//     cl(props);
    this.state={
      popUps: [],
      loaded: false,
      channelBase: 0,
      chans: [],
//       fields: ["barometricPressure", "co2", "coolSetpoint", "dehumidifySetpoint", "differentialPressure", "heatSetpoint", "highAlarm", "highInTemperatureAlarm", "humidifySetpoint", "humidityStage", "inHumidity", "inLight", "inTemperature", "inTemperatureSensorAlarm", "lowAlarm", "lowInTemperatureAlarm", "outHumidity", "outLight", "outTemperature", "rain", "snow", "temperatureStage", "windDirection", "windSpeed"],
      fields: ["Zone", "Name", "InTemp", "InHum", "InLight",
"InCO2", "OutLight", "OutTemp", "OutHum", "WindSpd", "WindDir",
"BaroPres", "HeatSP", "CoolSP", "HumSP", "DeHumSP", "TempStg",
"HumStg", "LoAlarm", "HiAlarm", ],

//       cols: ["zone"],
      rows: [],
      loadMsg: "loading",
    }
//     if(globs.token === "")
    checkLogin().then(r=>{
      if (!r) history.push("/li");
    });
    init().then(r=>{// loads site, and user info
      // cl(r);
      // if (r[0].command === "gotcursite00"){
      if ((dbVals.siteAuthorized) &&
        (r[0].command === "gotcursite00")){
        this.setState({loaded: true});
        let rows = globs.userConfigs.zoneViewColumns[props.match.params.id] ;
        if (rows === undefined) rows = ["Zone"];
//         cl(rows);
        let zone = props.match.params.zid;
        let unit = props.match.params.uid;
        this.setState({
          rows: rows,
          chans: this.getZoneChannels(zone, unit),

        });

      } else {
        this.setState({loaded: false,
          loadMsg: "Not Authorized"})
      }
        // this.setState({loaded: true});
//         cl(globs.userConfigs.zoneViewColumns[props.match.params.id]);
//         cl(globs.userConfigs.zoneViewColumns[props.match.params.id]);

//         cl(dbVals);
    })
//     cl(props.match.params.id)
  }

  getZoneValues(zoneId){// this is duplicated in SiteArray.js
    let zo = dbVals.zones[zoneId];
    let me = zo.measures;
    let zm = zo.units[0];
    let ss = zm.snapshots;
    // cl(zo);
    // cl(zm);
    // cl(ss);
    // cl(me);
    // cl(dbVals);
    let vals = {
      "Zone": {v: zoneId, u: ""},
      "Name": {v: zo.name, u: ""},
      "InTemp": {v: ss.inTemperature, u: me.tempUnits},
      "InHum": {v: ss.inHumidity, u: "pcnt"},
      "InLight": {v: ss.inLight, u: me.lightUnits},
      "InCO2": {v: ss.co2, u: "PPM"},
      "OutLight": {v: ss.outLight, u: me.lightUnits},
      "OutTemp": {v: ss.outTemperature, u: me.tempUnits},
      "OutHum": {v: ss.outHumidity, u: "pcnt"},
      "WindSpd": {v: ss.windSpeed, u: me.windUnits},
      "WindDir": {v: ss.windDirection, u: "degr"},
      "BaroPres": {v: ss.barometricPressure, u: "mbar"},
      "HeatSP": {v: ss.heatSetpoint, u: me.tempUnits},
      "CoolSP": {v: ss.coolSetpoint, u: me.tempUnits},
      "HumSP": {v: ss.humidifySetpoint, u: "pcnt"},
      "DeHumSP": {v: ss.dehumidifySetpoint, u: "pcnt"},
      "TempStg": {v: ss.temperatureStage, u: ""},
      "HumStg": {v: ss.humidityStage, u: ""},
      "LoAlarm": {v: ss.lowAlarm, u: ""},
      "HiAlarm":  {v: ss.highAlarm, u: ""}
    }
    return vals;
  }

  getZoneChannels(zoneId, unitId){
    let zo = dbVals.zones[zoneId];
//     cl(zoneId);
//     cl(zo);
    let un = zo.units[unitId];
    // cl(un);
    let ret = [];
    // try{
      un.channels.forEach((c, i)=>{
  //       if(parseInt(c.channelType) !== 0){
        if(parseInt(c.used) !== 0){
  //         cl(c);
          ret[i] = {
            id: i,
            name: c.channelName,
            type: c.channelType,
            position: c.position,
            relay: c.relay
          };
        }
      });
    // }catch{}
//     this.setState({chans: ret});
//       cl(ret);
    return ret;
//     cl(ret);
  }

  makeZoneArr =z=>{
/* this needs to be changed to remove the channels,
and add the units*/
    let vals = this.getZoneValues(z);
    let retArr = [];
    retArr.push(["Name", "Value"]);
//     cl(this.state.rows);
//     cl(vals);
    // this.state.rows.forEach(row=>{
    //   retArr.push([{v: row, u:""}, vals[row]]);
    // });
//     let chs = this.getZoneChannels(z);
//     this.setState({channelBase: retArr.length});
    this.state.chans.forEach(ch=>{
      retArr.push(
        [{v:ch.name, u:""},
        {v:ch.position, u:""},]
      );
    });
//     cl(chs);
//     cl(retArr);
    return retArr;
  }

  myPopUp = (e, loc, param) => {
    // cl(this.popupLoc)
//     cl(param);// 2|0: row, col
    return(
      <div key={e}
      style={{
        position: "absolute",
        left: loc.x,
        top: loc.y,
        backgroundColor: "#80FFFF",
      }}
      onClick={this.delPop}
      >
      <SiteArrayMenu
        fields={this.state.fields}
        col={+param[0]}
        cols={this.state.rows}
        sel={this.contextMenuCmd}
      />
      </div>
    )
  }

  showPops = e=>{
    return (
      this.state.popUps.map((p, i)=>p.f(i, p.l, p.p))
    )
  }

  popupPushPop=e=>{
    let pops = this.state.popUps.slice(0);
    if (e !== undefined){
      pops.push(e);
    } else {
      pops.pop();
    }
    this.setState({popUps: pops})
  }

  addPop = e=>{
    this.popupPushPop(e);
  }

  delPop = e=>{
    this.popupPushPop();
  }

  addCol(loc, newCol){
    cl(this.state);
//     return;
    let retCols = [];
//     loc = parseInt(loc);
    this.state.rows.forEach((c, i)=>{
      if(newCol==="delete"){
        if (i !== loc){
          retCols.push(c);
        }
      }else{
        retCols.push(c);
        if (i === loc){
          retCols.push(newCol);
        }

      }
//       if(!(newCol==="delete")){
//       }
    })
//     retCols.push(newCol);
    cl(retCols);
    this.setState({rows: retCols})
//     return
    if (globs.userConfigs.zoneViewColumns === undefined){
      globs.userConfigs.zoneViewColumns = [];
    }
    globs.userConfigs.zoneViewColumns[this.props.match.params.id] = retCols;
    cl(globs.userConfigs);
//     return
    saveConfigs();
//     cl(retCols);
//    saveConfigs({Post: {configs: ["Zone"]} }); from test
//     saveConfigs({Post: {configs: retCols}})
    // cl(retCols);
  }

  contextMenuCmd=e=>{
    // cl(e)
    let cmds = e.split("|");
//     cl(cmds);
    this.addCol(+cmds[0], cmds[1]);

    // if (cmds[1]==="delete"){
    //
    // } else {
    //
    // }

  }

  contextMenu=e=>{
    // cl("context menu")
    // cl(e.target.id); // "d0d2"
    // this.popupLoc = {x: e.clientX, y: e.clientY};
    this.addPop({
      f: this.myPopUp,
      l: {x: e.clientX, y: e.clientY},
      p: e.target.id,
    });
  }

  selRow = e=>{
//     cl(e.target.id);
//     return;
    let skip = this.state.rows.length;
    let zone = this.props.match.params.zid
    let unit = this.props.match.params.uid
    let coords = e.target.id.split('|');
    let chan = coords[0];
    let url = "/ch/" + zone + "-" + unit + "-" + chan;
    // cl(url)
    history.push(url);
//    <Route path="/ch/:zuc" exact component={Channel} />

    if (coords[0] >= skip){
      let ch = this.state.chans[coords[0] - skip];
  //     cl(ch);
      // history.push(`/co/z/${this.props.match.params.id}/c/${ch.id}`);

    }
//     cl(skip);
//     cl(e);

// http://l4ol.com:3000
  }

  showZoneTitle=()=>{
    let unitNames = ["Master", "Slave1", "Slave2", "Slave3"];
    let z = this.props.match.params.zid;
    let u = this.props.match.params.zid;
    let name = dbVals.zones[z].name;
    let ms = unitNames[u];
    let type = dbVals.zones[z].units[u].version;
    let title = name + " " + ms + ": " + type;
    // cl(title)
    // let u =
    // cl(dbVals.zones[z].units[u])

    return(
      <h3>{title}</h3>
    );
  }

  showZoneInfo = e=>{// actually, unit info, now
    let z = this.props.match.params.zid;
    let u = this.props.match.params.zid;
    // cl(z, u)
    this.getZoneChannels(z, u)
    let arr = this.makeZoneArr(z);
    return(
      <div>
      {this.showPops()}
      <ShowArray arr={arr} popsO={this.state.popUps}
        pops={{add: this.addPop, del: this.delPop}}
        leftContextMenu={this.contextMenu}
        selRow={this.selRow}
        />
      </div>
    )
  }

  barClick = (e)=>{
//     cl(e.currentTarget.id);
    switch (e.currentTarget.id){
      case "home":
        history.push("/sa")
        break;
      case "settings":
        cl(this.props.match.params); // zone is in id
        history.push('/zs/z/' + this.props.match.params.id + '/settings');// /zs/z/0/stages
        break;
      default:
        break;
    }
  }

  render(){
    // cl(this.props)
    // cl(this.props.match.params.id)
    if(this.state.loaded){
      return(
        <div>
        <MainBar home unitSettings
        zone={this.props.match.params.zid}
        unit={this.props.match.params.uid}
        menu click={this.barClick} title="Unit Array" />
        {this.showZoneTitle()}
        {this.showZoneInfo()}
        </div>
      )
    } else {
      return(<div>
        <MainBar home settings
        menu click={this.barClick} title="Unit Array" />
        {this.state.loadMsg}
      </div>)
    }
  }
}


  export default UnitArray ;
