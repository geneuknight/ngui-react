import React from 'react';

import {globs} from './utils/utils';
// import {cl} from './utils/utils';
// import {dispOurDate, dispOurTime} from './utils';
class Floater extends React.Component{
  constructor(props){
    // parser("(2 + 3) * (6 + 4)");
    super(props);
    // console.log(globs);
    globs.floater.push(this);
    // cl(globs.floater);
    this.state={
      time: props.time,
      mode: "blank",
      params: null,
    }
  }

  setMode(mode){

  }

  redSquare(){
    return(
      <div style={{
        width: 100,
        height: 100,
        left: 100,
        backgroundColor: "red",
        position: "absolute",
      }}>floater</div>
    )
  }

  blank(){
    return null;
  }

  render(){
    // cl(this.state.mode);
    switch(this.state.mode){
      case "blank": return this.blank();
      case "redSquare":
        return this.redSquare();
      default:
        return null;
    }
  }
}
  export default Floater ;
