import React from 'react';
import ShowArray from './ShowArray00';
import SiteArrayMenu from './SiteArrayMenu';
import {cl, globs} from './utils/utils';
import {dbVals, init} from './utils/http';
import {checkLogin, saveConfigs} from './utils/httpauth';
import {p, pi} from './utils/paramIds';
import MainBar from './MainBar';
import history from "../history"

class Channel extends React.Component{


  constructor(props) {
    super(props);
    this.state={
      loaded: false,
      loadMsg: "loading"
    }
    checkLogin().then(r=>{
      if (!r) history.push("/li");
    });
    cl("check")
    init().then(r=>{// loads site, and user info
      // cl("init")
      // cl(r)
      if ((dbVals.siteAuthorized) &&
        (r[0].command === "gotcursite00")){
          // cl("set")
        this.setState({loaded: true});
        this.getChannelInfo();
        let rows = globs.userConfigs.zoneViewColumns[props.match.params.id] ;
        if (rows === undefined) rows = ["Zone"];
        this.setState({
          rows: rows,
        });

      } else {
        this.setState({loaded: false,
          loadMsg: "Not Authorized"})
      }
    })

  }

  getChannelType=(z, u, c)=>{
    let chType = {};
    chType[0] = "channel_None";
    chType[10] = "channel_On_Off";
    chType[20] = "channel_Irrigation_Scheduled";
    chType[21] = "channel_Irrigation_Accumulated_Light";
    chType[22] = "channel_Irrigation_Cycle";
    chType[23] = "channel_Irrigation_Trigger";
    chType[24] = "channel_Irrigation_Soil_Triger";
    chType[25] = "channel_Irrigation_VPD";
    chType[30] = "channel_CO2";
    chType[40] = "channel_Light_Supplemental";
    chType[41] = "channel_Light_Scheduled";
    chType[42] = "channel_Light_Cyclic";
    chType[43] = "channel_Light_DLI";
    chType[50] = "channel_Microzone";
    chType[60] = "channel_Supply_Pump";
    chType[61] = "channel_Peristaltic_Recirculating_Pump";
    chType[62] = "channel_Peristaltic_Batch_Pump";
    chType[63] = "channel_Peristaltic_Balance_Pump";
    chType[70] = "channel_Fill_Valve";
    chType[80] = "channel_Vent_Roof";
    chType[81] = "channel_Vent_Retractable_Roof";
    chType[82] = "channel_Vent_Side_Wall";
    chType[90] = "channel_Curtain";
    chType[100] = "channel_Mix_Valve";
    chType[110] = "channel_Proportional_Microzone";
    chType[120] = "channel_PID";
    chType[180] = "channel_Variable_Out";

    let z0 = z;
    let c0 = 40 * (u*1) + (c*1);
    let ch = dbVals.z[z0][c0];
    let pid = p.PID_BASE_CONF_CHANNELS + pi.channels_configuration["channelType"];
    let ty1 = 10 * ch[pid];
    switch(ty1){
      case 20:
        pid = p.PID_BASE_CONF_CHAN_DATA + pi.config_channels_configuration["irrigation_mode"];
        ty1 += ch[pid]*1;
        break;
      case 40:
        pid = p.PID_BASE_CONF_CHAN_DATA + pi.config_channels_configuration["light_mode"];
        ty1 += ch[pid]*1
        break;
      case 60:
        pid = p.PID_BASE_CONF_CHAN_DATA + pi.config_channels_configuration["pump_type"];
        ty1 += ch[pid]*1
        break;
      case 80:
        pid = p.PID_BASE_CONF_CHAN_DATA + pi.config_channels_configuration["vent_mode"];
        ty1 += ch[pid]*1
        break;
      default:
        break;
    }
    // cl()
    return chType[ty1];
    // cl(ty1)
    // cl(pid)

    // cl(c0)
    // cl(dbVals.z[z0][c0]);

  }

  getChannelInfo=()=>{
    // cl(this.props.match.params.zuc)
    let zuc = this.props.match.params.zuc.split('-');
    // cl(zuc)
    this.chan = dbVals.zones[zuc[0]*1].units[zuc[1]*1].channels[zuc[2]*1];
    let chType = this.getChannelType(zuc[0], zuc[1], zuc[2]);
    this.setState({
      id: zuc[2]*1,
      name: this.chan.channelName,
      position: this.chan.position,
      relay: this.chan.relay,
      type: chType, // this.chan.channelType,
      used: this.chan.used,
    })

    // cl(this.chan)

  }

  showChannelInfo=()=>{
    return(
      <div>
      <h3>Channel {this.state.id + 1}</h3>
      Name: {this.state.name}<br />
      Position: {this.state.position}<br />
      Relay: {this.state.relay}<br />
      Type: {this.state.type}<br />
      Used: {this.state.used}<br />
      </div>
    );
  }

  barClick = (e)=>{
    cl(e.currentTarget.id);
    switch (e.currentTarget.id){
      case "home":
        history.push("/sa")
        break;
      case "settings":
        let zuc = this.props.match.params.zuc;
        let url = "/fui/live/" + this.state.type + "/" + zuc + "-0";
        cl(url)
        history.push(url);

// /fui/live/(pagetype)/0-0-0-0
        // cl(this.props.match.params); // zone is in id
        // history.push('/zs/z/' + this.props.match.params.id + '/settings');// /zs/z/0/stages
        break;
      default:
        break;
    }
  }

  render(){
    // cl(this.state)
    if(this.state.loaded){
      return(<div>
        <MainBar home settings
        menu click={this.barClick} title="Channel" />
        {this.showChannelInfo()}
      </div>)

    }else{
      return(<div>
        <MainBar home
        menu click={this.barClick} title="Channel" />
        {this.state.loadMsg}
      </div>)
    }
  }
}


  export default Channel ;
