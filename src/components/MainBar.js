import React from 'react';
// import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';
import {ThemeProvider as MuiThemeProvider} from '@material-ui/core/styles';
import theme from '../themes/theme00';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
// import Check1 from '@material-ui/icons/CheckRounded';
// import Close1 from '@material-ui/icons/CloseRounded';
import ArrowBack from '@material-ui/icons/ArrowBackRounded';
import ArrowForward from '@material-ui/icons/ArrowForwardRounded';
import ArrowUpward from '@material-ui/icons/ArrowUpwardRounded';
import Home from '@material-ui/icons/HomeRounded';
import Edit from '@material-ui/icons/EditRounded';
import Settings from '@material-ui/icons/SettingsRounded';
import MenuIcon from '@material-ui/icons/MenuRounded';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import history from "../history"
import {globs} from './utils/utils';
import {cl} from './utils/utils';

class MainBar extends React.Component{
  constructor(props){
    super(props);
    let at = (globs.username === "none") ? "" : " User: " + globs.username +
      (globs.siteName === "none") ? "" : " Site: " + globs.siteName;
    this.state={
      anchorEl: null,
      anchorEl2: null,
      addTitle: at,
    }
  }
  // menu =
  //       [
  //         <MenuItem key="1" id="ParamEditor" onClick={this.handleSelect}>Parameters</MenuItem>,
  //         <MenuItem key="2" id="TableEditor" onClick={this.handleSelect}>Tables</MenuItem>,
  //         <MenuItem key="3"  id="Login" onClick={this.handleSelect}>Log in</MenuItem>
  //       ];

  /*buttons for the main app bar:
  Home, forward, back, up, settings, menu,
  */
  leftButtons(){
    return (
      <div>
      {this.props.home &&
            <IconButton id="home" color="inherit" onClick={this.props.click}>
              <Home/>
            </IconButton>}
      {this.props.upward &&
            <IconButton id="upward" color="inherit" onClick={this.props.click}>
              <ArrowUpward/>
            </IconButton>}
      {this.props.back &&
            <IconButton id="back" color="inherit" onClick={this.props.click}>
              <ArrowBack/>
            </IconButton>}
      {this.props.forward &&
            <IconButton id="forward" color="inherit" onClick={this.props.click}>
              <ArrowForward/>
            </IconButton>}
      </div>
    )
  }

  getButtons(){
    return (
      <div>
      {this.props.edit &&
            <IconButton id="edit" color={this.props.editColor} onClick={this.props.click}>
              <Edit/>
            </IconButton>}
      {this.props.settings &&
            <IconButton id="settings" color="inherit" onClick={this.props.click}>
              <Settings/>
            </IconButton>}
      {(this.props.zoneSettings || this.props.unitSettings) &&
        <IconButton
          color="inherit"
          aria-owns={this.open ? 'menu-zs-appbar' : undefined}
          aria-haspopup="true"
          onClick={this.handleMenu2}
          >
          <Settings/>
        </IconButton>}
      </div>
    )
  }

  handleMenu = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = (e) => {
    this.setState({ anchorEl: null });
  };

  handleMenu2 = event => {
    this.setState({ anchorEl2: event.currentTarget });
  };

  handleClose2 = (e) => {
    this.setState({ anchorEl2: null });
  };

  zoneSettingsMenu=()=>{
    // this.open = false;
    if (this.props.zoneSettings) return(
      <Menu
        id="menu-zs-appbar"
        anchorEl={this.state.anchorEl2}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        open={this.open}
        onClose={this.handleClose2}
      >
        <MenuItem key="1" id="zone_Stages" onClick={this.handleZoneSelect}>Stages</MenuItem>
        <MenuItem key="2" id="zone_Fallback" onClick={this.handleZoneSelect}>Fallback</MenuItem>
        <MenuItem key="3" id="zone_Output" onClick={this.handleZoneSelect}>Output</MenuItem>
        <MenuItem key="4"  id="zone_History" onClick={this.handleZoneSelect}>History</MenuItem>
        <MenuItem key="5"  id="zone_Units" onClick={this.handleZoneSelect}>Units</MenuItem>
        <MenuItem key="6"  id="zone_Irrigation" onClick={this.handleZoneSelect}>Irrigation</MenuItem>
        <MenuItem key="7"  id="zone_Lighting" onClick={this.handleZoneSelect}>Lighting</MenuItem>
        <MenuItem key="8"  id="zone_Alarms" onClick={this.handleZoneSelect}>Alarms</MenuItem>
        <MenuItem key="9"  id="zone_Smartcool" onClick={this.handleZoneSelect}>Smartcool</MenuItem>
        <MenuItem key="10"  id="zone_H-C_Demand" onClick={this.handleZoneSelect}>H/C Demand</MenuItem>
        <MenuItem key="11"  id="zone_Setpoints" onClick={this.handleZoneSelect}>Setpoints</MenuItem>
        <MenuItem key="12"  id="zone_SP_Drive_to_Avg" onClick={this.handleZoneSelect}>SP Drive to Avg</MenuItem>
        <MenuItem key="13"  id="zone_SP_Influence_Factors" onClick={this.handleZoneSelect}>SP Influence Factors</MenuItem>
        <MenuItem key="14"  id="zone_SP_Retractable_Greenhouse" onClick={this.handleZoneSelect}>SP Retractable Greenhouse</MenuItem>
        <MenuItem key="15"  id="zone_Hum_DeHum" onClick={this.handleZoneSelect}>Hum DeHum</MenuItem>
        <MenuItem key="16"  id="zone_Aux_Controls" onClick={this.handleZoneSelect}>Aux Controls</MenuItem>
        <MenuItem key="17"  id="zone_Pump_Schedule" onClick={this.handleZoneSelect}>Pump Schedule</MenuItem>
        <MenuItem key="18"  id="zone_Sensors" onClick={this.handleZoneSelect}>Sensors</MenuItem>
      </Menu>
    );
  }

  unitSettingsMenu=()=>{
    // cl(this.props)
    // this.open = false;
    if (this.props.unitSettings) return(
      <Menu
        id="menu-zs-appbar"
        anchorEl={this.state.anchorEl2}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        open={this.open}
        onClose={this.handleClose2}
      >
        <MenuItem key="1" id="unit_Input_Mapping" onClick={this.handleZoneSelect}>Input Mapping</MenuItem>
        <MenuItem key="2" id="unit_Analog_Temp_Mapping" onClick={this.handleZoneSelect}>Analog Temp Mapping</MenuItem>
        <MenuItem key="3" id="unit_Irrigation_Sensor_Mapping" onClick={this.handleZoneSelect}>Irrigation Sensor_Mapping</MenuItem>
        <MenuItem key="4"  id="unit_Vent_Position_Mapping" onClick={this.handleZoneSelect}>Vent Position Mapping</MenuItem>
        <MenuItem key="5"  id="unit_Mixing_Tanks" onClick={this.handleZoneSelect}>Mixing Tanks</MenuItem>
        <MenuItem key="6"  id="unit_Generic_Mapping" onClick={this.handleZoneSelect}>Generic Mapping</MenuItem>
        <MenuItem key="7"  id="unit_Network_Sensors" onClick={this.handleZoneSelect}>Network Sensors</MenuItem>
        <MenuItem key="8"  id="unit_Accumulator" onClick={this.handleZoneSelect}>Accumulator</MenuItem>
        <MenuItem key="9"  id="unit_Input_Calibration" onClick={this.handleZoneSelect}>Input Calibration</MenuItem>
        <MenuItem key="10"  id="unit_Analog_Temp_Calibration-C_Demand" onClick={this.handleZoneSelect}>Analog Temp Calibration</MenuItem>
        <MenuItem key="11"  id="unit_Soil_Moisture_Calibration" onClick={this.handleZoneSelect}>Soil Moisture Calibration</MenuItem>
        <MenuItem key="12"  id="unit_Vent_Position_Calibration" onClick={this.handleZoneSelect}>Vent Position Calibration</MenuItem>
        <MenuItem key="13"  id="unit_Mixing_Tank_Calibration" onClick={this.handleZoneSelect}>Mixing Tank Calibration</MenuItem>
        <MenuItem key="14"  id="unit_Generic_Calibration" onClick={this.handleZoneSelect}>Generic Calibration</MenuItem>
        <MenuItem key="15"  id="unit_Input_Multipliers" onClick={this.handleZoneSelect}>Input Multipliers</MenuItem>
        <MenuItem key="16"  id="unit_Miscellaneous" onClick={this.handleZoneSelect}>Miscellaneous</MenuItem>
      </Menu>
    );
  }




  handleSelect = e=>{
    switch(e.target.id){
      case "ParamEditor":
        // cl("ae");
        history.push("/params")
        break;
      case "TableEditor":
        // cl("ae");
        history.push("/paramtab")
        break;
      case "FuiEditor":
        // cl("ae");
        history.push("/fui")
        break;
      case "Login":
        history.push("/li")
        break;
      default:
        break;

    }
  }

  handleZoneSelect = e=>{
    cl(e.target.id)
    cl(this.props.zone)
    let pageType = e.target.id;
    let zone = this.props.zone;
// http://localhost:3000/fui/live/two/0-0-0-0
    let url = "/fui/live/" + pageType + "/" + zone + "-0-0-0";
      history.push(url)
    cl(url)
    switch(e.target.id){
      // case "ParamEditor":
      //   // cl("ae");
      //   history.push("/params")
      //   break;
      // case "TableEditor":
      //   // cl("ae");
      //   history.push("/paramtab")
      //   break;
      // case "FuiEditor":
      //   // cl("ae");
      //   history.push("/fui")
      //   break;
      // case "Login":
      //   history.push("/li")
      //   break;
      default:
        break;

    }
  }
  render(){
    const { anchorEl } = this.state;
    const { anchorEl2 } = this.state;
    const open = Boolean(anchorEl);
    this.open = Boolean(anchorEl2);
    // cl(this.open)
    var style;
//     cl(this.props);
    if (this.props.width === undefined){
      style = {};
    } else {
      style = {width: parseInt(this.props.width)}
    }
//     cl(style);
    return(
      <div style={style}>
      <MuiThemeProvider theme={theme}>
      <AppBar position="static"
      >
        <Toolbar>
        {this.leftButtons()}
          <Typography
            variant="h6"
            color="inherit"
            style={{flexGrow: 1}}
            >
            {this.props.title + this.state.addTitle}
          </Typography>
          {this.getButtons()}
          {this.zoneSettingsMenu()}
          {this.unitSettingsMenu()}
          <Menu
            id="menu-appbar"
            anchorEl={this.state.anchorEl}
            anchorOrigin={{
              vertical: 'top',
              horizontal: 'right',
            }}
            transformOrigin={{
              vertical: 'top',
              horizontal: 'right',
            }}
            open={open}
            onClose={this.handleClose}
          >
          <MenuItem key="1" id="ParamEditor" onClick={this.handleSelect}>Parameters</MenuItem>
          <MenuItem key="2" id="TableEditor" onClick={this.handleSelect}>Tables2</MenuItem>
          <MenuItem key="3" id="FuiEditor" onClick={this.handleSelect}>FUI Editor</MenuItem>
          <MenuItem key="4"  id="Login" onClick={this.handleSelect}>Log in</MenuItem>


          </Menu>
          {this.props.menu &&
                <IconButton
                  color="inherit"
                  aria-owns={open ? 'menu-appbar' : undefined}
                  aria-haspopup="true"
                  onClick={this.handleMenu}
                  >
                  <MenuIcon/>
                </IconButton>}

        </Toolbar>
      </AppBar>
      </MuiThemeProvider>
      </div>
    )
  }
}
  export default MainBar ;
