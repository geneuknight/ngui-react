import {dbVals} from '../utils/http';//, postPacks
import {getTime} from '../utils/utils';
import {sendPacks} from '../utils/ws';
import {pi} from '../utils/paramIds';

function makeTabs (){
  return {
    days: ["week_a_sun", "week_a_mon", "week_a_tue", "week_a_wed", "week_a_thu", "week_a_fri", "week_a_sat", "week_b_sun", "week_b_mon", "week_b_tue", "week_b_wed", "week_b_thu", "week_b_fri", "week_b_sat"],
    starts: ["start_time_1", "start_time_2", "start_time_3", "start_time_4", "start_time_5", "start_time_6"],
  }
}

function getChannelIrrigation(z, c, ch){
//   let mode = ch["irrigation_mode"];
  let tabs = makeTabs();
  let repeat = [];
  tabs.days.forEach((d, i)=>{
    repeat[i] = (parseInt(ch[d]) === 1);
  });
  let startTimes = [];
  tabs.starts.forEach((t, i)=>{
    startTimes[i] = ch[t];
  });
  let ret = {
    type: "eq1800_irr_scheduled",
    data: {
      zone: z,
      channel: c,
      tank: ch["tankSensor"],
      name: ch["channelName"],
      type: ch["channelType"],
      startTimes: startTimes,
      onTime: ch["on_duration"],
      repeat: repeat,
    },
  }
  return ret;
//   cl("irig");
//   cl(ret);
//   cl(ch);
}

var getChannelNone = (z, c, ch)=>{
  let ret = {
    type: "eq1800_none",
    data: {
      zone: z,
      channel: c,
//       tank: ch["tankSensor"],
      name: ch["channelName"],
      type: ch["channelType"],
//       startTimes: startTimes,
//       onTime: ch["on_duration"],
//       repeat: repeat,
    },
  }
  return ret;
  
}

function getChannel(z, c){
  let jumps = [getChannelNone, 0, getChannelIrrigation, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ];
  let u = Math.floor(c / 40);
  let c2 = c % 40;
//   cl(dbVals.zones[z].units[u]);
//   cl(dbVals.zones[z].units[u].channels[c2]);
  let ch = dbVals.zones[z].units[u].channels[c2];
  let j = jumps[ch.channelType];
  if (j !== 0) return j(z, c, ch);
}

function saveToId(z, c, table, field, val, packs){
  let i = pi[table][field];
  if (val !== dbVals.z[z][c][i]){
    packs.push({
//       z: z,
      t: getTime(),
      c: c,
      i: i,
      d: val,// should become 'p'
    });
  }
//   if (cur !== val){
//     cl(field);
//     cl(cur);
//     cl(val);
//   }
}

function set_eq1800_irr_scheduled(ch){
/*need to save:
 tank, name, startTimes, onTime, repeat
 
 write tank sensor to the *native* data:
 dbVals.z has zone 0, and zone 255 (sitewide)
 zone 0 has channels 0-39, 192-199, 240, and 255
 the tank is stored in the channel
 so it has zo and c, and the id is 
 
 
 
 z[z]*/
//   cl(dbVals.z);
  let d = ch.data;
  let z = d.zone;
  let c = d.channel;
  let u = Math.floor(c / 40);
  let c2 = c % 40;
  let tabs = makeTabs();
  let reCh = dbVals.zones[z].units[u].channels[c2];
  let packs=[];
  reCh.tankSensor = d.tank.toString();
  saveToId(z, c, "config_channels_configuration", "tankSensor", d.tank.toString(), packs);
  reCh.channelName = d.name;
  saveToId(z, c, "channels_configuration", "channelName", d.name, packs);
  reCh.on_duration = d.onTime;
  saveToId(z, c, "config_channels_configuration", "on_duration", d.onTime, packs);
  tabs.days.forEach((da, i)=>{
    reCh[da] = (d.repeat[i]) ? 1 : 0;  
    saveToId(z, c, "config_channels_configuration", da, reCh[da], packs);
  })
  tabs.starts.forEach((t, i)=>{
    reCh[t] = d.startTimes[i];
    saveToId(z, c, "config_channels_configuration", t, reCh[t], packs);
  })
//   cl(packs);
  sendPacks(z, packs, dbVals.site, dbVals.user);
}

function setChannel(ch){// now, ch is the object that's come from the config editing
/*this needs to save to the zones structure, to z, and send the info to the db!
 what can change:
 if this is an irrigation valve, scheduled, */
//   cl(ch);
  switch(ch.type){
    case "eq1800_irr_scheduled":
      set_eq1800_irr_scheduled(ch);
      break;
    default:
      break ;
  }
}


export {getChannel, setChannel}

//         {type:"eq1800_irr_scheduled", 
//             data:{// this is the data for eq1800_irr_scheduled - each conf type has its own format
//                 zone: 0, 
//                 channel: 0, 
//                 tank: 0,
//                 name: "Irr01", 
//                 type: 2, 
//                 startTimes: [420, 1440, 1440, 1440, 1440, 1440], 
//                 onTime: 0, 
//                 repeat: [true, true, true, true, true, true, true, true, true, true, true, true, true, true],
//             }
//         };
