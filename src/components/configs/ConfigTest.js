import React from 'react';
// import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import DateFnsUtils from '@date-io/date-fns';
import Grid from '@material-ui/core/Grid';
import FormControlLabel from '@material-ui/core/FormControlLabel';// import ConfTextField from './ConfTextField';
import Checkbox from '@material-ui/core/Checkbox';
import { TimePicker } from "@material-ui/pickers";
import {checkLogin} from '../utils/httpauth';//, saveConfigs

// import ConfSelectField from './ConfSelectField';
import ConfParent from './ConfParent';
import MainBar from '../MainBar';
import {init} from '../utils/http'; // , dbVals, getSite
// import {openWS} from '../utils/ws';
import {
  MuiPickersUtilsProvider,
  // KeyboardTimePicker,
  KeyboardDatePicker,
} from '@material-ui/pickers';
import {cl} from '../utils/utils';// globs,
import history from "../../history"
class ConfigTest extends React.Component{
  constructor(props) {
    super(props);
//     if (!dbVals.gotSite){
//
//         let gs = getSite({site: 0});
//         gs.then(r=>{
//             this.setState({loaded: true});
//         })
//         openWS("ws://l4ol.com:3376");
//
//     }
    this.state={
        loaded: false,
        name: "now is",
        age: 20,
        selectedDate: new Date('2014-08-18T21:11:54'),
        spec: {z: this.props.match.params.zid,
          c: this.props.match.params.cid,
          conts: [
            {type: "text"},
            {type: "select"},
            {type: "repeat2week"},
        ]}
    }
    checkLogin().then(r=>{
      if (!r) history.push("/li");
    });
//     if(globs.token === "") return history.push("/li");
    init().then(r=>{// loads site, and user info
        this.setState({loaded: true});
    })

  }

  hc2 = e=>{
    // cl(e.target);
    this.setState({name: e.target.value})
  }

  hc3 = e=>{
    this.setState({age: e.target.value});
  }

  hc4 = e=>{
    cl(e)
    this.setState({selectedDate: e});
  }

  hc5 = e=>{
//     cl(e.target.checked)
    cl(e);
    // this.setState({selectedDate: e});
  }

  testPage = e=>{
    let TF = TextField
    let selectedDate = new Date();
      return(
          <div>
        ConfigTest
        <form className={"container"}>
          <TF
            label="Name"
            value={this.state.name}
            onChange={this.hc2}
          />
        </form>
        <hr/>
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <TimePicker
        ampm={false}

        openTo="hours"
        views={["hours", "minutes", "seconds"]}
        format="HH:mm:ss"
        label="With seconds"
        value={selectedDate}
        onChange={this.hc5}
      />
    </MuiPickersUtilsProvider>
      <hr/>
        <FormControl className={"formControl"}>
          <InputLabel htmlFor="age-simple">Age</InputLabel>
          <Select
            value={this.state.age}
            onChange={this.hc3}
            inputProps={{
              name: 'age',
              id: 'age-simple',
            }}
            >
            <MenuItem value={10}>Ten</MenuItem>
            <MenuItem value={20}>Twenty</MenuItem>
            <MenuItem value={30}>Thirty</MenuItem>
          </Select>
          <FormHelperText>Some important helper text</FormHelperText>
        </FormControl>
        <hr/>
        <form className={"container"} noValidate>
          <TextField
            id="time"
            label="Alarm clock"
            type="time"
            defaultValue="07:30"
            className={"textField"}
            InputLabelProps={{
              shrink: true,
            }}
            inputProps={{
              step: 300, // 5 min
            }}
          />
        </form>
        <hr/>
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
              <Grid container justify="space-around">
                <KeyboardDatePicker
                  disableToolbar
                  variant="inline"
                  format="MM/dd/yyyy"
                  margin="normal"
                  id="date-picker-inline"
                  label="Date picker inline"
                  value={this.state.selectedDate}
                  onChange={this.hc4}
                  KeyboardButtonProps={{
                    'aria-label': 'change date',
                  }}
                />
                />
              </Grid>
            </MuiPickersUtilsProvider>
            <FormControlLabel
              control={
                <Checkbox
                  checked={this.state.checkedB}
                  onChange={this.hc5}
                  value="checkedB"
                  color="primary"
                />
              }
              label="Primary"
            />
            <ConfParent spec={this.state.spec}/>
          </div>

    )
  }

  barClick = (e)=>{
//     cl(e.currentTarget.id);
    switch (e.currentTarget.id){
      case "home":
        history.push("/sa")
        break;
      default:
        break;
    }
  }

  render(){
    // cl(this.state.spec);
    if(this.state.loaded){
        return(
        <div>
          <MainBar home settings
          menu click={this.barClick} title="Channels" />
            <ConfParent spec={this.state.spec}/>
        {
//        this.testPage()
        }
        </div>
        )
    } else {
        return(
            <div>
            loading
            </div>
        )
    }
  }
}

  export default ConfigTest ;
