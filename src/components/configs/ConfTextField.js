import React from 'react';
import TextField from '@material-ui/core/TextField';
import DateFnsUtils from '@date-io/date-fns';
import { TimePicker } from "@material-ui/pickers";
import {
  MuiPickersUtilsProvider,
  // KeyboardTimePicker,
//   KeyboardDatePicker,
} from '@material-ui/pickers';

import {dateToHrMinString, hrMinStringToDate} from '../utils/utils';

// import {cl} from '../utils/utils';
// import history from '../history'
class ConfTextField extends React.Component{
  // constructor(props) {
  //   super(props);
  //   this.state = {
  //     popupLoc: {x: 0, y: 0}
  //   }
  // }
    
read_array_element = (field, data)=>{
//     cl(field);
//     cl(data);
//     cl(field.name);
//     cl(data[field.name][field.index]);
    return data[field.name][field.index];
}

read_field = (field, data)=>{
    switch(field.type){
        case "scalar":
            return data[field.value];
        case "array_element":
            return this.read_array_element(field, data);
        default:
            return;
    }
}

text_time_duration_hms = e=>{
  let selectedDate = new Date();
  return(
    <div>
      <MuiPickersUtilsProvider utils={DateFnsUtils}>
        <TimePicker
          ampm={false}
          openTo="hours"
          views={["hours", "minutes", "seconds"]}
          format="HH:mm:ss"
          label="At&nbsp;each&nbsp;Start&nbsp;Time,&nbsp;turn&nbsp;on&nbsp;irrigation&nbsp;for&nbsp;(h:m:s)"
          value={selectedDate}
          onChange={this.onChange}
        />
      </MuiPickersUtilsProvider>        
    </div>
  )
}
    
text_time_of_day_mins = e=>{
    let val = this.read_field(this.props.template.field, this.props.conf.data);
    let dt = new Date(60000 * val);
    let ti = (val >= 1440) ? "" : dateToHrMinString(dt);
    return(
      <div style={{margin: 20}}>
        <form className={"container"} noValidate>
          <TextField
            id="time"
            label={this.props.template.name}
            type="time"
            defaultValue={ti}
            className={"textField"}
            InputLabelProps={{
              shrink: true,
            }}
            inputProps={{
              step: 300, // 5 min
            }}
            onChange={this.onChange}
          />
        </form>
      </div>
    )
}

onChange = e=>{
  var val;
  switch (this.props.template.type){
    case "text_time_of_day_mins":
      val = hrMinStringToDate(e.target.value);
      break ;
    case "text0":
      val = e.target.value;
      break;
    default:
      break;
  }
//   cl(this.props.template);
//   cl(e.target.value);
//   cl(val);
  let ret = {target: {field: this.props.template.field, value: val}}
  this.props.onChange(ret);
}

text0 = e=>{
    let val = this.read_field(this.props.template.field, this.props.conf.data);
//     cl(val);
//     cl("text0");
    return(
      <div style={{margin: 20}}>
        <form className={"container"}>
          <TextField
            label="Name"
            value={val}
            onChange={this.onChange}
          />
        </form>
      </div>
    )
//     return (<div>text</div>);
}
    
    

  render(){
//       cl(this.props);
//       cl(this.props.template.type);
      switch(this.props.template.type){
          case "text_time_of_day_mins":
              return this.text_time_of_day_mins();
          case "text_time_duration_hms":
              return this.text_time_duration_hms();
          case "text0":
              return this.text0();
          default:
              return
      }
  }
}

  export default ConfTextField ;
