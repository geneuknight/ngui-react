import React from 'react';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
// import FormHelperText from '@material-ui/core/FormHelperText';
// import {cl} from '../utils/utils';
import {dbVals} from '../utils/http';
// import history from '../history'
class ConfSelectField extends React.Component{
//   constructor(props) {
//     super(props);
//     this.state = {
// //       popupLoc: {x: 0, y: 0}
//       age: 20,
//     }
//   }
  
    initMixTankSelects = e=>{
//         cl(this.props.template.parms);
        let parms = this.props.template.parms;
        let d = this.props.conf.data;
        let z = d[parms[0]];// zone
        let c = d[parms[1]];// channel
        let u = Math.floor(c / 40);
//         let tank = d.tank;
        return dbVals.zones[z].units[u].ecph.map((e, i)=>{
                if (e.name === undefined) e.name = "Tank " + (i + 1);
                return(
                    <MenuItem key={i} value={i}>{e.name}</MenuItem>
                )
            });
    }
    
    onChange = e=>{
      let field = {type: "scalar", value: "tank"}
      let value = e.target.value;
      let ret = {target: {field: field, value: value}};
      this.props.onChange(ret);
    }
    
    showTankSelect = e=>{
//          <FormHelperText>Some important helper text</FormHelperText>
        return(
      <div style={{margin: 20}}>
        <FormControl className={"formControl"}>
          <InputLabel htmlFor="age-simple">Mixing&nbsp;Tank</InputLabel>
          <Select
            value={this.props.conf.data.tank}
            onChange={this.onChange}
            inputProps={{
              name: 'Mixing Tank',
              id: 'mix_tank_select',
            }}
            >
            {this.initMixTankSelects()}
          </Select>
        </FormControl>
      </div>
        )
    }

  render(){
//     cl(this.props);
    switch (this.props.template.type){
      case "select_mix_tank":
        return this.showTankSelect();
      default:
        return
    }
  }
}

  export default ConfSelectField ;
