import React from 'react';
import FormControlLabel from '@material-ui/core/FormControlLabel';// import ConfTextField from './ConfTextField';
import Checkbox from '@material-ui/core/Checkbox';
// import {cl} from '../utils/utils';
// import history from '../history'
class ConfRepeat2Week extends React.Component{
  // constructor(props) {
  //   super(props);
  //   this.state = {
  //   }
  // }

  indexes = [[0, 7], [1, 8], [2, 9], [3, 10], [4, 11], [5, 12], [6, 13]];
  weekDays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

  hc5 = e=>{
/* this.props.repeatpat is an array. make a copy of it, change it, and call onChange*/
//     cl(e.target.id);
//     cl(e.target.checked);
    let arr = this.props.repeatpat.slice(0);
    arr[e.target.id]=e.target.checked;
    let ret = {target: {value: arr}};
    this.props.onChange(ret);
//     arr
// let objCopy = Object.assign({}, obj);    
//    this.
  }
  
  onChange = e=>{
//     cl(e.target);
//     cl(e.target.checked);
//     cl(e.target.id);
    let arr = this.props.conf.data[this.props.template.field.value].slice(0);
    arr[parseInt(e.target.id)] = e.target.checked;
//     cl(arr);
    let ret = {target: {field: this.props.template.field, value: arr}}
    this.props.onChange(ret);
    
  }

  showCheck = e=>{ // e is the index. No, e is an object
//     cl(e)
//     let field = this.props.template.field.value;
//     cl(field)
//     cl(this.props.conf.data[field])
//     let repeat = this.props.conf.repeat
    return (
      <FormControlLabel
        control={
          <Checkbox
            id={e.ind.toString()}
            checked={this.props.conf.data[this.props.template.field.value][e.ind]}
            onChange={this.onChange}
            value="checkedB"
            color="primary"
          />
        }
        label={e.day}
      />
    )
  }

  showControlCol = (e, i, j)=>{
    let cont = {ind: e, day: this.weekDays[i]}
    return(
      <td key={j}>
      {this.showCheck(cont)}
      </td>
    )
  }

  showControlRow = (e, i)=>{
    return(
      <tr key={i}>
      {e.map((c, j)=>{
        return this.showControlCol(c, i, j);
      })}
      </tr>
    )
  }

  render(){
    return(
      <div style={{margin: 20}}>
      <table><tbody>
      <tr><th>Week 1</th><th>Week 2</th></tr>
      {this.indexes.map((e, i)=>{
        return this.showControlRow(e, i)
      })}
      </tbody></table>
      </div>
    )
  }
}

  export default ConfRepeat2Week ;
