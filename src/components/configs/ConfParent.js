import React from 'react';
// import {cl} from '../utils/utils';
import ConfTextField from './ConfTextField';
import ConfSelectField from './ConfSelectField';
import ConfRepeat2Week from './ConfRepeat2Week';
import ConfTitleField from './ConfTitleField';
import Button from '@material-ui/core/Button';
import {getChannel, setChannel} from './getSetConfigs';
import {getZoneInfo} from '../utils/http';
// import history from '../history'
class ConfParent extends React.Component{
  constructor(props) {
    super(props);
    let chan = getChannel(props.spec.z, props.spec.c); // , // this.conf,
    // cl(chan);
    this.state = {
//       popupLoc: {x: 0, y: 0},
      repeat: [false, true, false, false, false, false, false, false, false, false, false, false, ],
      conf: chan, // getChannel(props.spec.z, props.spec.c), // this.conf,
      template: this.template[chan.type],
    }
//     setChannel(this.state.conf);
//     this.conf = getChannel(0, 0);
//     cl(this.props);
  }

//       conf =
//         {type:"eq1800_irr_scheduled",
//             data:{// this is the data for eq1800_irr_scheduled - each conf type has its own format
//                 zone: 0,
//                 channel: 0,
//                 tank: 0,
//                 name: "Irr01",
//                 type: 2,
//                 startTimes: [420, 1440, 1440, 1440, 1440, 1440],
//                 onTime: 0,
//                 repeat: [true, true, true, true, true, true, true, true, true, true, true, true, true, true],
//             }
//         };

        template = {
          eq1800_irr_scheduled: {
            controls: [
              {type: "title", value: "Irrigation Valve - Scheduled Mode",},
              {type: "text0", name: "Name", field: {type: "scalar", value: "name"}},
              {type: "select_mix_tank", name: "Mixing Tank", parms: ["zone", "channel"], field: {type: "scalar", value: "tank"}},
              {type: "repeat_2_week", name: "Watering Days Schedule", field: {type: "scalar", value: "repeat"}},
              {type: "text_time_of_day_mins", name: "Start Time 1", field: {type: "array_element", name: "startTimes", index: 0}},
              {type: "text_time_of_day_mins", name: "Start Time 2", field: {type: "array_element", name: "startTimes", index: 1}},
              {type: "text_time_of_day_mins", name: "Start Time 3", field: {type: "array_element", name: "startTimes", index: 2}},
              {type: "text_time_of_day_mins", name: "Start Time 4", field: {type: "array_element", name: "startTimes", index: 3}},
              {type: "text_time_of_day_mins", name: "Start Time 5", field: {type: "array_element", name: "startTimes", index: 4}},
              {type: "text_time_of_day_mins", name: "Start Time 6", field: {type: "array_element", name: "startTimes", index: 5}},
              {type: "text_time_duration_hms", name: "At each Start Time, turn on irrigation for:",
                  field: {type: "scalar", value: "onTime"}},
            ]
          },// for eq1800_irr_scheduled
          eq1800_none: {
            controls: [
              {type: "title", value: "No Equipment",},
              {type: "text0", name: "Name", field: {type: "scalar", value: "name"}},
            ]
          },// for eq1800_none
        };
/*so who is going to hold the state for a control dialog?
 it seems like it should be here, in the ConfParent.
 the idea is to go through the lines in the template, showing the proper control, and passing the template info, and the conf
 data to it in props. when it is modified, we'll get a onChange callback, and we'll update the conf object.
 presumably, when *this* dialog exits with an "ok", then we'll send an "onChange" to our parent.

 */

    contChange = e=>{
/*this needs to get the info to know what control and what value needs changing
 */
        this.setState({repeat: e.target.value});
//         cl(e);
    }

    setDataValue(data, field, value){
      switch (field.type){
        case "scalar":
          data[field.value] = value;
          break;
        case "array_element":
          let arr = data[field.name].slice(0);
          arr[field.index] = value.toString();
          data[field.name] = arr;
//           cl(arr)
          break;
        default:
          break ;
      }
    }

    onChange = e=>{
//       cl(e.target);// returns {field: {type: "scalar", value: "tank"}, value: 3}
      let conf = Object.assign({}, this.state.conf);
      this.setDataValue(conf.data, e.target.field, e.target.value);
      this.setState({conf: conf});
    }

  showMapConts(t, conf, i){
/*OK, this is going through an array of controls in this.props.spec.conts, and edit it
 whenever there's a change, it makes a *copy* of the information it's been given, changes it
 and sends it back as a "handle change" event, as the "value" of the event. this is
 just the way that normal dialog controls work.*/
//     cl(t);
//     cl(conf);
    switch(t.type){
      case "title":
//         cl("title");
        return(<ConfTitleField key={i} template={t} conf={conf} onChange={this.onChange}/>)
      case "text_time_of_day_mins":
        return(<ConfTextField key={i} template={t} conf={conf} onChange={this.onChange}/>)
      case "text_time_duration_hms":
        return(<ConfTextField key={i} template={t} conf={conf} onChange={this.onChange}/>)
      case "text0":
        return(
          <ConfTextField key={i} template={t} conf={conf} onChange={this.onChange}/>
        )
      case "select":
        return(
          <ConfSelectField key={i} template={t} conf={conf} onChange={this.onChange}/>
        )
      case "select_mix_tank":
        return(
          <ConfSelectField key={i} template={t} conf={conf} onChange={this.onChange}/>
        )
      case "repeat_2_week":
        return(
          <ConfRepeat2Week key={i} template={t} conf={conf} onChange={this.onChange}/>
        )
      default:
        return
    }
  }

  showControls=(template, conf)=>{
//     let conts = this.props.spec.conts;
//     cl(conts)
    return template.controls.map((t, i)=>{
      return this.showMapConts(t, conf, i)
      })
  }

  onClick = (e)=>{
    switch(e){
      case "ok":
        return setChannel(this.state.conf);
      case "refresh":
        getZoneInfo();
        return this.setState({conf: getChannel(0, 0)})
      default:
        break ;
    }
//     cl(e);
//     cl(e.parent.id);
  }

  showOkCancel = ()=>{
    return(
      <div>
      <Button onClick={()=>this.onClick("ok")} style={{margin: 20}} variant="contained" color="primary" className={"button"}>
        OK
      </Button>
      <Button onClick={()=>this.onClick("cancel")} style={{margin: 20}} variant="contained" color="primary" className={"button"}>
        Cancel
      </Button>
      <Button onClick={()=>this.onClick("refresh")} style={{margin: 20}} variant="contained" color="primary" className={"button"}>
        Refresh
      </Button>
      </div>
    )
  }

  render(){
    return(
      <div>
        {this.showControls(this.state.template, this.state.conf)}
        {this.showOkCancel()}
      </div>
    )
  }
}

  export default ConfParent ;


  /* these are all in channelData:
   start_time_1
   week_a_tue
   on_duration
   channelName: Irr01
   channelType: 2 (irr valve)
   *
   * |         0 |         0 |            0 | 2019-08-24 08:51:20 |    1 |        0 | Irr01       |           2 | {"irrigation_mode": 0, "on_duration": 0, "off_duration": 180, "start_time_1": 1440, "start_time_2": 1440, "start_time_3": 1440, "start_time_4": 1440, "start_time_5": 1440, "start_time_6": 1440, "week_a_sun": 1, "week_a_mon": 1, "week_a_tue": 1, "week_a_wed": 1, "week_a_thu": 1, "week_a_fri": 1, "week_a_sat": 1, "week_b_sun": 1, "week_b_mon": 1, "week_b_tue": 1, "week_b_wed": 1, "week_b_thu": 1, "week_b_fri": 1, "week_b_sat": 1, "start_astroadjust": 0, "start": 0, "end_astroadjust": 0, "end": 0, "setting_1_threshold_light": 0.0, "setting_1_threshold_temp": 40.0, "setting_2_threshold_light": 0.0, "setting_2_threshold_temp": 40.0, "soil_moisture_input": 0, "soil_moisture_threshold": 0.0, "vpdacc_threshold": 0.0, "tankSensor": 255} |            0 |             0 |              0 |            0 |           0 |            NULL |            NULL |               NULL |         NULL |                    NULL |             |
   sample channel object, for config
   {type:"eq1800_irr_scheduled", d:{// this is the data for eq1800_irr_scheduled - each conf type has its own
   z: 0, c:0, name:"Irr01", type:2, confType: 0x20 (irr, mode 0),
   startTimes: [1440, 1440, 1440, 1440, 1440, 1440], onTime: 0,
   repeat: [true, true, true, true, true, true, true, true, true, true, true, true, true, true],
   }
   }

   this can be managed by getChannelConfig(z, c), and putChannelConfig(z, c, d)


   */
