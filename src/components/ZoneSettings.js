import React from 'react';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Settings from './Settings';
import MainBar from './MainBar';
import {globs} from './utils/utils';
import {init} from './utils/http';
import {checkLogin} from './utils/httpauth';
import history from "../history"

import {cl} from './utils/utils';
class ZoneSettings extends React.Component{
  constructor(props) {
    super(props);
    let loaded = false;
    if (globs.token === ""){
      checkLogin().then(r=>{
        if (!r) {
          history.push("/li");
        } else {}
          init().then(r=>{// loads site, and user info
            cl("got");
              this.setState({loaded: true});
        })
      });
    }else{
      loaded = true;
    }
    this.state={
      page: this.props.match.params.page,
      loaded: loaded,
      
    }
  }
  
  zonePages = [
    {name: "Stages", id: "stages"},
    {name: "Fallback",  id: "fallback"},
    {name: "Output",  id: "output"},
    {name: "History",  id: "history"},
    {name: "Units",  id: "units"},
    {name: "Irrigation",  id: "irrigation"},
    {name: "Lighting",  id: "lighting"},
    {name: "Alarms",  id: "alarms"},
    {name: "Smartcool",  id: "smartcool"},
    {name: "H/C Demand",  id: "hcdemand"},
    {name: "Setpoints",  id: "setpoints"},
    {name: "SP Drive to Avg",  id: "spd2a"},
    {name: "SP Influence Factors",  id: "spif"},
    {name: "SP Retractable Greenhouse",  id: "sprg"},
    {name: "Hum / DeHum",  id: "hdh"},
    {name: "Aux Controls",  id: "auxcontrols"},
    {name: "Pump Schedule",  id: "pumpsched"},
    {name: "Sensors", id: "sensors"},
  ];
  
  initHistoryListen=()=>{
    history.listen( (l, a) =>  {
      cl(l)
    });
  }
  
  barClick = (e)=>{
    cl(e.currentTarget.id);
    switch (e.currentTarget.id){
      case "home":
        history.push("/sa")
        break;
      case "settings":
        cl(this.props.match.params); // zone is in id
        history.push('/zs/z/' + this.props.match.params.id + '/settings');// /zs/z/0/stages
        break;
      default:
        break;
    }
  }
  
  initSettingsSelects=()=>{
    return this.zonePages.map((s, i)=>{
//       cl(s);
      return(
        <MenuItem key={i} value={s.id}>{s.name}</MenuItem>
      );
    });
  }
  
  onSettingsChange=(e)=>{
//     cl(e.target.value);
    let val = e.target.value;
//     this.setState({page: val});
    history.push('/zs/z/' + this.props.match.params.zone + '/' + val);// /zs/z/0/stages
}
  
  showSettingsPages=()=>{
    return(
      <div style={{margin: 20}}>
        <FormControl className={"formControl"}>
          <InputLabel htmlFor="age-simple">Settings&nbsp;Page</InputLabel>
          <Select
            value={this.props.match.params.page}
            onChange={this.onSettingsChange}
            inputProps={{
              name: 'settings_select',
            }}
            >
            {this.initSettingsSelects()}
          </Select>
        </FormControl>
      </div>
    );
  }

  render(){
//     cl(this.zonePages);
    if (this.state.loaded){
      return(
        <div>
          <MainBar home settings
          menu click={this.barClick} title="Zone Settings" />
          {this.showSettingsPages()}
          <Settings zone="0" unit="0" chan="0" page={this.props.match.params.page}/>
        </div>
      );
    }else{
      return(
        <div>loading</div>
      );
    }
  }
  
}

export default ZoneSettings ;
