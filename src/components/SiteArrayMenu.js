import React from 'react';
// import {getSite, } from './utils/http'; // dbVals, getLogins
// import SiteArray from './SiteArray';
import {cl} from './utils/utils';

class SiteArrayMenu extends React.Component{
  // constructor(props) {
  //   super(props);
  // }

  click=e=>{
    cl(e.target.id)// 2 | Name
    this.props.sel(e.target.id);
  }

  showFields=e=>{
    // cl(this.props);
    return(
      <div>
      <div
        onClick={this.click}
        style={{cursor:"pointer"}}
      ><strong
        id={this.props.col+"|delete"}
      >Delete {this.props.cols[this.props.col]}</strong></div>
      <div><strong>Add:</strong></div>
      {this.props.fields.map((f, i)=>{
        let id=this.props.col+"|"+f;
        return(
        <div key={i}
          id={id}
          onClick={this.click}
          style={{cursor:"pointer"}}
        >{f}</div>
      )})}
      </div>
    )
  }



  render(){
    return(
      <div>
      {this.showFields()}
      </div>
    )
  }
}


  export default SiteArrayMenu ;
