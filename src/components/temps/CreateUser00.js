import React from 'react';
// import {getLogins} from '../utils/http';
class CreateUser extends React.Component{
  constructor(props) {
    super(props);
    this.state = {value: ''};

    // this.handleChange = this.handleChange.bind(this);
    // this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange = event=> {
    this.setState({value: event.target.value});
  }

  handleSubmit = event=> {
    alert('A name was submitted: ' + this.state.value);
    event.preventDefault();
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
      <h3>Create a New User</h3>
      <table><tbody>
      <tr><td>Username:</td>
      <td><input type="text" value={this.state.value} onChange={this.handleChange} /></td></tr>
      <tr><td>Password:</td>
      <td><input type="text" value={this.state.value} onChange={this.handleChange} /></td></tr>
      <tr><td></td>
      <td><input type="button" value="Create" /></td></tr>
      </tbody></table>
      </form>
    );
  }
}

  export default CreateUser ;
