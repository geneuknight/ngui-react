import React from 'react';
import {cl, globs} from '../utils/utils';//, saveLocalStorage, getLocalStorage
import {login, createUser, getConfigs, saveConfigs, getUsers, getSites, saveSite, saveUser, deleteUser} from '../utils/httpauth';
import history from '../../history';
class Login extends React.Component{
  constructor(props) {
    getUsers().then(r=>{
      this.setState({
        users: r.u,
      });
//       this.addPrivs(r.u);
//       cl(r);
//       this.selUser(0, r.u[0]);
    });
    getSites().then(r=>{
//       cl(r);
      this.setState({sites: r.s});
    });
    super(props);
//     saveLocalStorage("this", "is");
//     let that = getLocalStorage("this");
//     cl(that);
    this.state = {value: '',
      username: "test",
      password: "pass",
      siteid: 0,
      users: [],
      sites:[],
      siteName: "none",
      siteSel: -1,
      siteLoginSel: -1,
      userSelected: -1,
      privs: [],
      privsText: "",
      siteSelected: 0,
      sitePrivs: "",
      errorMsg: "",
    };

    // this.handleChange = this.handleChange.bind(this);
    // this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidCatch(error, info){  }

//   addPrivs=(users)=>{
//     users.forEach(u=>{
//       u.privs=[{s:0, p:1}, {s:1, p:1}];
// //       cl(u);
//     })
//   }

  handleChange = e=> {
    // switch(e.target.id){
    //   case "username":
    //     field = e
    // }
    // cl(e.target.id);
    let s = {}
    s[e.target.id] = e.target.value;
    // cl(s)
   this.setState(s);
  }

  handleSubmit = event=> {
    event.preventDefault();
    // alert('A name was submitted: ' + this.state.value);
    let siteSel = this.state.siteLoginSel;
    cl(siteSel)
//     cl(siteSel);
    let id2 = this.state.sites[siteSel].siteid;
    // return
    globs.siteName = this.state.sites[siteSel].name;
//     let shortName = this.state.sites[siteSel].shortName;
//     cl(shortName);
    let parms = {
      username: this.state.username,
      password: this.state.password,
//       siteido: this.state.siteid,
      siteid: id2,
//       siteShortName: shortName,
    }
    event.preventDefault();
//     cl(parms);
    let lo = login({Post: parms});
    lo.then(r=>{
      // cl(r);
      history.goBack();
    })

    // cl(parms)
  }

  create=e=>{
    cl("create");
    let parms = {
      username: this.state.username,
      password: this.state.password,
      siteid: this.state.siteid,
    }
    createUser({Post: parms});
  }

  saveConfig=e=>{
    cl("save");
    saveConfigs({Post: {configs: ["Zone"]} });
  }


  getConfig=e=>{
    cl("get")
    getConfigs({Post: ""})
  }

  selUser=(uid, u)=>{
    this.setState({
      username: u.username,
      password: "",
      userSelected: uid,
      siteid: "",
      privs: u.privs,
      privsText: JSON.stringify(u.privs),
    });
  }

  userSelectChange=e=>{
//     cl(e.target.value);
    let val = e.target.value;
    let u = this.state.users[val];
    this.selUser(val, u);
  }

  dispUsers=()=>{
// {_id: "5d6f991b3b67ac100e5923b5", username: "none", passhash: "$2b$12$SLRwBmez3NJHAcaoDXMaIO8ZxVxUL0xu/8YzdPQCH.RDww29xBrKW", userid: "4bMgiyBm3ZyON7iiL1R-OpnGk3Et9Wy4"}
    return(
      <tr><td>Users:</td>
      <td>
      <select onChange={this.userSelectChange}>
      {this.state.users.map((u, i)=>{
        return(
          <option key={i} value={i}>{u.username}</option>
        );
      })}
      </select>
      </td></tr>
    );
  }

  siteEditSelectChange=(e)=>{
//     cl("change");
//     cl(e.target.value);
    let val = e.target.value;
    let site = this.state.sites[val];
    this.setState({siteName: site.name, siteSel: val});

  }

  saveSite=()=>{
//     cl("save");
//     cl(this.state.sites[this.state.siteSel]);
    let arr = this.state.sites.slice(0);
    arr[this.state.siteSel].name = this.state.siteName;
    this.setState({sites: arr});
    saveSite(arr[this.state.siteSel]);
//     cl(arr);
  }

  siteLoginSelectChange=(e)=>{
//     let sel = e.target.value;
//     cl(sel);
    this.setState({siteLoginSel: e.target.value});
  }

  showLoginSites=()=>{
    return(
      <select onChange={this.siteLoginSelectChange}>
    {this.state.sites.map((s, i)=>{
      return(
        <option key={i} value={i}>{s.name}</option>
      );
    })}
    </select>
    );
  }



  dispSites=()=>{
    return(
      <tr><td>Sites:</td>
      <td>
      <select onChange={this.siteEditSelectChange}>
    {this.state.sites.map((s, i)=>{
      return(
        <option key={i} value={i}>{s.name}</option>
      );
    })}
    </select>
      </td></tr>
    );
  }

  siteNameChange=(e)=>{
//     cl(e);
//     cl(e.target.value);
    this.setState({siteName: e.target.value});

  }

  addSite=()=>{
    cl("add");
  }

  siteSelectChange=(e)=>{
    let val = e.target.value;
    this.setState({siteSelected: val});
    this.setState({
      sitePrivs: JSON.stringify(this.state.privs[val]),
    });
//     cl("ch");
  }

  sitePrivChange=(e)=>{
    cl(e.target.value);
    this.setState({sitePrivs: e.target.value})
  }

  dispSitePrivs=()=>{
    return(
      <>
      <tr><td>site</td>
        <td>
          <select onChange={this.siteSelectChange} value={this.state.siteSelected}>
          {this.state.privs.map((site, i)=>{
//             cl(u);
            return(
              <option key={i} value={i}>{site.s}</option>
            );
          })}
          </select>
        </td></tr>
      <tr>
        <td><input type="button" value="Add" onClick={this.addSite} /></td>
        <td><input type="text" name="addSite" onChange={this.sitePrivChange} value={this.state.sitePrivs}/></td>
      </tr>
      </>
    );
  }

  dispPrivilegesChange=(e)=>{
    this.setState({
      privsText: e.target.value,
    });

  }

  dispPrivileges=()=>{
    return(
      <tr valign="top"><td>Privileges<br/><span style={{color: "red"}}>{this.state.errorMsg}</span></td>
      <td>
        <textarea style={{resize: "none"}} cols="40" rows="10" onChange={this.dispPrivilegesChange} value={this.state.privsText}></textarea>
      </td></tr>
    );
  }

//   createSaveUser=(func, u)=>{
//     let errorMsg = "";
// //     let u = this.state.users[uid];
//     try {
//       let privs = JSON.parse(this.state.privsText);
//       u.privs = privs;
//       cl(u);
//       func(u);
//     } catch {errorMsg = "JSON error"}
//     this.setState({errorMsg: errorMsg});
//   }

  getPrivs=()=>{// returns empty string on error
    let errorMsg = "";
    let privs = "";
    try {
//       cl(this.state.privsText);
      privs = JSON.parse(this.state.privsText);
    } catch {errorMsg = "JSON error"}
    this.setState({errorMsg: errorMsg});
//     cl(privs);
    return privs;
  }

  saveUser=()=>{
    let privs = this.getPrivs();
    if (privs !== ""){
//       cl(privs);
      let users = this.state.users.slice(0);
      let u = users[this.state.userSelected];
      u.username = this.state.username;
      u.password = this.state.password;
      u.privs = privs;
      this.setState({users: users});
      saveUser(u);
    }
  }

  createUser=()=>{
    let privs = this.getPrivs();
    if (privs !== ""){
      let u = {
        password: this.state.password,
        username: this.state.username,
        privs: privs,
      }
      let users = this.state.users.slice(0); // Object.assign({}, this.state.users);
      users.push(u);
      this.setState({users: users});
      createUser(u);
    }
  }

  deleteUser=()=>{
      let users = this.state.users.slice(0);
      cl(users);
      let u = users[this.state.userSelected];
      users.splice(this.state.userSelected, 1);
      cl(users);
      this.setState({users: users, userSelected: 0});
      cl("state");
      deleteUser(u);
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
      <h3>Login</h3>
      <table><tbody>
      <tr><td>Username:</td>
      <td><input type="text" id="username" value={this.state.username} onChange={this.handleChange} /></td></tr>
      <tr><td>Password:</td>
      <td><input type="text" id="password" value={this.state.password} onChange={this.handleChange} /></td></tr>
      <tr><td>Site</td><td>{this.showLoginSites()}</td></tr>
      <tr>
      <td></td>
      <td><input type="submit" value="Login" /></td></tr>
      </tbody></table>
      <h3>Edit Users</h3>
      <table><tbody>
      <tr>
      <td><input type="button" value="Create" onClick={this.createUser} /></td>
      <td>(fill in username / password above)</td></tr>
      <tr>
      <td><input type="button" value="Save" onClick={this.saveUser} /></td>
      <td><input type="button" value="Delete" onClick={this.deleteUser} /></td></tr>
      {this.dispUsers()}
      {this.dispPrivileges()}
      </tbody></table>
      <h3>Edit Sites</h3>
      <table><tbody>
      {this.dispSites()}
      <tr><td>Name</td><td></td>
      </tr>
      <tr><td><input type="button" value="Save" onClick={this.saveSite} /></td>
      <td><input type="text" id="sitename" value={this.state.siteName} onChange={this.siteNameChange} /></td>
      </tr>
      </tbody></table>
      </form>
    );
  }
}

  export default Login ;
