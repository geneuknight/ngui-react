var config = require('./config');
var globs = {};

globs.floater = [];
globs.token = "";
globs.userConfigs = null;
globs.siteid = 0;
globs.timers = {};
globs.fuiPages = null;

var cl = console.log
// const server = "dev";

var c = { // constants
  UNIT_TYPE: 0,
  CHANNEL_TYPE: 1,
  ECPH_TYPE: 2,
  ECPH_SENSOR_TYPE: 3,
  ZONE_TYPE: 4,
  SITE_TYPE: 5,
  ZONE_CONFIG_TYPE: 6,
  CONT_CONFIG_TYPE: 7,
  wsUrl: "ws://dev.l4ol.com:3376",
}

const init = ()=>{
    switch (config.server){
      case "main":
        c.logUrl = "http://main.li4.pw:3373/log/" ;
        c.wsUrl = "ws://main.li4.pw:3376" ;
        c.authUrl = "http://main.li4.pw:3374";
        break;
      case "home":
        c.logUrl = "http://dads.tr8.us:3373/log/" ;
        c.wsUrl = "ws://dads.tr8.us:3376" ;
        c.authUrl = "http://dads.tr8.us:3374";
        break;
      case "dev":
        c.logUrl = "http://dev.main.li4.pw:3373/log/" ;
        c.wsUrl = "ws://dev.main.li4.pw:3376" ;
        c.authUrl = "http://dev.main.li4.pw:3374";
        break;
      case "stage":
        c.logUrl = "http://stage.l4ol.com:3373/log/" ;
        c.wsUrl = "ws://main.li4.pw:3376" ;
        c.authUrl = "http://main.li4.pw:3374";
        break;
      case "prod":
        c.logUrl = "http://prod.l4ol.com:3373/log/" ;
        c.wsUrl = "ws://main.li4.pw:3376" ;
        c.authUrl = "http://main.li4.pw:3374";
        break;
      default:
        break;
    }
} ;

  init();

globs.events = (function(){
  var topics = {};
  var hOP = topics.hasOwnProperty;

  return {
    subscribe: function(topic, listener) {
      // Create the topic's object if not yet created
      if(!hOP.call(topics, topic)) topics[topic] = [];

      // Add the listener to queue
      var index = topics[topic].push(listener) -1;

      // Provide handle back for removal of topic
      return {
        remove: function() {
          delete topics[topic][index];
        }
      };
    },
    publish: function(topic, info) {
      // If the topic doesn't exist, or there's no listeners in queue, just leave
      if(!hOP.call(topics, topic)) return;

      // Cycle through topics queue, fire!
      topics[topic].forEach(function(item) {
      		item(info !== undefined ? info : {});
      });
    }
  };
})();

function ms(id, left, top, width){
  switch (id){
    case 0:
      return {
      position: "absolute",
      fontFamily: "sans-serif",
      fontSize: 15,
      left: left,
      top: top,
      minHeight: 600,
      width: width, // globs.windowWidth,
      backgroundColor: "#008070",
    //        backgroundImage: "url(" + backImage + ")",
    }
    default:
      return ;
  }
}

function getTime(){
//   return Math.floor ((new Date()).getTime() / 1000);
  return (new Date()).getTime() / 1000 ;
}

function leadZeros(val, zeros){
    return ("000000" + val).substr(0 - zeros);
}

function dateToHrMinString(dt){
    let hr = dt.getUTCHours();
    let mn = dt.getUTCMinutes();
    return leadZeros(hr, 2) + ":" + leadZeros(mn, 2);
}

function hrMinStringToDate(hm){
  let hmArr = hm.split(":");
  return 60 * parseInt(hmArr[0]) + parseInt(hmArr[1]);
}

function saveLocalStorage(key, val){
  localStorage.setItem(key, val);
}

function getLocalStorage(key){
  return localStorage.getItem(key);
}

function start(id){
  globs.timers[id] = getTime();
//   cl(getTime());
}

function stop(id){
  let elapse = getTime() - globs.timers[id];
  elapse = Math.floor(1000 * elapse) / 1000;
  delete globs.timers[id];
  return (id + " timer: " + elapse);
//   cl(id + " timer: " + elapse);
}

function show(id){
  let elapse = getTime() - globs.timers[id];
  elapse = Math.floor(1000 * elapse) / 1000;
//   delete globs.timers[id];
  return (id + " timer: " + elapse);
}



export {c, globs, ms, cl, getTime, dateToHrMinString, hrMinStringToDate, saveLocalStorage,
  getLocalStorage, start, show, stop}
