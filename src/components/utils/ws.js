import {cl, start, stop, globs} from './utils';
import {dbVals, putSite} from './http'; // , putStream
import {getToken, getSiteId} from './httpauth';


// var globWs = 0;

// function testWs(){
//   cl("testing ws")
//   let ws = new WebSocket("ws://l4ol.com:3376");
//   globWs = ws
//   ws.onopen = e=>onOpen(e);
//   ws.onclose = e=>onClose(e);
//   ws.onmessage = e=>onMessage(e);
//   ws.onerror = e=>onError(e);
// }

var iv = {
  ws: null,
  key: 0,
  callBacks: {},
};

var handleData00=(mo)=>{
//   cl("data00");
//   cl(mo);
//   let z = mo.zone;
  mo.params.forEach(p=>{
//     cl(z, p.c, p.i, p.d)
    try{
      dbVals.z[p.z][p.c][p.i] = p.d;
    }catch{}
    if (p.i === 4642){
      cl("Setpoint Change from Site: " + p.d);
    }
  });
  globs.events.publish("data", 0);
}

var handleMsg=(mo)=>{
  switch(mo.command){
    case "data00":
      handleData00(mo);
      break;
    default:
      break;
  }

}

var recvSocket = (msg)=>{
  let mo = JSON.parse(msg.data);
  // cl(mo);
  if (mo.key !== undefined){
    iv.callBacks[mo.key][0](mo);
  }else{
    handleMsg(mo);
  }
}

var sendSocket = (msg)=>{
  return new Promise((r, e)=>{
    msg.key = iv.key;
    iv.callBacks[msg.key] = [r, e];
    iv.key = (iv.key + 1) % 100 ;
    iv.ws.send(JSON.stringify(msg))
  })
}

function sendPacks(z, packs, site, user){
  let sendPack = {
    command: "data01",
    site: site, //dbVals.site,
    user: user, // dbVals.user,
    zone: z,
    params: packs,
  }
  sendSocket(sendPack).then(r=>{
  });
}

var loginToWebSocket = (res, rej)=>{
  let token = getToken();
  let site = getSiteId();
  let pack = {
    command: "login00",
    token: token,
    site: site,

  };
  start("ws2");
  sendSocket(pack).then(r=>{// login
//     cl(show("ws2"));
    if (r.result === 'ok'){
//       cl("send socket return");
//       cl(r);
      pack = {
        command: "getcursite00",
        token: token,
        site: site,
      }
//       cl(pack);
      sendSocket(pack).then(r=>{// then, get the data
          // cl("ret");
        cl(stop("ws2"));
        // cl(r);
        dbVals.siteAuthorized = false;
        if(r.command === "gotcursite00"){

          putSite(r.cursite, null);
          dbVals.siteAuthorized = true;
        }
//         cl("putting site");

//         cl("send 2");
//         cl(r);
        res(r);
      });
    }
  })
}

// function onOpen(e){
var onOpen = (r, e)=>{
// on open, "login", by sending the JWT and the site that we want.
/* this is the hard part!
we want to wrap web socket communication in a Promise
so, when we send a request, it has a "key", then, when the response comes back, we look up the key, and */
//   cl("ws onOpen");
  loginToWebSocket(r, e);
}

function onClose(e){
  cl("ws onClose");
  cl(e)
}

// function onMessage(e){
//   cl("ws onMessage");
// //   cl("received: " + e.data)
//   let mo = JSON.parse(e.data);
//   cl(mo);
// //   putStream(JSON.parse(e.data));
// //   cl(mo);
// }

function onError(e){
  cl("ws onError");
  cl(e)
}

var sendWS = (pack)=>{
  cl(pack);
  iv.ws.send(JSON.stringify(pack))
//   cl(iv);
}

function openWS(uri){
/* this is to manage *the* websocket with the server*/
//  globs.ws = openWS("ws://l4ol.com:8765/");
//   testWs();
//   cl("open ws: " + uri);
  return new Promise((r, e)=>{
//     start("ws2");
    let ws = new WebSocket(uri);
    iv.ws = ws;
  //   cl("opening " + uri)
    ws.onopen = ()=>onOpen(r, e);
    ws.onclose = e=>onClose(e);
    ws.onmessage = e=>recvSocket(e);
    ws.onerror = e=>onError(e);
//     return ws;
  });
}

export {openWS, sendWS, sendPacks}
