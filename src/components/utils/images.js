// import bigTextures from '../../../images/MainClipfile.jpg'
import sensorBacks from '../../materials/sensorBacks.png';
// import {cl} from './utils';

var canvas3 = null, ctx;
// let cl = console.log;

// function loadTextures(){
//   if (canvas3 != null) return;
//   return new Promise((res, rej)=>{
//     canvas3 = document.createElement("CANVAS");
//     canvas3.width = 2048;
//     canvas3.height = 2048;
//     ctx = canvas3.getContext("2d")
//     var img2 = new Image(2048, 2048);
//     img2.src = bigTextures;
//     img2.onload = () => {
//       ctx.drawImage(img2, 0, 0);
//       // cl("loaded");
//       res(1);
//     }
//   });
// }

function loadSensorBacks(){
  if (canvas3 != null) return;
  return new Promise((res, rej)=>{
    canvas3 = document.createElement("CANVAS");
    canvas3.width = 1400;
    canvas3.height = 2500;
    ctx = canvas3.getContext("2d")
    var img2 = new Image(1400, 2500);
    img2.src = sensorBacks;
    img2.onload = () => {
      ctx.drawImage(img2, 0, 0);
      // cl("loaded");
      res(1);
    }
  });
}

function getSensorBack(id, w, h, color){
    // cl(r + ", " + g + ", " + b )
  let canvas = document.createElement("CANVAS");
  canvas.width=w;
  canvas.height=h;
  let ctx2 = canvas.getContext("2d");
  let x = 0 ;
  let y = 500 * id;
  // cl(x, y);
  // y = 0;
  var imgData = ctx.getImageData(x, y, 1400, 500);
  if (id < 64){
    // getRGB(color);
    let r = getCol(color, 0);
    let g = getCol(color, 1);
    let b = getCol(color, 2);
    // cl(r, g, b)
    var data=imgData.data;
    for (let i = 0 ; i < imgData.data.length ; i+=4){
      data[i] *= r ;
      data[i+1] *= g ;
      data[i+2] *= b ;
    }
  }
  ctx2.putImageData(imgData, 0, 0);
  return canvas.toDataURL("image/png");
}

function getCol(color, ind){
  return parseInt(color.substr(2 * ind, 2), 16) / 256;
}

// function getRGB(color){
//   let r1 = getCol(color, 0);
//   let g1 = getCol(color, 1);
//   let b1 = getCol(color, 2);
//   cl(r1 + ", " + g1 + ", " + b1 )
// }

// function getTexture(id, w, h, color){
//     // cl(r + ", " + g + ", " + b )
//   let canvas = document.createElement("CANVAS");
//   canvas.width=w;
//   canvas.height=h;
//   let ctx2 = canvas.getContext("2d");
//   let x = 128 * (id % 16) ;
//   let y = 128 * Math.floor(id / 16);
//   // cl(x, y);
//   // y = 0;
//   var imgData = ctx.getImageData(x, y, w, h);
//   if (id < 64){
//     // getRGB(color);
//     let r = getCol(color, 0);
//     let g = getCol(color, 1);
//     let b = getCol(color, 2);
//     var data=imgData.data;
//     for (let i = 0 ; i < imgData.data.length ; i+=4){
//       data[i] *= r ;
//       data[i+1] *= g ;
//       data[i+2] *= b ;
//     }
//   }
//   ctx2.putImageData(imgData, 0, 0);
//   return canvas.toDataURL("image/png");
// }

export {loadSensorBacks, getSensorBack}
