// import {cl} from './utils';

/*I don't think we're going to try to duplicate
the lc1800 database here. Instead, we'll come up
with a new interface for dealing with finding
values*/

/*table ids:
snaps		pb.snapshots = 0 // 1-81
snapChans	pb.snapshot_channels = 0 // 101-120
snapEcph	pb.snapshot_ecphs = 0 // 201-235
snapChanData	pb.snapshot_chan_Snapshot = 0 // 401-448
confChan	pb.channels_configuration = 0 // 501-520
confChanData	pb.config_channels_configuration = 0 // 600-796
snapAuxAlarms	pb.snapshot_aux_alarms = p.PID_BASE_AUX_ALARMS; // 1024
snapAuxPVars	pb.snapshot_aux_persistent_variables = p.PID_BASE_AUX_PERSISTENT_VARIABLE;//1056
snapAuxVars	pb.snapshot_aux_variables = p.PID_BASE_AUX_VARIABLE;//1088
snapXBoards	pb.snapshot_expansion_boards = p.PID_BASE_EXPANSION_BOARDS;//1152
confAuxAlarms	pb.config_aux_alarms = p.PID_BASE_CONFIG_AUX_ALARMS;//1216 4 * 32
confAuxConts	pb.config_aux_controls = p.PID_BASE_CONFIG_AUX_CONTROLS;//1344 17 * 128
confAuxPVars	pb.config_aux_persistent_variables = p.PID_BASE_CONFIG_AUX_PERSISTENT_VARIABLES;//3520 4 * 32
convAuxVars	pb.config_aux_variables = p.PID_BASE_CONFIG_AUX_VARIABLES;//3640 4 * 64
convXBoards	pb.config_expansion_boards = p.PID_BASE_CONFIG_EXPANSION_BOARDS;//3896 5 * 64
confContSets	pb.controller_configuration_settings = p.PID_BASE_CONFIG_CONTROLLER_SETTINGS;//4216
confEcph	pb.config_ecph = p.PID_BASE_CONFIG_ECPH;//4506
confEcphSens	pb.config_ecph_sensors = p.PID_BASE_CONFIG_ECPH_SENSORS;//4517
confSetpoints	pb.config_setpoints = p.PID_BASE_CONFIG_SETPOINTS;//4586
confZoneSets	pb.zone_configuration_settings = p.PID_BASE_CONFIG_ZONE_SETTINGS;//4666
confZones	pb.config_zones = p.PID_BASE_CONFIG_ZONES;//4972
confConts	pb.config_controllers = p.PID_BASE_CONFIG_CONTROLLERS;//4974
commStat	pb.config_communication_status = p.PID_BASE_CONFIG_COMM_STAT;//4978
*/

var tableIds = ["snaps", "snapChans", "snapEcph", "snapChanData",
  "confChan", "confChanData", "snapAuxAlarms", "snapAuxPVars",
  "snapAuxVars", "snapXBoards", "confAuxAlarms", "confAuxConts",
  "confAuxPVars", "confAuxVars", "confXBoards", "confContSets",
  "confEcph", "confEcphSens", "confSetpoints", "confZoneSets",
  "confZones", "confConts", "commStat"];
// var tableBases = [0, 100, 200, 400, 500, 600, 1024, 1056,
//   1088, 1152, 1216, 1344, 3520, 3640, 3896, 4216, 4556,
//   4567, 4636, 4716, 5022, 5024, 5028, 999999] ;




var p = {};
var pids =[];

p.PID_BASE_SNAPSHOTS = 0 // 81
p.PID_BASE_SNAP_CHANNELS = 100;
p.PID_BASE_SNAP_ECPHS = 200;
p.PID_BASE_SNAP_CHAN_DATA = 400;
p.PID_BASE_CONF_CHANNELS = 500;
p.PID_BASE_CONF_CHAN_DATA = 600;

p.PID_BASE_AUX_ALARMS = 1024 // 32
p.PID_BASE_AUX_PERSISTENT_VARIABLE = 1056 // 32
p.PID_BASE_AUX_VARIABLE = 1088 // 64
p.PID_BASE_EXPANSION_BOARDS = 1152 // 64
// p.PID_BASE_EXPANSION_BOARDS = 1216
p.PID_BASE_CONFIG_AUX_ALARMS = 1216 // 4 * 32
p.PID_BASE_CONFIG_AUX_CONTROLS = 1344 // 17 * 64 no! 128!
p.PID_BASE_CONFIG_AUX_PERSISTENT_VARIABLES = 3520 // 4 * 32
p.PID_BASE_CONFIG_AUX_VARIABLES = 3640 // 4 * 64
p.PID_BASE_CONFIG_EXPANSION_BOARDS = 3896 // 5 * 64
p.PID_BASE_CONFIG_CONTROLLER_SETTINGS = 4216 // 145 * 2, 170

// # the following have been +50, on 20190908
// PID_BASE_CONFIG_ECPH = 4556 # 11 - actually, 8 * 11
// PID_BASE_CONFIG_ECPH_SENSORS = 4567 # 3 * 23 = 69, actually, 8 * 69
// #4440#
// # zone wide
// PID_BASE_CONFIG_SETPOINTS = 4636 # 8 * 10
// PID_BASE_CONFIG_ZONE_SETTINGS = 4716 # 153 * 2!
// PID_BASE_CONFIG_ZONES = 5022 # 2
// PID_BASE_CONFIG_CONTROLLERS = 5024 # 4
// # site wide
// PID_BASE_CONFIG_COMM_STAT = 5028 # 3
//
// PID_ZONE_MIN = PID_BASE_CONFIG_ZONES # 5022
// PID_ZONE_MAX = PID_BASE_CONFIG_CONTROLLERS - 1 # 5023
// PID_SETPOINTS_MIN = PID_BASE_CONFIG_SETPOINTS # 4636
// PID_SETPOINTS_MAX = PID_BASE_CONFIG_ZONE_SETTINGS - 1 # 4715
// PID_CONTROLLERS_VERSION = PID_BASE_CONFIG_CONTROLLERS + 1 # 5025

p.PID_BASE_CONFIG_ECPH = 4556 // 11
p.PID_BASE_CONFIG_ECPH_SENSORS = 4567 // 3 * 23 = 69
//4440//
// zone wide
p.PID_BASE_CONFIG_SETPOINTS = 4636 // 8 * 10
p.PID_BASE_CONFIG_ZONE_SETTINGS = 4716 // 153 * 2!
p.PID_BASE_CONFIG_ZONES = 5022 // 2
p.PID_BASE_CONFIG_CONTROLLERS = 5024 // 4
// site wide
p.PID_BASE_CONFIG_COMM_STAT = 5028 // 3
p.PID_BASE_CONFIG_TIME = 5031 // 1 - current time at the server

p.PID_ZONE_NAME = 5022;
p.PID_ZONE_DESCRIPTION = 5023;
p.PID_CONTROLLER_VERSION = 5025;

var pb = {};// these are the id bases
pb.snapshots = 0 // 1-81
pb.snapshot_channels = 100 // 101-120
pb.snapshot_ecphs = 200 // 201-235
pb.snapshot_chan_Snapshot = 400 // 401-448
pb.channels_configuration = 500 // 501-520
pb.config_channels_configuration = 600 // 600-796
pb.snapshot_aux_alarms = p.PID_BASE_AUX_ALARMS; // 1024
pb.snapshot_aux_persistent_variables = p.PID_BASE_AUX_PERSISTENT_VARIABLE;//1056
pb.snapshot_aux_variables = p.PID_BASE_AUX_VARIABLE;//1088
pb.snapshot_expansion_boards = p.PID_BASE_EXPANSION_BOARDS;//1152
pb.config_aux_alarms = p.PID_BASE_CONFIG_AUX_ALARMS;//1216 4 * 32
pb.config_aux_controls = p.PID_BASE_CONFIG_AUX_CONTROLS;//1344 17 * 128
pb.config_aux_persistent_variables = p.PID_BASE_CONFIG_AUX_PERSISTENT_VARIABLES;//3520 4 * 32
pb.config_aux_variables = p.PID_BASE_CONFIG_AUX_VARIABLES;//3640 4 * 64
pb.config_expansion_boards = p.PID_BASE_CONFIG_EXPANSION_BOARDS;//3896 5 * 64
pb.controller_configuration_settings = p.PID_BASE_CONFIG_CONTROLLER_SETTINGS;//4216
pb.config_ecph = p.PID_BASE_CONFIG_ECPH;//4506
pb.config_ecph_sensors = p.PID_BASE_CONFIG_ECPH_SENSORS;//4517
pb.config_setpoints = p.PID_BASE_CONFIG_SETPOINTS;//4586
pb.zone_configuration_settings = p.PID_BASE_CONFIG_ZONE_SETTINGS;//4666
pb.config_zones = p.PID_BASE_CONFIG_ZONES;//4972
pb.config_controllers = p.PID_BASE_CONFIG_CONTROLLERS;//4974
pb.config_communication_status = p.PID_BASE_CONFIG_COMM_STAT;//4978

var pInd = {};// these are the index values
// types of organization:
// 0: zone, unit
// 1: zone, unit, channel
// 2: zone, unit, chaneel, tank
// 3: zone, unit, channel, tank, ecph sensor
// 4: zone
// 5: (site wide)

pInd.snapshots = [p.PID_BASE_SNAPSHOTS, 0, 0, 0] // 1-81: base, channel type, index size, index count. channel type 0 = "unit"
pInd.snapshot_channels = [p.PID_BASE_SNAP_CHANNELS, 1, 0, 0] // 101-120 channel type 1 = "channel" - zone, unit, chan
pInd.snapshot_ecphs = [p.PID_BASE_SNAP_ECPHS, 2, 0, 0] // 201-235// ecph channel type - put in 192-199, on ecphIndex
pInd.snapshot_chan_Snapshot = [p.PID_BASE_SNAP_CHAN_DATA, 1, 0, 0] // 401-448 channeldata channel type
pInd.channels_configuration = [p.PID_BASE_CONF_CHANNELS, 1, 0, 0] // 501-520 channel type
pInd.config_channels_configuration = [p.PID_BASE_CONF_CHAN_DATA, 1, 0, 0] // 600-796 channeldata
pInd.snapshot_aux_alarms = [p.PID_BASE_AUX_ALARMS, 0, 1, 32]; // 1024 + 32
pInd.snapshot_aux_persistent_variables = [p.PID_BASE_AUX_PERSISTENT_VARIABLE, 0, 1, 32];//1056 + 32
pInd.snapshot_aux_variables = [p.PID_BASE_AUX_VARIABLE, 0, 1, 64];//1088 + 64
pInd.snapshot_expansion_boards = [p.PID_BASE_EXPANSION_BOARDS, 0, 1, 64];//1152 + 64
pInd.config_aux_alarms = [p.PID_BASE_CONFIG_AUX_ALARMS, 0, 4, 32];//1216 4 * 32
pInd.config_aux_controls = [p.PID_BASE_CONFIG_AUX_CONTROLS, 0, 17, 128];//1344 17 * 128
pInd.config_aux_persistent_variables = [p.PID_BASE_CONFIG_AUX_PERSISTENT_VARIABLES, 0, 4, 32];//3520 4 * 32
pInd.config_aux_variables = [p.PID_BASE_CONFIG_AUX_VARIABLES, 0, 4, 64];//3640 4 * 64
pInd.config_expansion_boards = [p.PID_BASE_CONFIG_EXPANSION_BOARDS, 0, 5, 64];//3896 5 * 64
pInd.controller_configuration_settings = [p.PID_BASE_CONFIG_CONTROLLER_SETTINGS, 7, 2, 170];//4216 170 * 2
pInd.config_ecph = [p.PID_BASE_CONFIG_ECPH, 0, 11, 8];//4506 11 * 8
pInd.config_ecph_sensors = [p.PID_BASE_CONFIG_ECPH_SENSORS, 3, 23, 3];//4517 - channel type 3 - ecph double indexed
pInd.config_setpoints = [p.PID_BASE_CONFIG_SETPOINTS, 4, 10, 8];//4586 type 4 = zone wide
pInd.zone_configuration_settings = [p.PID_BASE_CONFIG_ZONE_SETTINGS, 6, 2, 153];//4666 zone *config* type
pInd.config_zones = [p.PID_BASE_CONFIG_ZONES, 4, 0, 0];//4972
pInd.config_controllers = [p.PID_BASE_CONFIG_CONTROLLERS, 0, 0, 0];//4974
pInd.config_communication_status = [p.PID_BASE_CONFIG_COMM_STAT, 5, 0, 0];//4978 type 5 - site wide

var tableBases2 = [
  pInd.snapshots,
  pInd.snapshot_channels,
  pInd.snapshot_ecphs,
  pInd.snapshot_chan_Snapshot,
  pInd.channels_configuration,
  pInd.config_channels_configuration,
  pInd.snapshot_aux_alarms,
  pInd.snapshot_aux_persistent_variables,
  pInd.snapshot_aux_variables,
  pInd.snapshot_expansion_boards,
  pInd.config_aux_alarms,
  pInd.config_aux_controls,
  pInd.config_aux_persistent_variables,
  pInd.config_aux_variables,
  pInd.config_expansion_boards,
  pInd.controller_configuration_settings,
  pInd.config_ecph,
  pInd.config_ecph_sensors,
  pInd.config_setpoints,
  pInd.zone_configuration_settings,
  pInd.config_zones,
  pInd.config_controllers,
  pInd.config_communication_status];

// var snapshots = {} // these are in channels 240 + u
var pi = {};
pi.snapshots = [];//0, 1-81
pi.snapshot_channels = [];//0, 101-120
pi.snapshot_ecphs = [];//0, 201-235
pi.snapshot_chan_Snapshot = [];//0, 401-448
pi.channels_configuration = [];//0 501-520
pi.config_channels_configuration = [];// 0, 600-796
pi.snapshot_aux_alarms = [];// 1024
pi.snapshot_aux_persistent_variables = [];// 1056
pi.snapshot_aux_variables = [];// 1088
pi.snapshot_expansion_boards = [];// 1152
pi.config_aux_alarms = [];// 1216
pi.config_aux_controls = [];// 1344
pi.config_aux_persistent_variables = [];// 3520
pi.config_aux_variables = [];// 3640
pi.config_expansion_boards = [];// 3896
pi.controller_configuration_settings = [];//4216
pi.config_ecph = [];//4506
pi.config_ecph_sensors = [];//4517
pi.config_setpoints = [];//4586
pi.zone_configuration_settings = [];//4666
pi.config_zones = [];//4972
pi.config_controllers = [];//4974
pi.config_communication_status = [];//4978

var offsetTables = [pi.snapshots, pi.snapshot_channels, pi.snapshot_ecphs,
  pi.snapshot_chan_Snapshot, pi.channels_configuration,
  pi.config_channels_configuration, pi.snapshot_aux_alarms,
  pi.snapshot_aux_persistent_variables, pi.snapshot_aux_variables,
  pi.snapshot_expansion_boards, pi.config_aux_alarms,
  pi.config_aux_controls, pi.config_aux_persistent_variables,
  pi.config_aux_variables, pi.config_expansion_boards,
  pi.controller_configuration_settings, pi.config_ecph,
  pi.config_ecph_sensors, pi.config_setpoints,
  pi.zone_configuration_settings, pi.config_zones, pi.config_controllers,
  pi.config_communication_status];

pi.snapshots["id"] =	1
pi.snapshots["zoneIndex"] =	2
pi.snapshots["unitIndex"] =	3
pi.snapshots["unix_timestamp(created)"] =	4
pi.snapshots["unix_timestamp(unitTime)"] =	5
pi.snapshots["temperatureStage"] =	6
pi.snapshots["humidityStage"] =	7
pi.snapshots["heatSetpoint"] =	8
pi.snapshots["coolSetpoint"] =	9
pi.snapshots["humidifySetpoint"] =	10
pi.snapshots["dehumidifySetpoint"] =	11
pi.snapshots["accumulator0"] =	12
pi.snapshots["accumulator1"] =	13
pi.snapshots["d2avgTotalAverage"] =	14
pi.snapshots["d2avgDayAverage"] =	15
pi.snapshots["d2avgNightAverage"] =	16
pi.snapshots["d2avgFailDays"] =	17
pi.snapshots["lowAlarm"] =	18
pi.snapshots["highAlarm"] =	19
pi.snapshots["lowInTemperatureAlarm"] =	20
pi.snapshots["highInTemperatureAlarm"] =	21
pi.snapshots["inTemperatureSensorAlarm"] =	22
pi.snapshots["inTemperature"] =	23
pi.snapshots["outTemperature"] =	24
pi.snapshots["localTemperature"] =	25
pi.snapshots["backupTemperature"] =	26
pi.snapshots["inHumidity"] =	27
pi.snapshots["outHumidity"] =	28
pi.snapshots["localHumidity"] =	29
pi.snapshots["differentialPressure"] =	30
pi.snapshots["co2"] =	31
pi.snapshots["inLight"] =	32
pi.snapshots["outLight"] =	33
pi.snapshots["windSpeed"] =	34
pi.snapshots["windDirection"] =	35
pi.snapshots["rain"] =	36
pi.snapshots["snow"] =	37
pi.snapshots["analogTemperature1"] =	38
pi.snapshots["analogTemperature2"] =	39
pi.snapshots["analogTemperature3"] =	40
pi.snapshots["analogTemperature4"] =	41
pi.snapshots["analogTemperature5"] =	42
pi.snapshots["ventPosition1"] =	43
pi.snapshots["ventPosition2"] =	44
pi.snapshots["ventPosition3"] =	45
pi.snapshots["ventPosition4"] =	46
pi.snapshots["ventPosition5"] =	47
pi.snapshots["soilMoisture1"] =	48
pi.snapshots["soilMoisture2"] =	49
pi.snapshots["soilMoisture3"] =	50
pi.snapshots["soilMoisture4"] =	51
pi.snapshots["soilMoisture5"] =	52
pi.snapshots["ecph1"] =	53
pi.snapshots["ecph2"] =	54
pi.snapshots["ecph3"] =	55
pi.snapshots["ecph4"] =	56
pi.snapshots["ecph5"] =	57
pi.snapshots["generic1"] =	58
pi.snapshots["generic2"] =	59
pi.snapshots["heatDemand"] =	60
pi.snapshots["coolDemand"] =	61
pi.snapshots["coolDemandPassive"] =	62
pi.snapshots["auxHeatSetpointStatus"] =	63
pi.snapshots["auxHeatSetpointValue"] =	64
pi.snapshots["auxCoolSetpointStatus"] =	65
pi.snapshots["auxCoolSetpointValue"] =	66
pi.snapshots["auxHumidifySetpointStatus"] =	67
pi.snapshots["auxHumidifySetpointValue"] =	68
pi.snapshots["auxDehumidifySetpointStatus"] =	69
pi.snapshots["auxDehumidifySetpointValue"] =	70
pi.snapshots["auxLowAlarmStatus"] =	71
pi.snapshots["auxLowAlarmValue"] =	72
pi.snapshots["auxHighAlarmStatus"] =	73
pi.snapshots["auxHighAlarmValue"] =	74
pi.snapshots["auxActiveCoolStatus"] =	75
pi.snapshots["auxActiveCoolValue"] =	76
pi.snapshots["auxPassiveCoolStatus"] =	77
pi.snapshots["auxPassiveCoolValue"] =	78
pi.snapshots["accvpd"] =	79
pi.snapshots["outTemperatureSecondary"] =	80
pi.snapshots["barometricPressure"] =   81

pi.snapshot_channels["snapshotId"] = 1
pi.snapshot_channels["channelIndex"] = 2
pi.snapshot_channels["channelData"] = 3
pi.snapshot_channels["position"] = 4
pi.snapshot_channels["relay"] = 5
pi.snapshot_channels["channelOverride"] = 6
pi.snapshot_channels["analogOutput"] = 7
pi.snapshot_channels["co2Setpoint"] = 8
pi.snapshot_channels["microzoneSetpoint"] = 9
pi.snapshot_channels["proportionalZoneLowSetpoint"] = 10
pi.snapshot_channels["proportionalZoneHighSetpoint"] = 11
pi.snapshot_channels["pumpPeristalticSetpoint"] = 12
pi.snapshot_channels["pumpPeristalticTankLevel"] = 13
pi.snapshot_channels["pumpPeristalticInjectedMilliliters"] = 14
pi.snapshot_channels["pumpPeristalticInjectedSeconds"] = 15
pi.snapshot_channels["lightDailyLightIntegral"] = 16
pi.snapshot_channels["auxOutputStatus"] = 17
pi.snapshot_channels["auxOutputValue"] = 18
pi.snapshot_channels["auxEquipmentStatus"] = 19
pi.snapshot_channels["auxEquipmentValue"] = 20

//var snapshot_ecphs = {} // these are stored in the ecph
// channels, 192-199
pi.snapshot_ecphs["snapshotId"] = 1
pi.snapshot_ecphs["ecphIndex"] = 2
pi.snapshot_ecphs["ec1"] = 3
pi.snapshot_ecphs["ec2"] = 4
pi.snapshot_ecphs["ec3"] = 5
pi.snapshot_ecphs["ec1SensorAlarm"] = 6
pi.snapshot_ecphs["ec1ServiceAlarm"] = 7
pi.snapshot_ecphs["ec1CalibrationAlarm"] = 8
pi.snapshot_ecphs["ec2SensorAlarm"] = 9
pi.snapshot_ecphs["ec2ServiceAlarm"] = 10
pi.snapshot_ecphs["ec2CalibrationAlarm"] = 11
pi.snapshot_ecphs["ec3SensorAlarm"] = 12
pi.snapshot_ecphs["ec3ServiceAlarm"] = 13
pi.snapshot_ecphs["ec3CalibrationAlarm"] = 14
pi.snapshot_ecphs["ecLowAlarm"] = 15
pi.snapshot_ecphs["ecHighAlarm"] = 16
pi.snapshot_ecphs["ecDeviationAlarm"] = 17
pi.snapshot_ecphs["ph1"] = 18
pi.snapshot_ecphs["ph2"] = 19
pi.snapshot_ecphs["ph3"] = 20
pi.snapshot_ecphs["ph1SensorAlarm"] = 21
pi.snapshot_ecphs["ph1ServiceAlarm"] = 22
pi.snapshot_ecphs["ph1CalibrationAlarm"] = 23
pi.snapshot_ecphs["ph2SensorAlarm"] = 24
pi.snapshot_ecphs["ph2ServiceAlarm"] = 25
pi.snapshot_ecphs["ph2CalibrationAlarm"] = 26
pi.snapshot_ecphs["ph3SensorAlarm"] = 27
pi.snapshot_ecphs["ph3ServiceAlarm"] = 28
pi.snapshot_ecphs["ph3CalibrationAlarm"] = 29
pi.snapshot_ecphs["phLowAlarm"] = 30
pi.snapshot_ecphs["phHighAlarm"] = 31
pi.snapshot_ecphs["phDeviationAlarm"] = 32
pi.snapshot_ecphs["temperature1"] = 33
pi.snapshot_ecphs["temperature2"] = 34
pi.snapshot_ecphs["temperature3"] = 35

//var snapshot_chan_Snapshot = {} // stored in the Channel ID
// these are the values out of ChannelData
pi.snapshot_chan_Snapshot["accumulatedLight"] = 1
pi.snapshot_chan_Snapshot["accumulator"] = 2
pi.snapshot_chan_Snapshot["accvpd"] = 3
pi.snapshot_chan_Snapshot["activatedTimes"] = 4
pi.snapshot_chan_Snapshot["activeCount"] = 5
pi.snapshot_chan_Snapshot["auxControl"] = 6
pi.snapshot_chan_Snapshot["coolDemand"] = 7
pi.snapshot_chan_Snapshot["coolDemandPassive"] = 8
pi.snapshot_chan_Snapshot["currentSetpoint"] = 9
pi.snapshot_chan_Snapshot["cycleOffTimeRemains"] = 10
pi.snapshot_chan_Snapshot["cycleOnTimeRemains"] = 11
pi.snapshot_chan_Snapshot["cycleState"] = 12
pi.snapshot_chan_Snapshot["dailyLightIntegral"] = 13
pi.snapshot_chan_Snapshot["driveToAverage"] = 14
pi.snapshot_chan_Snapshot["ecphs"] = 15
pi.snapshot_chan_Snapshot["equipment"] = 16
pi.snapshot_chan_Snapshot["expansionBoardStatus"] = 17
pi.snapshot_chan_Snapshot["heatDemand"] = 18
pi.snapshot_chan_Snapshot["highAlarm"] = 19
pi.snapshot_chan_Snapshot["highInTemperatureAlarm"] = 20
pi.snapshot_chan_Snapshot["humidityStage"] = 21
pi.snapshot_chan_Snapshot["injectedVolumeMLiters"] = 22
pi.snapshot_chan_Snapshot["injectedVolumeSeconds"] = 23
pi.snapshot_chan_Snapshot["inQueue"] = 24
pi.snapshot_chan_Snapshot["inTemperatureSensorAlarm"] = 25
pi.snapshot_chan_Snapshot["inWindow"] = 26
pi.snapshot_chan_Snapshot["irrigationWeek"] = 27
pi.snapshot_chan_Snapshot["isCycling"] = 28
pi.snapshot_chan_Snapshot["isPulseOn"] = 29
pi.snapshot_chan_Snapshot["lowAlarm"] = 30
pi.snapshot_chan_Snapshot["lowInTemperatureAlarm"] = 31
pi.snapshot_chan_Snapshot["offTime"] = 32
pi.snapshot_chan_Snapshot["originallyOn"] = 33
pi.snapshot_chan_Snapshot["output"] = 34
pi.snapshot_chan_Snapshot["pdTimer"] = 35
pi.snapshot_chan_Snapshot["peristalticState"] = 36
pi.snapshot_chan_Snapshot["sensors"] = 37
pi.snapshot_chan_Snapshot["setpoint"] = 38
pi.snapshot_chan_Snapshot["setpointHigh"] = 39
pi.snapshot_chan_Snapshot["setpointLow"] = 40
pi.snapshot_chan_Snapshot["siteChanged"] = 41
pi.snapshot_chan_Snapshot["tankLevelLiters"] = 42
pi.snapshot_chan_Snapshot["tankSizeLiters"] = 43
pi.snapshot_chan_Snapshot["temperatureStage"] = 44
pi.snapshot_chan_Snapshot["timestamp"] = 45
pi.snapshot_chan_Snapshot["unitTime"] = 46
pi.snapshot_chan_Snapshot["pdTimerState"] = 47
pi.snapshot_chan_Snapshot["week"] = 248

//var channels_configuration = {}
pi.channels_configuration["zoneIndex"] = 1
pi.channels_configuration["unitIndex"] = 2
pi.channels_configuration["channelIndex"] = 3
pi.channels_configuration["unix_timestamp(modified)"] = 4
pi.channels_configuration["used"] = 5
pi.channels_configuration["isAnalog"] = 6
pi.channels_configuration["channelName"] = 7
pi.channels_configuration["channelType"] = 8
pi.channels_configuration["channelData"] = 9
pi.channels_configuration["timedEnabled"] = 10
pi.channels_configuration["timedInterval"] = 11
pi.channels_configuration["timedStartTime"] = 12
pi.channels_configuration["timedEndTime"] = 13
pi.channels_configuration["timedOutput"] = 14
pi.channels_configuration["stageDehumidfy1"] = 15
pi.channels_configuration["stageDehumidfy2"] = 16
pi.channels_configuration["stageColdDehumidfy"] = 17
pi.channels_configuration["stageHumidfy"] = 18
pi.channels_configuration["expansionFailSafeOutput"] = 19
pi.channels_configuration["userComment"] = 20

//var config_channels_configuration = {}
pi.config_channels_configuration["activeCool_cool1"] = 0
pi.config_channels_configuration["activeCool_cool2"] = 1
pi.config_channels_configuration["activeCool_cool3"] = 2
pi.config_channels_configuration["activeCool_cool4"] = 3
pi.config_channels_configuration["activeCool_cool5"] = 4
pi.config_channels_configuration["activeCool_cool6"] = 5
pi.config_channels_configuration["activeCool_heat1"] = 6
pi.config_channels_configuration["activeCool_heat2"] = 7
pi.config_channels_configuration["activeCool_heat3"] = 8
pi.config_channels_configuration["activeCool_heat4"] = 9
pi.config_channels_configuration["activeCool_heat5"] = 10
pi.config_channels_configuration["activeCool_heat6"] = 11
pi.config_channels_configuration["activeCool_normal"] = 12
pi.config_channels_configuration["activeTrigger"] = 13
pi.config_channels_configuration["advanced_close_perc"] = 14
pi.config_channels_configuration["advanced_shock_threshold"] = 15
pi.config_channels_configuration["advanced_stage"] = 16
pi.config_channels_configuration["advanced_time"] = 17
pi.config_channels_configuration["air_cool_setpoint"] = 18
pi.config_channels_configuration["air_heat_setpoint"] = 19
pi.config_channels_configuration["air_temp_sensor"] = 20
pi.config_channels_configuration["analog_max"] = 21
pi.config_channels_configuration["analog_max"] = 22
pi.config_channels_configuration["analog_min"] = 23
pi.config_channels_configuration["cool1"] = 24
pi.config_channels_configuration["cool2"] = 25
pi.config_channels_configuration["cool3"] = 26
pi.config_channels_configuration["cool4"] = 27
pi.config_channels_configuration["cool5"] = 28
pi.config_channels_configuration["cool6"] = 29
pi.config_channels_configuration["cooling_device"] = 30
pi.config_channels_configuration["cycleOffPeriod"] = 31
pi.config_channels_configuration["cycleOnPeriod"] = 32
pi.config_channels_configuration["D"] = 33
pi.config_channels_configuration["daytime_astroadjust"] = 34
pi.config_channels_configuration["daytime_enabled"] = 35
pi.config_channels_configuration["daytime_setpoint"] = 36
pi.config_channels_configuration["daytime_stage"] = 37
pi.config_channels_configuration["daytime_start"] = 38
pi.config_channels_configuration["deadband"] = 39
pi.config_channels_configuration["dir"] = 40
pi.config_channels_configuration["disable_inside_temp_pid"] = 41
pi.config_channels_configuration["dliEndTime"] = 42
pi.config_channels_configuration["dliEnergyOutput"] = 43
pi.config_channels_configuration["dliOnTime"] = 44
pi.config_channels_configuration["dliSensor"] = 45
pi.config_channels_configuration["dliStartTime"] = 46
pi.config_channels_configuration["dliThreshold"] = 47
pi.config_channels_configuration["enableActiveCoolStageOverride"] = 48
pi.config_channels_configuration["enableSmartInject"] = 49
pi.config_channels_configuration["end"] = 50
pi.config_channels_configuration["end_astroadjust"] = 51
pi.config_channels_configuration["end_time"] = 52
pi.config_channels_configuration["energy_end"] = 53
pi.config_channels_configuration["energy_end_astroadjust"] = 54
pi.config_channels_configuration["energy_start"] = 55
pi.config_channels_configuration["energy_start_astroadjust"] = 56
pi.config_channels_configuration["feedingMode"] = 57
pi.config_channels_configuration["fine_adjust_range"] = 58
pi.config_channels_configuration["followChannelIndex"] = 59
pi.config_channels_configuration["followRatio"] = 60
pi.config_channels_configuration["function"] = 61
pi.config_channels_configuration["function_float_max"] = 62
pi.config_channels_configuration["function_float_min"] = 63
pi.config_channels_configuration["function_int_max"] = 64
pi.config_channels_configuration["function_int_min"] = 65
pi.config_channels_configuration["gain"] = 66
pi.config_channels_configuration["heat_on_delay"] = 67
pi.config_channels_configuration["heat1"] = 68
pi.config_channels_configuration["heat2"] = 69
pi.config_channels_configuration["heat3"] = 70
pi.config_channels_configuration["heat4"] = 71
pi.config_channels_configuration["heat5"] = 72
pi.config_channels_configuration["heat6"] = 73
pi.config_channels_configuration["highlight_cover_perc"] = 74
pi.config_channels_configuration["highlight_cover_trigger_light"] = 75
pi.config_channels_configuration["highlight_cover_trigger_temp"] = 76
pi.config_channels_configuration["hold_delay"] = 77
pi.config_channels_configuration["I"] = 78
pi.config_channels_configuration["increased_co2_level"] = 79
pi.config_channels_configuration["injectingBatchTimeSeconds"] = 80
pi.config_channels_configuration["injectingBatchVolumeMilliLiters"] = 81
pi.config_channels_configuration["injectingLimitHours"] = 82
pi.config_channels_configuration["injectingLimitMilliLiters"] = 83
pi.config_channels_configuration["injectingLimitSeconds"] = 84
pi.config_channels_configuration["injectingTimeSeconds"] = 85
pi.config_channels_configuration["injectingVolumeMilliLiters"] = 86
pi.config_channels_configuration["injectionRate"] = 87
pi.config_channels_configuration["inside_temp_d"] = 88
pi.config_channels_configuration["inside_temp_i"] = 89
pi.config_channels_configuration["inside_temp_p"] = 90
pi.config_channels_configuration["irrigation_mode"] = 91
pi.config_channels_configuration["isCycling"] = 92
pi.config_channels_configuration["keep_open_threshold_light"] = 93
pi.config_channels_configuration["keep_open_threshold_temp"] = 94
pi.config_channels_configuration["light_drive_to"] = 95
pi.config_channels_configuration["light_level"] = 96
pi.config_channels_configuration["light_mode"] = 97
pi.config_channels_configuration["light_sensor_enabled"] = 98
pi.config_channels_configuration["light_threshold"] = 99
pi.config_channels_configuration["low_light_threshold_duration"] = 100
pi.config_channels_configuration["low_light_threshold_light"] = 101
pi.config_channels_configuration["mixingTimeSeconds"] = 102
pi.config_channels_configuration["mode"] = 103
pi.config_channels_configuration["nighttime_astroadjust"] = 104
pi.config_channels_configuration["nighttime_enabled"] = 105
pi.config_channels_configuration["nighttime_setpoint"] = 106
pi.config_channels_configuration["nighttime_stage"] = 107
pi.config_channels_configuration["nighttime_start"] = 108
pi.config_channels_configuration["normal"] = 109
pi.config_channels_configuration["normal_co2_level"] = 110
pi.config_channels_configuration["off_delay"] = 111
pi.config_channels_configuration["off_duration"] = 112
pi.config_channels_configuration["off_threshold"] = 113
pi.config_channels_configuration["offDelay"] = 114
pi.config_channels_configuration["offset"] = 115
pi.config_channels_configuration["on_duration"] = 116
pi.config_channels_configuration["on_threshold"] = 117
pi.config_channels_configuration["output_type"] = 118
pi.config_channels_configuration["override_action_target"] = 119
pi.config_channels_configuration["override_dir"] = 120
pi.config_channels_configuration["override_limit"] = 121
pi.config_channels_configuration["override_sensor"] = 122
pi.config_channels_configuration["override_trigger"] = 123
pi.config_channels_configuration["P"] = 124
pi.config_channels_configuration["passive_cooling"] = 125
pi.config_channels_configuration["probe_id"] = 126
pi.config_channels_configuration["probeSensorIndex"] = 127
pi.config_channels_configuration["probeType"] = 128
pi.config_channels_configuration["pulse_ff_time"] = 129
pi.config_channels_configuration["pulse_on_time"] = 130
pi.config_channels_configuration["pump"] = 131
pi.config_channels_configuration["pump_id"] = 132
pi.config_channels_configuration["pump_type"] = 133
pi.config_channels_configuration["pumpMeasurementUnits"] = 134
pi.config_channels_configuration["resetInjectionTracker"] = 135
pi.config_channels_configuration["sensor"] = 136
pi.config_channels_configuration["sensorHoldTime"] = 137
pi.config_channels_configuration["setpoint"] = 138
pi.config_channels_configuration["setting_1_threshold_light"] = 139
pi.config_channels_configuration["setting_1_threshold_temp"] = 140
pi.config_channels_configuration["setting_2_threshold_light"] = 141
pi.config_channels_configuration["setting_2_threshold_temp"] = 142
pi.config_channels_configuration["shade_end"] = 143
pi.config_channels_configuration["shade_end_astroadjust"] = 144
pi.config_channels_configuration["shade_start"] = 145
pi.config_channels_configuration["shade_start_astroadjust"] = 146
pi.config_channels_configuration["shock_protect_enabled"] = 147
pi.config_channels_configuration["shock_protect_open_valve_perc"] = 148
pi.config_channels_configuration["shock_protect_threshold"] = 149
pi.config_channels_configuration["shock_protect_time_closed"] = 150
pi.config_channels_configuration["shock_protect_water_sensor"] = 151
pi.config_channels_configuration["soil_moisture_input"] = 152
pi.config_channels_configuration["soil_moisture_threshold"] = 153
pi.config_channels_configuration["stage"] = 154
pi.config_channels_configuration["start"] = 155
pi.config_channels_configuration["start_astroadjust"] = 156
pi.config_channels_configuration["start_time"] = 157
pi.config_channels_configuration["start_time_1"] = 158
pi.config_channels_configuration["start_time_2"] = 159
pi.config_channels_configuration["start_time_3"] = 160
pi.config_channels_configuration["start_time_4"] = 161
pi.config_channels_configuration["start_time_5"] = 162
pi.config_channels_configuration["start_time_6"] = 163
pi.config_channels_configuration["supply_pump"] = 164
pi.config_channels_configuration["tankSensor"] = 165
pi.config_channels_configuration["tankSize"] = 166
pi.config_channels_configuration["temp_drive_to"] = 167
pi.config_channels_configuration["temp_sensor"] = 168
pi.config_channels_configuration["temp_threshold"] = 169
pi.config_channels_configuration["threshold"] = 170
pi.config_channels_configuration["time"] = 171
pi.config_channels_configuration["triggerDelay"] = 172
pi.config_channels_configuration["valve_open_time"] = 173
pi.config_channels_configuration["vpdacc_threshold"] = 174
pi.config_channels_configuration["water_temp_d"] = 175
pi.config_channels_configuration["water_temp_i"] = 176
pi.config_channels_configuration["water_temp_max"] = 177
pi.config_channels_configuration["water_temp_min"] = 178
pi.config_channels_configuration["water_temp_p"] = 179
pi.config_channels_configuration["water_temp_sensor"] = 180
pi.config_channels_configuration["week_a_fri"] = 181
pi.config_channels_configuration["week_a_mon"] = 182
pi.config_channels_configuration["week_a_sat"] = 183
pi.config_channels_configuration["week_a_sun"] = 184
pi.config_channels_configuration["week_a_thu"] = 185
pi.config_channels_configuration["week_a_tue"] = 186
pi.config_channels_configuration["week_a_wed"] = 187
pi.config_channels_configuration["week_b_fri"] = 188
pi.config_channels_configuration["week_b_mon"] = 189
pi.config_channels_configuration["week_b_sat"] = 190
pi.config_channels_configuration["week_b_sun"] = 191
pi.config_channels_configuration["week_b_thu"] = 192
pi.config_channels_configuration["week_b_tue"] = 193
pi.config_channels_configuration["week_b_wed"] = 194
//added
pi.config_channels_configuration["triggers"] = 195
pi.config_channels_configuration["smartInject"] = 196

pi.snapshot_aux_alarms["alarmValue"] = 0;;// 1024
pi.snapshot_aux_persistent_variables["variableValue"] = 0;;// 1056
pi.snapshot_aux_variables["variableValue"] = 0;;// 1056
pi.snapshot_expansion_boards["boardStatus"] = 0;;// 1056




//var config_aux_alarms = {}
pi.config_aux_alarms["alarmIndex"] = 0
pi.config_aux_alarms["alarmName"] = 1
pi.config_aux_alarms["allowGraphing"] = 2
pi.config_aux_alarms["userComment"] = 3

//var config_aux_controls = {}
pi.config_aux_controls["auxIndex"] = 0
pi.config_aux_controls["unix_timestamp(modified)"] = 1
pi.config_aux_controls["operand1Type"] = 2
pi.config_aux_controls["operand1Value"] = 3
pi.config_aux_controls["operand2Type"] = 4
pi.config_aux_controls["operand2Value"] = 5
pi.config_aux_controls["operand3Type"] = 6
pi.config_aux_controls["operand3Value"] = 7
pi.config_aux_controls["operator1"] = 8
pi.config_aux_controls["operator2"] = 9
pi.config_aux_controls["conditionSeconds"] = 10
pi.config_aux_controls["action"] = 11
pi.config_aux_controls["targetType"] = 12
pi.config_aux_controls["targetValue"] = 13
pi.config_aux_controls["actionParameter"] = 14
pi.config_aux_controls["actionHoldTime"] = 15
pi.config_aux_controls["userComment"] = 16

//var config_aux_persistent_variables = {}
// config_aux_persistent_variables["zoneIndex"] = 827
// config_aux_persistent_variables["unitIndex"] = 828
pi.config_aux_persistent_variables["variableIndex"] = 0
pi.config_aux_persistent_variables["variableName"] = 1
pi.config_aux_persistent_variables["allowGraphing"] = 2
pi.config_aux_persistent_variables["userComment"] = 3

//var config_aux_variables = {}
// axxx["zoneIndex"] = 834
// axxx["unitIndex"] = 835
pi.config_aux_variables["variableIndex"] = 0
pi.config_aux_variables["variableName"] = 1
pi.config_aux_variables["allowGraphing"] = 2
pi.config_aux_variables["userComment"] = 3

//var config_expansion_boards = {}
// zxx["zoneIndex"] =
// zxx["unitIndex"] =
pi.config_expansion_boards["boardIndex"] = 0 // 64
pi.config_expansion_boards["unix_timestamp(modified)"] = 1
pi.config_expansion_boards["boardType"] = 2
pi.config_expansion_boards["address"] = 3
pi.config_expansion_boards["startChannelIndex"] = 4

//var config_setpoints = {}
// zxx["zoneIndex"] =
pi.config_setpoints["setpointIndex"] = 0 // 8 * 10
pi.config_setpoints["unix_timestamp(modified)"] = 1
pi.config_setpoints["enabled"] = 2
pi.config_setpoints["startTimeOfDay"] = 3
pi.config_setpoints["astroAdjust"] = 4
pi.config_setpoints["rampMinutes"] = 5
pi.config_setpoints["heatSetpoint"] = 6
pi.config_setpoints["coolSetpoint"] = 7
pi.config_setpoints["humidifySetpoint"] = 8
pi.config_setpoints["dehumidifySetpoint"] = 9

//var config_ecph = {}
// zzz["zoneIndex"] =
// zzz["unitIndex"] =
pi.config_ecph["ecphIndex"] = 0 // 8 * 11 of these
pi.config_ecph["unix_timestamp(modified)"] = 1
pi.config_ecph["name"] = 2
pi.config_ecph["ecType"] = 3
pi.config_ecph["alarmHoldTime"] = 4
pi.config_ecph["lowECThreshold"] = 5
pi.config_ecph["highECThreshold"] = 6
pi.config_ecph["highECDeviationThreshold"] = 7
pi.config_ecph["lowPHThreshold"] = 8
pi.config_ecph["highPHThreshold"] = 9
pi.config_ecph["highPHDeviationThreshold"] = 10

//var config_ecph_sensors = {}
// config_ecph_sensors["zoneIndex"] =
// config_ecph_sensors["unitIndex"] =
// config_ecph_sensors["ecphIndex"] =
// config_ecph_sensors["sensorIndex"] =
pi.config_ecph_sensors["(modified)"] = 0
pi.config_ecph_sensors["ecMapping"] = 1
pi.config_ecph_sensors["ecServiceIntervalDays"] = 2
pi.config_ecph_sensors["ecServiceTime"] = 3
pi.config_ecph_sensors["ecCalibrationIntervalDays"] = 4
pi.config_ecph_sensors["ecCalibrationTime"] = 5
pi.config_ecph_sensors["ecCalibration1Value"] = 6
pi.config_ecph_sensors["ecCalibration1Raw"] = 7
pi.config_ecph_sensors["ecCalibration2Value"] = 8
pi.config_ecph_sensors["ecCalibration2Raw"] = 9
pi.config_ecph_sensors["phMapping"] = 10
pi.config_ecph_sensors["phServiceIntervalDays"] = 11
pi.config_ecph_sensors["phServiceTime"] = 12
pi.config_ecph_sensors["phCalibrationIntervalDays"] = 13
pi.config_ecph_sensors["phCalibrationTime"] = 14
pi.config_ecph_sensors["phCalibration1Value"] = 15
pi.config_ecph_sensors["phCalibration1Raw"] = 16
pi.config_ecph_sensors["phCalibration2Value"] = 17
pi.config_ecph_sensors["phCalibration2Raw"] = 18
pi.config_ecph_sensors["temperatureMapping"] = 19
pi.config_ecph_sensors["temperatureCalibration"] = 20
pi.config_ecph_sensors["temperatureCompensationMode"] = 21
pi.config_ecph_sensors["temperatureCompensationValue"] = 22

//var config_zones = {}
// config_zones["zoneIndex"] =
pi.config_zones["name"] = 0
pi.config_zones["description"] = 1

//var config_controllers = {}
// config_controllers["zoneIndex"] =
// config_controllers["unitIndex"] =
pi.config_controllers["address"] = 0
pi.config_controllers["igrowVersion"] = 1
pi.config_controllers["isConnected"] = 2
pi.config_controllers["isInNetwork"] = 3

//var config_communication_status = {}
pi.config_communication_status["statusID"] = 0
pi.config_communication_status["unix_timestamp(statusTime)"] = 1
pi.config_communication_status["statusLog"] = 2

//var controller_configuration_settings = {}
pi.controller_configuration_settings["Inside Temperature Mapping"] = 1
pi.controller_configuration_settings["Relative Humidity Mapping"] = 2
pi.controller_configuration_settings["Local Temperature Mapping"] = 3
pi.controller_configuration_settings["Local Humidity Mapping"] = 4
pi.controller_configuration_settings["Outside Temperature Mapping"] = 5
pi.controller_configuration_settings["Outside Humidity Mapping"] = 6
pi.controller_configuration_settings["CO2 Mapping"] = 7
pi.controller_configuration_settings["Outside Light Mapping"] = 8
pi.controller_configuration_settings["Wind Speed Mapping"] = 9
pi.controller_configuration_settings["Wind Direction Mapping"] = 10
pi.controller_configuration_settings["Rain Mapping"] = 11
pi.controller_configuration_settings["Analog Temperature 1 Mapping"] = 12
pi.controller_configuration_settings["Analog Temperature 2 Mapping"] = 13
pi.controller_configuration_settings["Analog Temperature 3 Mapping"] = 14
pi.controller_configuration_settings["Analog Temperature 4 Mapping"] = 15
pi.controller_configuration_settings["Analog Temperature 5 Mapping"] = 16
pi.controller_configuration_settings["Soil Moisture 1 Mapping"] = 17
pi.controller_configuration_settings["Soil Moisture 2 Mapping"] = 18
pi.controller_configuration_settings["Soil Moisture 3 Mapping"] = 19
pi.controller_configuration_settings["Soil Moisture 4 Mapping"] = 20
pi.controller_configuration_settings["Soil Moisture 5 Mapping"] = 21
pi.controller_configuration_settings["Vent Position Sensor 1 Mapping"] = 22
pi.controller_configuration_settings["Vent Position Sensor 2 Mapping"] = 23
pi.controller_configuration_settings["Vent Position Sensor 3 Mapping"] = 24
pi.controller_configuration_settings["Vent Position Sensor 4 Mapping"] = 25
pi.controller_configuration_settings["Vent Position Sensor 5 Mapping"] = 26
pi.controller_configuration_settings["EC/pH 1 Mapping"] = 27
pi.controller_configuration_settings["EC/pH 2 Mapping"] = 28
pi.controller_configuration_settings["EC/pH 3 Mapping"] = 29
pi.controller_configuration_settings["EC/pH 4 Mapping"] = 30
pi.controller_configuration_settings["EC/pH 5 Mapping"] = 31
pi.controller_configuration_settings["EC/pH 1 Probe Type"] = 32
pi.controller_configuration_settings["EC/pH 2 Probe Type"] = 33
pi.controller_configuration_settings["EC/pH 3 Probe Type"] = 34
pi.controller_configuration_settings["EC/pH 4 Probe Type"] = 35
pi.controller_configuration_settings["EC/pH 5 Probe Type"] = 36
pi.controller_configuration_settings["Generic 1 Mapping"] = 37
pi.controller_configuration_settings["Generic 2 Mapping"] = 38
pi.controller_configuration_settings["Inside Temperature Calibration"] = 39
pi.controller_configuration_settings["Relative Humidity Calibration"] = 40
pi.controller_configuration_settings["Outside Temperature Calibration"] = 41
pi.controller_configuration_settings["Outside Humidity Calibration"] = 42
pi.controller_configuration_settings["CO2 Calibration"] = 43
pi.controller_configuration_settings["Outside Light Calibration"] = 44
pi.controller_configuration_settings["Wind Speed Calibration"] = 45
pi.controller_configuration_settings["Fallback Temperature Calibration"] = 46
pi.controller_configuration_settings["Analog Temperature 1 Calibration"] = 47
pi.controller_configuration_settings["Analog Temperature 2 Calibration"] = 48
pi.controller_configuration_settings["Analog Temperature 3 Calibration"] = 49
pi.controller_configuration_settings["Analog Temperature 4 Calibration"] = 50
pi.controller_configuration_settings["Analog Temperature 5 Calibration"] = 51
pi.controller_configuration_settings["Soil Mositure 1 Calibration"] = 52
pi.controller_configuration_settings["Soil Mositure 2 Calibration"] = 53
pi.controller_configuration_settings["Soil Mositure 3 Calibration"] = 54
pi.controller_configuration_settings["Soil Mositure 4 Calibration"] = 55
pi.controller_configuration_settings["Soil Mositure 5 Calibration"] = 56
pi.controller_configuration_settings["Vent Position Sensor 1 Calibration - Adjust"] = 57
pi.controller_configuration_settings["Vent Position Sensor 2 Calibration - Adjust"] = 58
pi.controller_configuration_settings["Vent Position Sensor 3 Calibration - Adjust"] = 59
pi.controller_configuration_settings["Vent Position Sensor 4 Calibration - Adjust"] = 60
pi.controller_configuration_settings["Vent Position Sensor 5 Calibration - Adjust"] = 61
pi.controller_configuration_settings["EC/pH 1 Calibration - Zero"] = 62
pi.controller_configuration_settings["EC/pH 2 Calibration - Zero"] = 63
pi.controller_configuration_settings["EC/pH 3 Calibration - Zero"] = 64
pi.controller_configuration_settings["EC/pH 4 Calibration - Zero"] = 65
pi.controller_configuration_settings["EC/pH 5 Calibration - Zero"] = 66
pi.controller_configuration_settings["EC/pH 1 Calibration - Gain"] = 67
pi.controller_configuration_settings["EC/pH 2 Calibration - Gain"] = 68
pi.controller_configuration_settings["EC/pH 3 Calibration - Gain"] = 69
pi.controller_configuration_settings["EC/pH 4 Calibration - Gain"] = 70
pi.controller_configuration_settings["EC/pH 5 Calibration - Gain"] = 71
pi.controller_configuration_settings["EC/pH 1 Calibration - Given Gain"] = 72
pi.controller_configuration_settings["EC/pH 2 Calibration - Given Gain"] = 73
pi.controller_configuration_settings["EC/pH 3 Calibration - Given Gain"] = 74
pi.controller_configuration_settings["EC/pH 4 Calibration - Given Gain"] = 75
pi.controller_configuration_settings["EC/pH 5 Calibration - Given Gain"] = 76
pi.controller_configuration_settings["Generic 1 Calibration"] = 77
pi.controller_configuration_settings["Generic 2 Calibration"] = 78
pi.controller_configuration_settings["CO2 Least Significant Bit"] = 79
pi.controller_configuration_settings["Light Multiplier"] = 80
pi.controller_configuration_settings["Generic 1 Multiplier"] = 81
pi.controller_configuration_settings["Generic 2 Multiplier"] = 82
pi.controller_configuration_settings["Local/Remote Setpoints"] = 83
pi.controller_configuration_settings["Enable Bump Vents"] = 84
pi.controller_configuration_settings["Bump Intervals Minutes)"] = 85
pi.controller_configuration_settings["Spike Temperature Delta"] = 86
pi.controller_configuration_settings["Spike Temperature Hold Time Exception"] = 87
pi.controller_configuration_settings["Outside Temperature 2 Mapping"] = 88
pi.controller_configuration_settings["Outside Temperature 2 Calibration"] = 89
pi.controller_configuration_settings["Barometric Pressure Mapping"] = 90
pi.controller_configuration_settings["Barometric Pressure Calibration"] = 91
pi.controller_configuration_settings["Enable expansion board"] = 92
pi.controller_configuration_settings["Autodetect Mode"] = 93
pi.controller_configuration_settings["Heat Demand Offset"] = 94
pi.controller_configuration_settings["Cool Demand Offset"] = 95
pi.controller_configuration_settings["Generic Sensor 1 Range"] = 96
pi.controller_configuration_settings["Generic Sensor 1 Units"] = 97
pi.controller_configuration_settings["Generic Sensor 1 Filter Max Samples"] = 98
pi.controller_configuration_settings["Generic Sensor 2 Range"] = 99
pi.controller_configuration_settings["Generic Sensor 2 Units"] = 100
pi.controller_configuration_settings["Generic PID Multipliers P"] = 101
pi.controller_configuration_settings["Generic PID Multipliers I"] = 102
pi.controller_configuration_settings["Generic PID Multipliers D"] = 103
pi.controller_configuration_settings["Expansion Output 1 Type"] = 109
pi.controller_configuration_settings["Expansion Output 2 Type"] = 110
pi.controller_configuration_settings["Expansion Input 1 Type"] = 111
pi.controller_configuration_settings["Expansion Input 2 Type"] = 112
pi.controller_configuration_settings["Sensor Delay"] = 113
pi.controller_configuration_settings["Vent Position 1 Calibration - Open"] = 114
pi.controller_configuration_settings["Vent Position 2 Calibration - Open"] = 115
pi.controller_configuration_settings["Vent Position 3 Calibration - Open"] = 116
pi.controller_configuration_settings["Vent Position 4 Calibration - Open"] = 117
pi.controller_configuration_settings["Vent Position 5 Calibration - Open"] = 118
pi.controller_configuration_settings["Vent Position 1 Calibration - Close"] = 119
pi.controller_configuration_settings["Vent Position 2 Calibration - Close"] = 120
pi.controller_configuration_settings["Vent Position 3 Calibration - Close"] = 121
pi.controller_configuration_settings["Vent Position 4 Calibration - Close"] = 122
pi.controller_configuration_settings["Vent Position 5 Calibration - Close"] = 123
pi.controller_configuration_settings["Inside Light Mapping"] = 124
pi.controller_configuration_settings["Differential Pressure Mapping"] = 125
pi.controller_configuration_settings["Snow Mapping"] = 126
pi.controller_configuration_settings["Inside Light Calibration"] = 127
pi.controller_configuration_settings["Differential Pressure Calibration"] = 128
pi.controller_configuration_settings["Accumulator 1 - Sensor"] = 129
pi.controller_configuration_settings["Accumulator 1 - Operating Period"] = 130
pi.controller_configuration_settings["Accumulator 2 - Sensor"] = 131
pi.controller_configuration_settings["Accumulator 2 - Operating Period"] = 132
pi.controller_configuration_settings["Generic Sensor 2 Filter Max Samples"] = 133
pi.controller_configuration_settings["Enable Loud Vent"] = 134
pi.controller_configuration_settings["Irrigation Trigger Input Mapping"] = 135
pi.controller_configuration_settings["Fallback Sensor Input Mapping"] = 136
pi.controller_configuration_settings["Enable Expansion Board"] = 137
pi.controller_configuration_settings["Enable Loud Vent"] = 138
pi.controller_configuration_settings["Wind Direction Calibration"] = 139
pi.controller_configuration_settings["Rain Calibration"] = 140
pi.controller_configuration_settings["Snow Calibration"] = 141
pi.controller_configuration_settings["Inside Light Multiplier"] = 142
pi.controller_configuration_settings["Canopy Sensor Mapping"] = 143
pi.controller_configuration_settings["Canopy Sensor Calibration"] = 144

//var zone_configuration_settings = {} each of these has *2* ids
pi.zone_configuration_settings["Use Fallback Sensor if In Temp Fails"] = 1
pi.zone_configuration_settings["Fallback Sensor Failed Temperature Stage"] = 3
pi.zone_configuration_settings["Force To No Sensor Stage setting 3) if High Alarm occurs"] = 4
pi.zone_configuration_settings["Rain Hold Time"] = 5
pi.zone_configuration_settings["Command Delay Time"] = 6
pi.zone_configuration_settings["Daylight Savings Time"] = 7
pi.zone_configuration_settings["Log History"] = 8
pi.zone_configuration_settings["Latitude"] = 9
pi.zone_configuration_settings["Longitude"] = 10
pi.zone_configuration_settings["Temperature Units"] = 11
pi.zone_configuration_settings["Windspeed Units"] = 12
pi.zone_configuration_settings["Light Units"] = 13
pi.zone_configuration_settings["Irrigation Mode"] = 14
pi.zone_configuration_settings["Irrigation Delay"] = 15
pi.zone_configuration_settings["High Alarm Temperature Above Cool Setpoint Threshold"] = 17
pi.zone_configuration_settings["High Alarm Temperature Hold Time"] = 18
pi.zone_configuration_settings["Low Alarm Temperature Below Heat Setpoint Threshold"] = 19
pi.zone_configuration_settings["Low Alarm Temperature Hold Time"] = 20
pi.zone_configuration_settings["Cool Deadband"] = 21
pi.zone_configuration_settings["Heat Deadband"] = 22
pi.zone_configuration_settings["Humidity Deadband"] = 23
pi.zone_configuration_settings["Analog Temperature Deadband"] = 24
pi.zone_configuration_settings["Enable SmartCool"] = 25
pi.zone_configuration_settings["SmartCool Setting"] = 26
pi.zone_configuration_settings["Show Heat Demand on iGrow"] = 32
pi.zone_configuration_settings["Heat Demand Max Light"] = 33
pi.zone_configuration_settings["Heat Demand Max Wind"] = 34
pi.zone_configuration_settings["Greenhouse heating design delta-T"] = 35
pi.zone_configuration_settings["Light''s influence on Heat Demand"] = 36
pi.zone_configuration_settings["Wind''s influence on Heat Demand"] = 37
pi.zone_configuration_settings["Show Cool Demand on iGrow"] = 38
pi.zone_configuration_settings["Cool Demand Max Light"] = 39
pi.zone_configuration_settings["Greenhouse cooling design delta-T"] = 40
pi.zone_configuration_settings["Cool Demand Light Factor"] = 41
pi.zone_configuration_settings["Enable Active Cooling"] = 42
pi.zone_configuration_settings["Passive Lock Stage"] = 43
pi.zone_configuration_settings["Enter Active Cooling Cool Demand Threshold"] = 44
pi.zone_configuration_settings["Exit Active Cooling Cool Demand Threshold"] = 45
pi.zone_configuration_settings["Cool stages"] = 46
pi.zone_configuration_settings["Heat stages"] = 47
pi.zone_configuration_settings["Stage Width"] = 48
pi.zone_configuration_settings["Stage Override 1 Enabled"] = 49
pi.zone_configuration_settings["Stage Override 1 Temperature Stage"] = 50
pi.zone_configuration_settings["Stage Override 1 Start Time"] = 51
pi.zone_configuration_settings["Stage Override 1 End Time"] = 52
pi.zone_configuration_settings["Stage Override 1 Interval"] = 53
pi.zone_configuration_settings["Stage Override 2 Enabled"] = 54
pi.zone_configuration_settings["Stage Override 2 Temperature Stage"] = 55
pi.zone_configuration_settings["Stage Override 2 Start Time"] = 56
pi.zone_configuration_settings["Stage Override 2 End Time"] = 57
pi.zone_configuration_settings["Stage Override 2 Interval"] = 58
pi.zone_configuration_settings["Stage Override 3 Enabled"] = 59
pi.zone_configuration_settings["Stage Override 3 Temperature Stage"] = 60
pi.zone_configuration_settings["Stage Override 3 Start Time"] = 61
pi.zone_configuration_settings["Stage Override 3 End Time"] = 62
pi.zone_configuration_settings["Stage Override 3 Interval"] = 63
pi.zone_configuration_settings["Stage Override 4 Enabled"] = 64
pi.zone_configuration_settings["Stage Override 4 Temperature Stage"] = 65
pi.zone_configuration_settings["Stage Override 4 Start Time"] = 66
pi.zone_configuration_settings["Stage Override 4 End Time"] = 67
pi.zone_configuration_settings["Stage Override 4 Interval"] = 68
pi.zone_configuration_settings["Vapor Pressure Deficit Media Sensor"] = 69
pi.zone_configuration_settings["Lighting Cyclic Mode"] = 70
pi.zone_configuration_settings["Lighting Start Delay"] = 71
pi.zone_configuration_settings["Lighting Finish Delay"] = 72
pi.zone_configuration_settings["Lighting Active Time"] = 73
pi.zone_configuration_settings["Lighting Minimum On Time"] = 74
pi.zone_configuration_settings["Drive to Average - Enable"] = 75
pi.zone_configuration_settings["Drive to Average - Target Temperature"] = 76
pi.zone_configuration_settings["Drive to Average - Maximum Failed Days"] = 77
pi.zone_configuration_settings["Drive to Average - Deviated Temperature Threshold"] = 78
pi.zone_configuration_settings["Drive to Average - Setpoint Correction"] = 79
pi.zone_configuration_settings["Cool Setpoint Influence Factor - Enable"] = 80
pi.zone_configuration_settings["Cool Setpoint Influence Factor - Sensor"] = 81
pi.zone_configuration_settings["Cool Setpoint Influence Factor - Upper Threshold"] = 82
pi.zone_configuration_settings["Cool Setpoint Influence Factor - Upper Offset"] = 83
pi.zone_configuration_settings["Cool Setpoint Influence Factor - Lower Threshold"] = 84
pi.zone_configuration_settings["Cool Setpoint Influence Factor - Lower Offset"] = 85
pi.zone_configuration_settings["Heat Setpoint Influence Factor - Enable"] = 86
pi.zone_configuration_settings["Heat Setpoint Influence Factor - Sensor"] = 87
pi.zone_configuration_settings["Heat Setpoint Influence Factor - Upper Threshold"] = 88
pi.zone_configuration_settings["Heat Setpoint Influence Factor - Upper Offset"] = 89
pi.zone_configuration_settings["Heat Setpoint Influence Factor - Lower Threshold"] = 90
pi.zone_configuration_settings["Heat Setpoint Influence Factor - Lower Offset"] = 91
pi.zone_configuration_settings["Start Up Delay"] = 92
pi.zone_configuration_settings["Curtain Energy Mode - Light Deadband"] = 93
pi.zone_configuration_settings["Curtain Energy Mode - Temperature Deadband"] = 94
pi.zone_configuration_settings["Humidity Override On Time"] = 95
pi.zone_configuration_settings["Humidity Override Off Time"] = 96
pi.zone_configuration_settings["Dehumidify Low Outside Temperature Threshold"] = 97
pi.zone_configuration_settings["Dehumidify Heat Boost"] = 98
pi.zone_configuration_settings["Enable Demands Based on Active Cool"] = 99
pi.zone_configuration_settings["Enable Active Cool Stage Override"] = 100
pi.zone_configuration_settings["SmartCool[0] Ku"] = 101
pi.zone_configuration_settings["SmartCool[0] Gmax"] = 102
pi.zone_configuration_settings["SmartCool[0] Kd"] = 103
pi.zone_configuration_settings["SmartCool[0] Gmin"] = 104
pi.zone_configuration_settings["SmartCool[1] Ku"] = 105
pi.zone_configuration_settings["SmartCool[1] Gmax"] = 106
pi.zone_configuration_settings["SmartCool[1] Kd"] = 107
pi.zone_configuration_settings["SmartCool[1] Gmin"] = 108
pi.zone_configuration_settings["SmartCool[2] Ku"] = 109
pi.zone_configuration_settings["SmartCool[2] Gmax"] = 110
pi.zone_configuration_settings["SmartCool[2] Kd"] = 111
pi.zone_configuration_settings["SmartCool[2] Gmin"] = 112
pi.zone_configuration_settings["SmartCool[3] Ku"] = 113
pi.zone_configuration_settings["SmartCool[3] Gmax"] = 114
pi.zone_configuration_settings["SmartCool[3] Kd"] = 115
pi.zone_configuration_settings["SmartCool[3] Gmin"] = 116
pi.zone_configuration_settings["SmartCool[4] Ku"] = 117
pi.zone_configuration_settings["SmartCool[4] Gmax"] = 118
pi.zone_configuration_settings["SmartCool[4] Kd"] = 119
pi.zone_configuration_settings["SmartCool[4] Gmin"] = 120
pi.zone_configuration_settings["SmartCool[5] Ku"] = 121
pi.zone_configuration_settings["SmartCool[5] Gmax"] = 122
pi.zone_configuration_settings["SmartCool[5] Kd"] = 123
pi.zone_configuration_settings["SmartCool[5] Gmin"] = 124
pi.zone_configuration_settings["SmartCool[6] Ku"] = 125
pi.zone_configuration_settings["SmartCool[6] Gmax"] = 126
pi.zone_configuration_settings["SmartCool[6] Kd"] = 127
pi.zone_configuration_settings["SmartCool[6] Gmin"] = 128
pi.zone_configuration_settings["SmartCool[7] Ku"] = 129
pi.zone_configuration_settings["SmartCool[7] Gmax"] = 130
pi.zone_configuration_settings["SmartCool[7] Kd"] = 131
pi.zone_configuration_settings["SmartCool[7] Gmin"] = 132
pi.zone_configuration_settings["SmartCool[8] Ku"] = 133
pi.zone_configuration_settings["SmartCool[8] Gmax"] = 134
pi.zone_configuration_settings["SmartCool[8] Kd"] = 135
pi.zone_configuration_settings["SmartCool[8] Gmin"] = 136
pi.zone_configuration_settings["SmartCool[9] Ku"] = 137
pi.zone_configuration_settings["SmartCool[9] Gmax"] = 138
pi.zone_configuration_settings["SmartCool[9] Kd"] = 139
pi.zone_configuration_settings["SmartCool[9] Gmin"] = 140
pi.zone_configuration_settings["Peristaltic - Output Mode"] = 141
pi.zone_configuration_settings["Peristaltic - Output Delay"] = 142
pi.zone_configuration_settings["Volume Measurement Units"] = 143
pi.zone_configuration_settings["Nutrient Units"] = 144
pi.zone_configuration_settings["Nutrient Units - TDS Conversion Factor"] = 145
pi.zone_configuration_settings["Equipment Delay"] = 146
pi.zone_configuration_settings["Sensor Display - 1"] = 147
pi.zone_configuration_settings["Sensor Display - 2"] = 148
pi.zone_configuration_settings["Sensor Display - 3"] = 149
pi.zone_configuration_settings["Sensor Display - 4"] = 150
pi.zone_configuration_settings["Schedule Pump Transition Time"] = 151
pi.zone_configuration_settings["Peristaltic Pump - Advanced Mode"] = 152


// function getParamIds(){
//   cl(pi.config_setpoints);
//   for (var key in pi.config_setpoints){
//     pids[p.PID_BASE_CONFIG_SETPOINTS +
//       pi.config_setpoints[key]] = key;
//   }
//   cl(pids);
//
// }

export {p, pb, pInd, pi, pids, tableIds, tableBases2, offsetTables}
