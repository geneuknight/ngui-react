import React from 'react';
import {dbVals} from './utils/http';
// import {cl} from './utils/utils';
class TestSetpoint extends React.Component{

  constructor(props){
      super(props);
      this.state = {
        setpoint: dbVals.zones[0].setpoints[0].coolSetpoint,

      }
  }


  setSetpoint = e=>{
    // cl("setting")
    let val = Number(this.state.setpoint);
    // cl(val);
    // let val2 = Math.round(10 * val);
    // cl(val2);
    // val = val2 / 10
    // cl(val);

    if (!isNaN(val))
      this.props.setter(0, val);// (z, sp)
    // cl(isNaN(val), val);
    // this.props.setter(0, this.state.setpoint);

  }

  onChange = e=>{
    // cl(e.target.value)
    this.setState({setpoint: e.target.value})

  }

  render(){
    return(
      <div style={{
        width: 300,
        height: 200,
        fontSize: 20,
        padding: 5,
        backgroundColor: "#C0FFFF",
      }}>
      Cool Setpoint for Zone 0:
      <form
      >
      <input type="text"
        size="4"
        value={this.state.setpoint}
        onChange={this.onChange}
        style={{
          fontSize: 40,
          margin: 5,
        }}
      /><br/>
      <button
        type="BUTTON"
        onClick={this.setSetpoint}
        style={{
          fontSize: 20,
          margin: 5,
        }}
      >Set</button>
      </form>

      </div>
    )

  }

}

export default TestSetpoint ;
