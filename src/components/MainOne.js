import React from 'react';
import MainBar from './MainBar';
import ShowVal from './ShowVal';
import TestSetpoint from './TestSetpoint';
import {init, getId, setAVal} from './utils/http';// getSite, // , putHistoryTime
import {checkLogin} from './utils/httpauth';
import {loadSensorBacks} from './utils/images';
// import {openWS} from './utils/ws';
import {p, pi} from './utils/paramIds';
import {globs, ms} from './utils/utils';
// import {cl} from './utils/utils';
class MainOne extends React.Component{
constructor(props){
    super(props);
    // globs.ws = openWS("ws://l4ol.com:8765/one");
//     let gs = getSite({site: 0});
    let ls = loadSensorBacks();
    checkLogin().then(r=>{
      let inProm = init();
      Promise.all([ls, inProm]).then(r=>{// gs,
      // gs.then(r=>{
        // cl(r);
  //       putHistoryTime()
        // cl("got it");
        this.setState({loaded: true})

      })
    });

    // let gz = getZones();
    // gz.then(r=>{
    //   getChannels({zone: 0});
    // })
    // getSiteOverview();
    this.state = {
      loaded: false,
    }
  }
/*site overview:
Alexa interface
full site (single screen):
image of site
alarms, with zone and type of alarm
all zones shown, with color indicating what's
most off (temp, hum, etc.)
zones:
Name/Alarm, stage, temp, hum, light, co2 for each zone
zone detail:
image of zone
name
temp, hum, light, co2 (HTLC)
stage, setpoints,
custom readings
list of channels, with name, type and state
equipment types: onoff, irr, co2, hid, mzone, pump,
fvalve, vent, curt, mixv, pid,
Alarms:
zone, type of alarm, current value
colors:
temp: blue / red
hum: dark red (brown) / cyan
co2: purple / green
light: dark gray / yellow

the REST function has to be reduced to just one:
get everything current

getSite is called from MainOne
this gets the current state of the complete site
first, this puts all the data in a z[zone][channel][id]
array
dbVals has a zones array with the zones in it
each zone has name, description, units for the
master and slaves. each unit has version and
snapshots
snapshots has all the values from the snapshots
table

setpoint is in dbVals.zones[0].setpoints[0].coolsetpoint
cool setpoint
cl(dbVals.zones[0].setpoints[0].coolSetpoint);

Oh Shit! Now, how do I write the information back?!
I could have an id (zone, channel, id, stored with )
each value 8 bit zone, 8 bit ch, 16 bit id

in any case, it's going to come back to the REST service
as a POST, with z, c, i, d, and s, t values

the POST is just an array of these, which are
put in the db, and sent as MQTT pubs

from REST.py:
ob = {
    's': site,
    't': time,
    'z': zone,
    'c': chan,
    'i': pid,
    'd': data,
}

todo 20190527
the zone info (name, desc) isn't put in database

*/

/*
{dbVals.zones[0].setpoints[0].coolSetpoint}

setting:
pi.config_setpoints["coolSetpoint"] = 7
zone = 0, channel = 255, base is
p.PID_BASE_CONFIG_SETPOINTS = 4586 // 8 * 10

*/
setSetpoint(z,  s){
  // cl(z, s);
  let id = getId(p.PID_BASE_CONFIG_SETPOINTS, 0,
    pi.config_setpoints, "coolSetpoint")
// function getId(base, ind, table, key){
  // cl(id);
  setAVal(0, 0, 255, id, s); // function setAVal(s, z, c, i, d){
}

showpage(left, top){
  let colors = [0x8080FF, 0xC080FF, 0xFF80FF, 0xFF80C0, 0xFF8080];
  // let colors = 5;
  return(
    <div style={ms(0, left, top, 360)}
    >
    <MainBar home upward back forward settings
    menu click={this.barClick} title=""  width="360"/>
      <div style={{ padding: 20,}}
        className="type01">
        <ShowVal
          wid="120"
          hgt="50"
          min="60"
          max="100"
          val="68"
          cols={colors}
          unit="3"
        />
      </div>
      <TestSetpoint setter={this.setSetpoint} />
    </div>
  )
}


render(){
  // cl("render");
  // cl(this.state);
  var left = 0, top = 0;
  if (globs.windowWidth < 500){
    left = 0 ;
    top = 0;
  }
  if(this.state.loaded){
    return(
      this.showpage(left, top)
    )
  } else {
    return(
      <div>loading</div>
    )
  }
}

}

  export default MainOne ;
