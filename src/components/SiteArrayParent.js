import React from 'react';
import {dbVals, } from './utils/http'; // dbVals, getLogins, getSite,
// import {getConfigs} from './utils/httpauth';
import {globs} from './utils/utils';
import {init} from './utils/http';
import {checkLogin} from './utils/httpauth';
import history from "../history"
import SiteArray from './SiteArray';
import MainBar from './MainBar';
import {cl} from './utils/utils';

class SiteArrayParent extends React.Component{
  constructor(props) {
    super(props);
//     cl("sitearray");
    let loaded = false;
    // cl(globs.token);
    if ((globs.token === "") || (!dbVals.gotSite)){
//       cl("check login");
      checkLogin().then(r=>{
        if (!r) {
          history.push("/li");
        } else {
          init().then(r=>{// loads site, and user info
            // cl("got");
            // this.setState({loaded: true});
            if ((dbVals.siteAuthorized) &&
              (r[0].command === "gotcursite00")){
            // if (r[0].command === "gotcursite00"){
              this.setState({loaded: true});
            } else {
              this.setState({loadMsg: "Not Authorized"})
            }

          })
        }
      });
    }else{
      loaded = true;
    }
    this.state = {
      loaded: loaded,
      loadMsg: "loading",
      // cols: ["Zone", "Name", "InTemp"],
    };
  }

  barClick = (e)=>{
    cl(e.currentTarget.id);
    switch (e.currentTarget.id){
      case "home":
        history.push("/sa")
        break;
      default:
        break;
    }
  }

  render(){
//     cl("site parent render");
      if (this.state.loaded){ // upward back forward
        return(
          <div>
            <MainBar home settings
            menu click={this.barClick} title="Site Array" />
          <SiteArray
            zones={dbVals.zones}
          /></div>
        )
      } else {
        return (
          <div>
          <MainBar home settings
          menu click={this.barClick} title="Site Array" />
          {this.state.loadMsg}</div>
        )
      }
  }
}


  export default SiteArrayParent ;
