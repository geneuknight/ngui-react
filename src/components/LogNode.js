import React from 'react';
// import MainBar from './MainBar';
// import {openWS} from './utils/ws';
// import {globs} from './utils/utils';
// import {cl} from './utils/utils';
class LogNode extends React.Component{
constructor(props){
    super(props);
    // cl(props.nodeInfo.id);
    this.state = {

    }
  }

/*each node should show the most recent
message, the definition (fromClient),
and have a background color for 1 sec
after each new message
*/
  render(){
    return(
      <table style={{
        backgroundColor: this.props.nodeInfo.color,
      }}><tbody>
      <tr><td width="200"><h3>{this.props.nodeInfo.id}</h3></td></tr>
      <tr><td>{this.props.nodeInfo.msg}</td></tr>
      <tr><td>{this.props.nodeInfo.time}</td></tr>
      </tbody></table>
    )
  }

}

export default LogNode ;
