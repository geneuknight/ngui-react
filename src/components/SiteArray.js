import React from 'react';
// import {getLogins} from './utils/http';
import {globs} from './utils/utils'; // cl,//cl, 
// import {cl} from './utils/utils';
import {saveConfigs} from './utils/httpauth'; // , getConfigs
import {dbVals} from './utils/http';
import ShowArray from './ShowArray00';
import SiteArrayMenu from './SiteArrayMenu';
import history from '../history'
class SiteArray extends React.Component{
  constructor(props) {
    super(props);
//     cl(globs);
    this.state={
      popUps: [],
      fields: ["Zone", "Name", "InTemp", "InHum", "InLight",
"InCO2", "OutLight", "OutTemp", "OutHum", "WindSpd", "WindDir",
"BaroPres", "HeatSP", "CoolSP", "HumSP", "DeHumSP", "TempStg",
"HumStg", "LoAlarm", "HiAlarm", ],
      cols: globs.userConfigs.siteViewColumns,
    }
    // let gc = getConfigs();
    // gc.then(r=>{
    //   cl(r);
    // })
    // cl(dbVals)
  }
  popupLoc = {x: 0, y: 0};



  getZoneValues(zoneId){// this is duplicated in ZoneArray.js
    let zo = dbVals.zones[zoneId];
//     cl(zoneId);
//     cl(zo);
    let me = zo.measures;
    let zm = zo.units[0];
//     cl(zm);
    let ss = zm.snapshots;
//     cl(ss);
    // cl(zo);
    // cl(zm);
    // cl(ss);
    // cl(me);
    // cl(dbVals);
    let vals = {
      "Zone": {v: zoneId, u: ""},
      "Name": {v: zo.name, u: ""},
      "InTemp": {v: ss.inTemperature, u: me.tempUnits},
      "InHum": {v: ss.inHumidity, u: "pcnt"},
      "InLight": {v: ss.inLight, u: me.lightUnits},
      "InCO2": {v: ss.co2, u: "PPM"},
      "OutLight": {v: ss.outLight, u: me.lightUnits},
      "OutTemp": {v: ss.outTemperature, u: me.tempUnits},
      "OutHum": {v: ss.outHumidity, u: "pcnt"},
      "WindSpd": {v: ss.windSpeed, u: me.windUnits},
      "WindDir": {v: ss.windDirection, u: "degr"},
      "BaroPres": {v: ss.barometricPressure, u: "mbar"},
      "HeatSP": {v: ss.heatSetpoint, u: me.tempUnits},
      "CoolSP": {v: ss.coolSetpoint, u: me.tempUnits},
      "HumSP": {v: ss.humidifySetpoint, u: "pcnt"},
      "DeHumSP": {v: ss.dehumidifySetpoint, u: "pcnt"},
      "TempStg": {v: ss.temperatureStage, u: ""},
      "HumStg": {v: ss.humidityStage, u: ""},
      "LoAlarm": {v: ss.lowAlarm, u: ""},
      "HiAlarm":  {v: ss.highAlarm, u: ""}
    }
    return vals;
  }

  makeZoneArr=e=>{
    // cl(this.props.cols);
//     cl(this.props.zones);
    let retArr = [];
    // cl(this.state.cols);
    retArr.push(this.state.cols);
    this.props.zones.forEach((z, i)=>{
      let vals = this.getZoneValues(i);
      let arr = [];
      this.state.cols.forEach(col=>{
        arr.push(vals[col]);
      })
      retArr.push(arr);
      // cl(i)
    })
    // cl(retArr);
    return retArr;

  }

  showPops = e=>{
    return (
      this.state.popUps.map((p, i)=>p.f(i, p.l, p.p))
    )
  }

  popupPushPop=e=>{
    let pops = this.state.popUps.slice(0);
    if (e !== undefined){
      pops.push(e);
    } else {
      pops.pop();
    }
    this.setState({popUps: pops})
  }

  addPop = e=>{
    this.popupPushPop(e);
  }

  delPop = e=>{
    this.popupPushPop();
  }

  // closePop = e=>{
  //   // cl(this.state)
  //   this.props.pops.del();
  // }

  myPopUp = (e, loc, param) => {
    // cl(this.popupLoc)
    // cl(param);
    return(
      <div key={e}
      style={{
        position: "absolute",
        left: loc.x,
        top: loc.y,
        backgroundColor: "#80FFFF",
      }}
      onClick={this.delPop}
      >
      <SiteArrayMenu
        fields={this.state.fields}
        col={param}
        cols={this.state.cols}
        sel={this.contextMenuCmd}
      />
      </div>
    )
  }

  addCol(loc, newCol){
    // cl(this.state);
    let retCols = [];
    loc = parseInt(loc);
    this.state.cols.forEach((c, i)=>{
      if(!((i===loc) && (newCol==="delete"))){
        retCols.push(c);
        if (i===loc) retCols.push(newCol);
      }
    })
    this.setState({cols: retCols})
//    saveConfigs({Post: {configs: ["Zone"]} }); from test
//     globs.userConfigs = {};
    globs.userConfigs.siteViewColumns = retCols;
    saveConfigs();// {Post: {configs: retCols}}
    // cl(retCols);
  }

  contextMenuCmd=e=>{
//     cl(e)
    let cmds = e.split("|");// e is row | col | column name
    // cl(cmds);
    this.addCol(cmds[1], cmds[2]);

    // if (cmds[1]==="delete"){
    //
    // } else {
    //
    // }

  }

  contextMenu=e=>{
    // cl("context menu")
    // cl(e.target.id); // "d0d2"
    // this.popupLoc = {x: e.clientX, y: e.clientY};
    this.addPop({
      f: this.myPopUp,
      l: {x: e.clientX, y: e.clientY},
      p: e.target.id,
    });
  }
  
  selRow = e=>{
//     cl(e.target.id);
    let coords = e.target.id.split('|');
    history.push("/za/" + coords[0]);
  }

  render(){
    // cl("render")
    // this.getZoneValues(0);
    // let v1 = {v: 1, u: "degF"};
    // let v2 = {v: 2, u: "degF"};
    // let testArr = [[v1, v1, v2], [v1, v1, v2], [v1, v1, v2], ];
    let testArr = this.makeZoneArr();
    // cl(testArr)
    return(
      <div>
      {this.showPops()}
      <ShowArray arr={testArr} popsO={this.state.popUps}
        pops={{add: this.addPop, del: this.delPop}}
        takeContextMenu={this.contextMenu}
        topContextMenu = {this.contextMenu}
        selRow={this.selRow}
        />
      </div>
    )
  }
}


  export default SiteArray ;
