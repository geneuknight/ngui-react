import React from 'react';
import MainBar from './MainBar';
import LogNode from './LogNode';
// import {openWS} from './utils/ws';
import {globs} from './utils/utils';
import {cl} from './utils/utils';
class Log extends React.Component{
constructor(props){
    super(props);
    cl("cons1");
    // this.ni = {
    //   code: "fromCont",
    //   lastMsg: "Read Snapshot 0",
    // };
    // cl(Date.now());
    this.nodeIds = [
      "fromCont",
      "toMQTT",
      "serverFromMQTT",
      "toClient",
      "toCont",
      "fromMQTT",
      "serverToMQTT",
      "fromClient",
    ];
    cl("cons1");
    let nodes = [];
    for (let i = 0 ; i < 8 ; i++){
      nodes[i] = {
        color: "#C0FFFF",
        update: 0,
        id: this.nodeIds[i],
        msg: "",
        time: "",
      }
    }
    cl("cons1");
    setInterval(this.checkNodeTimes, 3000);
//     globs.ws = openWS("ws://l4ol.com:8765/");
    globs.ws.onmessage = e=>this.onMessage(e);
    cl("cons1");
    this.state = {
      nodes: nodes,

    }
    cl(nodes);
  }

  onMessage = e=>{
    // cl(e);
    let msg = e.data;
    let fields = msg.split(',');
    cl(fields);
    let now = Date.now();
    let ti = (new Date()).toLocaleTimeString();
    let nodes = this.state.nodes.slice(0);
    let upd = false;
    for (let i = 0 ; i < 8 ; i++){
      if (nodes[i].id === fields[0]){
        nodes[i].update = now;
        nodes[i].color = "#80FF80";
        nodes[i].msg = fields[1];// + "\n" + ti;
        nodes[i].time = ti;
        upd = true;
      }
    }
    if (upd){
      this.setState({nodes: nodes});
    }
  }

  checkNodeTimes = e=>{
    // cl(this.state);
    let now = Date.now();
    let upd = false;
    cl("check node");
    let nodes = this.state.nodes.slice(0);
    for (let i = 0 ; i < 8 ; i++){
      let nt = nodes[i].update;
      if (now - nt > 2000){
        nodes[i].color = "white";
        upd = true;
      }
    }
    if (upd){
      this.setState({
        nodes: nodes,
      })
    }
  }

  showANode = function(ind){
    // cl("show " + ind)
    return(
      <td key={ind}>
        <LogNode nodeInfo = {this.state.nodes[ind]}/>
      </td>
    )
  }

  showNodeRow = function(ind){
    let adds = [0, 1, 2, 3];
    return (
      <tr key={ind}>
        {adds.map((p, i)=>{
          return this.showANode(ind + p);
        })}
      </tr>
    )

  }

  showNodes = e=>{
    return(
      <table><tbody>
      {this.showNodeRow(0)}
      {this.showNodeRow(4)}
      </tbody></table>
    )
  }

  render(){
    return(
      <div>
      <MainBar home upward back forward settings
      menu click={this.barClick} title=""/>
      {this.showNodes()}
      </div>
    )
  }

}

export default Log ;
