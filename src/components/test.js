console.log("here");

/* we need to distinguish taps and swipes
actions:
tap
tapHold
swipeUp: distance, speed, duration
when the touch ends, keep the motion going,
loses speed 1/2 per second
keep swiping, and it should keep accelerating
first order:
accummulate the actual movement
collect information:
keep track of max dx dy values
startX, startY
moveX, moveY
endX, endY
maxDx, maxDy
touching
a touch may be: tap, tapHold, vSwipe(u/d), hSwipe(l/r)
fullSwipe means that a new page is brought in



*/

var touching = false;
var touchDir = "t";
var touchCancel = false;
var startX = 0, startY = 0, startT = 0;
var moveX = 0, moveY = 0, moveT = 0;
var maxDx = 0, maxDy = 0;
var endX = 0, endY = 0;
var throtTime = 0;
var curTime = 0 ;

function showTouch(type){
  tc = gebi("touchSpan");
  tc.innerHTML = type;
}

function handleTouchStart(e){
  e.preventDefault();
  touching = true ;
  touchCancel = false ;
  maxDx = 0 ;
  maxDy = 0;
  startX = e.targetTouches[0].screenX;
  startY = e.targetTouches[0].screenY;
  startT = Date.now();
  touchDir = "t";
  showTouch("start");
  // console.log(e);
}

function handleTouchEnd(e){
  e.preventDefault();
  touching = false ;
  showTouch("end");
  let dist = maxDx * maxDx + maxDy * maxDy;
  showVal("elapseSpan", Date.now() - startT + ": " + dist);
  showVal("dxSpan", maxDx);
  showVal("dySpan", maxDy);
}

function handleTouchCancel(e){
  e.preventDefault();
  touchCancel = true ;
  showTouch("cancel");
  console.log(e);
}

function gebi(id){
  return document.getElementById(id);
}

function showYVal(val){
  var yObj = gebi("yValue");
  yObj.innerHTML=val;
}

function showVal(id, val){
  var o = gebi(id);
  o.innerHTML=val;
}

function handleTouchMove(e){
//  console.log(e);
//  console.log(e.targetTouches[0].pageY);
  e.preventDefault();
  let mX = e.targetTouches[0].screenX;
  let mY = e.targetTouches[0].screenY;
  let abx = Math.abs(mX - startX);
  if (abx > maxDx) maxDx = abx ;
  let aby = Math.abs(mY - startY);
  if (aby > maxDy) maxDy = aby ;
  if (touchDir == "t"){
    if (maxDx > 10) touchDir = "h";
    if ((maxDy > maxDx) && (maxDy > 10)) touchDir = "v";
  }
  showVal("dirSpan", touchDir);
  // console.log(maxDy);
  showTouch("move");
  curTime = Date.now(); // d.getTime();
  moveX = mX;
  moveY = mY;
  if (curTime - throtTime > 100){
    throtTime = curTime;
    showYVal(e.targetTouches[0].pageY);
    console.log(curTime);
    // showTot(curTime);
  }
}

function initTouch(){
  document.addEventListener('touchstart', handleTouchStart, {passive: false, capture: true});
  document.addEventListener('touchmove', handleTouchMove, {passive: false, capture: true});
  document.addEventListener('touchcancel', handleTouchCancel, {passive: false, capture: true});
  document.addEventListener('touchend', handleTouchEnd, {passive: false, capture: true});
}


function initIt(){
  initTouch();
}
