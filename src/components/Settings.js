import React from 'react';
// import FormControl from '@material-ui/core/FormControl';
// import InputLabel from '@material-ui/core/InputLabel';
// import Select from '@material-ui/core/Select';
// import MenuItem from '@material-ui/core/MenuItem';
// import TextField from '@material-ui/core/TextField';
import SettingsText from './SettingsText';
// import MainBar from './MainBar';
import history from "../history"

import {cl} from './utils/utils';
class Settings extends React.Component{
  constructor(props) {
    super(props);
    cl(props);
    this.state={
      
    }
  }
  
  testPages = {
    output:{// a list of controls.
      conts: [
        {
          name: "Zone Name", 
          type: "text", 
          z: 0, 
          c: 255, 
          i: 5022
          
        }
      ]
    },
  };
  
  barClick = (e)=>{
    cl(e.currentTarget.id);
    switch (e.currentTarget.id){
      case "home":
        history.push("/sa")
        break;
      case "settings":
        cl(this.props.match.params); // zone is in id
        history.push('/zs/z/' + this.props.match.params.id + '/settings');// /zs/z/0/stages
        break;
      default:
        break;
    }
  }
  
  showControl=(c, i)=>{
/*props for controls:
 name, type, z, c, i, */
    let zone = this.props.zone;
    switch(c.type){
      case "text":
        return(
          <SettingsText key={i} name={c.name} z={zone} c={c.c} i={c.i}/>
        );
      default:
        return;
    }
  }
  
  showControls=(type)=>{
    cl("conts");
    cl(this.testPages[type]);
    let conts = this.testPages[type].conts
    return conts.map((c, i)=>{
        return this.showControl(c, i);
      })
  }
  
  showSettingsPage=()=>{
    return(
      <div style={{margin: 20}}>
        {this.showControls("output")}
      </div>
    );
  }

  render(){
    return(
      <div>
      {this.showSettingsPage()}
      </div>
    );
  }
  
}

export default Settings ;
