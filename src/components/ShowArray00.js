import React from 'react';
import {cl} from './utils/utils';
// import history from '../history'
class ShowArrayx extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      popupLoc: {x: 0, y: 0}
    }
  }

  popupLoc = {x: 0, y: 0};

units = {
  "degF": String.fromCharCode(176) + "F",
  "degC": String.fromCharCode(176) + "C",
  "mph": "mph",
  "kph": "kph",
  "w/m2": "w/m2h",
  "klux": "klux",
  "uMol": "uMol",
  "l/ml": ["l", "ml"],
  "gal/oz": ["gal", "oz"],
  "gal/ml": ["gal", "ml"],
  "uS": "uS",
  "CF": "CF",
  "PPM": "PPM",
  "pcnt": "%",
  "mbar": "mb",
}

  showVal(v){
    if (v === undefined){
      return ("---");
    } else {
      return (v) ;
    }
  }

  showArray00Cell(val){
    // cl(val)
    if (val.u === undefined){
      return this.showVal(val);
    } else {
      if (val.v === undefined) return ("---");
      if (val.u === ""){
        return this.showVal(val.v);
      } else {
        return this.showVal(val.v + this.units[val.u]);
      }
    }
  }

  myClick=e=>{
    let coords = e.target.id.split('|');
    if ((coords[0] === "0") && (this.props.topContextMenu !== undefined)){
//       cl("top row menu");
//       cl(coords);// remember these are strings!
      e.preventDefault();
      this.props.topContextMenu(e);
    }
//     cl(e.target.id);
//     cl(e.currentTarget);
//     if((this.props.takeContextMenu !== undefined) &&
//       (e.type==="contextmenu")){
//       // cl("take");
//       e.preventDefault();
//       this.props.takeContextMenu(e);
//     }
  }

  myTopClick=e=>{
    let coords = e.target.id.split('|');
    if ((coords[0] === "0") && (this.props.topContextMenu !== undefined)){
      e.preventDefault();
      this.props.topContextMenu(e);
    }
  }

  myLeftClick=e=>{
    cl("left click");
    let coords = e.target.id.split('|');
    cl(coords);
    if ((coords[1] === "0") && (this.props.leftContextMenu !== undefined)){
      e.preventDefault();
      this.props.leftContextMenu(e);
    }
  }

  selZone=e=>{
    this.props.selRow(e.target.id);
  }

  showArray00Col(col, row, ind){
    // let id = "d" + row + "d" + ind;
    let topStyle = {};
    let leftStyle = {};
    let topMenu = null;
    let leftMenu = null;
    let leftClick = null;
    if (this.props.topContextMenu !== undefined){
      topStyle={cursor:"default"}
      topMenu = this.myTopClick;
    }
    if (this.props.leftContextMenu !== undefined){
      leftStyle={cursor:"default"}
      leftMenu = this.myLeftClick;
//       cl("left context");
    }
    if (this.props.selRow !== undefined){
      leftStyle={cursor:"pointer"}
      leftClick = this.props.selRow;
    }
    if (row === 0){
      return (
        <th key={ind}
          id={row + "|" + ind}
          onContextMenu={topMenu}
          style={topStyle}
        >
          {this.showArray00Cell(col)}
        </th>
      )
    } else {
      // cl(ind);
      // cl(col)
      if (ind === 0){// first column
        return (
          <td key={ind}
            onClick={leftClick}
            style={leftStyle}
            onContextMenu={leftMenu}
            id={row - 1 + "|" + ind}
          >
            {this.showArray00Cell(col)}
          </td>
        )
      } else {
        return (
          <td key={ind}>
            {this.showArray00Cell(col)}
          </td>
        )
      }
    }
    // return (
    //   <td key={ind} onClick={this.myClick}
    //   onContextMenu={this.myClick}
    //   >
    //   {this.showArray00Cell(col)}
    //   </td>
    // )
  }

  showArray00Row(row, ind){
    return (
      <tr key={ind}>
        {row.map((c, i)=>this.showArray00Col(c, ind, i))}
      </tr>
    )
  }

  showArray00(arr){
/* arr is an array (rows) of arrays (cols) each element is an object:
v: 72.3, u: degF
the first row should be headers*/
// cl(arr);
  return(
    <table><tbody>
      {arr.map((r, i)=>this.showArray00Row(r, i))}
    </tbody></table>
  )
// const listItems = numbers.map((number) =>
//   <li key={number.toString()}>
//     {number}
//   </li>
// );
  }

  render(){
    return(
      <div>
      {this.showArray00(this.props.arr)}
      </div>
    )
  }
}

  export default ShowArrayx ;
