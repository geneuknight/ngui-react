import React from 'react';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import MainBar from './MainBar';
import history from "../history"
import {c, cl, globs, getTime} from './utils/utils';
import {pb, pInd, pi} from './utils/paramIds';//p,
import {dbVals, init} from './utils/http';
import {sendPacks} from './utils/ws';
import {checkLogin} from './utils/httpauth';

class Param extends React.Component{
  constructor(props) {
    super(props);
    let loaded = false;
    this.makeParmTabs();
    let col = Object.keys(this.tableList[0].cols)[0];
//     cl(Object.keys(pb));
    // cl("construct params")
    // cl(dbVals.gotSite)
    if ((globs.token === "") || (!dbVals.gotSite)){
      // cl("checvking")
      checkLogin().then(r=>{
        // cl("initting")
        init().then(r=>{
          // cl(r[0].command);
          // cl(r);
          this.refreshTable();
          if ((dbVals.siteAuthorized) &&
            (r[0].command === "gotcursite00")){
            // cl("this.set")
            this.setState({loaded: true});
          } else {
            this.setState({loadMsg: "Not Authorized"})
          }
        });
      });
    }else{
      loaded = true;
    }
    globs.events.subscribe("data", d=>{
      // cl(this.state);
      if (!this.state.focused){
        // cl("this.set")
        this.setState({paramParm: this.calcCurParam(this.state.paramZone, this.state.paramChan, this.state.paramPid)})
      }
//       this.setState({data: this.state.data+1});
//       cl("got data");
    })
    this.state={
      table: 0,
      unit: 0,
      column: col,
      channel: 0,
      index: 0,
      sensor: 0,
      paramSite: 0,
      paramZone: 0,
      paramChan: 0,
      paramPid: 0,
      paramParm: 0,
      data: 0,
      loaded: loaded,
      loadMsg: "loading",
      focused: 0,
      tableValues: [],
    }
  }

  tableList = [];

  makeParmTabs = ()=>{
//     this.tableList = [];//Object.keys(pb);
    Object.keys(pb).forEach((e, i)=>{
      this.tableList.push({name: e, base: pInd[e][0], type: pInd[e][1], indSize: pInd[e][2], indCnt: pInd[e][3], cols: pi[e]});
//       cl(pb[e]);
    });
//     cl(this.tableList);
//     cl(this.tableList[0].cols);

//     this.tableList.forEach((e, i)=>{
//       cl(e);
//     });
  }

  initTables = ()=>{
    return this.tableList.map((e, i)=>{
//       cl(e.name);
      return(
          <MenuItem key={i} value={i}>{e.name}</MenuItem>
      )
    });
  }

  initTableValues = (state)=>{
    // cl("init table");
    let tabState = Object.assign({}, state);
    state.tableValues = [];
    let cols = this.tableList[state.table].cols;
    Object.keys(cols).forEach((k, i)=>{
      tabState.column = k;
      let ret = this.getPidVals(tabState);
      ret.push(this.calcCurParam(ret[0], ret[1], ret[2]));
      state.tableValues.push(ret);
    })
    // cl("this.set")
    this.setState({tableValues: state.tableValues})

// tableList: [
//   base: 0
//   cols: [id: 1, zoneIndex: 2, unitIndex: 3, unix_timestamp(created): 4, unix_timestamp(unitTime): 5, …]
//   indCnt: 0
//   indSize: 0
//   name: "snapshots"
//   type: 0
    // cl(this.tableList[table*1]);
  }

  tableChange=(e)=>{
    let tv = this.state.tableValues.slice(0);
    tv[e.target.id*1][3] = e.target.value;
    this.setState({tableValues: tv})

    // cl(tv[e.target.id*1]);
    // cl(e.target.id);
  }

  showInput = (name, id)=>{
    var val;
    if (this.state.tableValues[id] !== undefined)
      val = this.state.tableValues[id][3];
    return(
      <input
        type="text"
        id={id}
        onChange={this.tableChange}
        value={val}
      />
    );
  }

  showColumns = (table)=>{
    // this.initTableValues(table);
    let tab = this.tableList[parseInt(table)];
    return (
      <table><tbody>
        {Object.keys(tab.cols).map((e, i)=>{
          return(
            <tr key={i}><td>{e}</td>
            <td>{this.showInput(e, i)}</td></tr>
          )
        })}
      </tbody></table>
    );
  }
/*
<TextField
  label={name}
  id="t"
  value={this.state.paramPid}
  onChange={this.onNumericChange}
/>
*/

  initColumns = (table)=>{
    // cl(this.tableList[parseInt(table)].cols);// the name: id of each column
    return Object.keys(this.tableList[parseInt(table)].cols).map((e, i)=>{
      return(
          <MenuItem key={i} value={e}>{e}</MenuItem>
      )
    });
  }

  initZones = ()=>{
//     cl(dbVals.z.length);
    let arr = [];
    for (let i = 0 ; i < dbVals.z.length ; i++){
      arr.push(i + 1);
    }
    return arr.map((e, i)=>{
      return(
          <MenuItem key={i} value={i}>{e}</MenuItem>
      );
    });
  }

  initUnits = ()=>{
    let names = ["Zone Master", "Slave 1", "Slave 2", "Slave 3"];
//     cl(dbVals.z);
    let cnt = 0;
    for (let i = 240 ; i < 244 ; i++){
      if ((dbVals.z[this.state.paramZone] !== undefined) && (dbVals.z[this.state.paramZone][i] !== undefined)) cnt += 1;
    }
//     cl(cnt);
    let arr = [];
    for (let i = 0 ; i < cnt ; i++){
      arr.push(i);
    }
    return arr.map((e, i)=>{
      return(
          <MenuItem key={i} value={i}>{names[i]}</MenuItem>
      );
    });
  }

  initChannels = ()=>{
//     cl(dbVals.z.length);
    let arr = [];
    for (let i = 0 ; i < 40 ; i++){
      arr.push(i + 1);
    }
    return arr.map((e, i)=>{
      return(
          <MenuItem key={i} value={i}>{e}</MenuItem>
      );
    });
  }

  initIndexes = ()=>{
    let arr = [];
    for (let i = 0 ; i < 40 ; i++){
      arr.push(i + 1);
    }
    return arr.map((e, i)=>{
      return(
          <MenuItem key={i} value={i}>{e}</MenuItem>
      );
    });
  }

  initSensors = ()=>{
    let arr = [];
    for (let i = 0 ; i < 3 ; i++){
      arr.push(i + 1);
    }
    return arr.map((e, i)=>{
      return(
          <MenuItem key={i} value={i}>{e}</MenuItem>
      );
    });
  }



  setChannelPidUnitType = (state)=>{
/* now the hard part! we have the zone, unit, table, and column. Calculate the pid*/
    let tab = this.tableList[state.table];
    let pid = pi[tab.name][state.column];
    // cl(tab.name, state.column, pid);
    var offset;
    if (tab.indSize === 0){
      offset = 0;
    } else {
      offset = state.index * tab.indSize;
    }
    state.paramZone = state.zone;
    state.paramChan = 240 + state.unit ;
    state.paramPid = tab.base + offset + pid ;
    return [state.paramZone, state.paramChan, state.paramPid];
    // this.setState({
    //   paramChan: state.paramChan, // 240 + state.unit,
    //   paramPid: state.paramPid, // tab.base + offset + pid,
    // });


  }

  setChannelPidChannelType = (state)=>{
/* now the hard part! we have the zone, unit, table, and column. Calculate the pid*/
    let tab = this.tableList[state.table];
    let pid = pi[tab.name][state.column];
    cl(tab.name, state.column, pid);
    state.paramZone = state.zone;
    state.paramChan = 40 * state.unit + state.channel ;
    state.paramPid = tab.base + pid ;
    return [state.paramZone, state.paramChan, state.paramPid];
    // this.setState({
    //   paramChan: state.paramChan, // 40 * state.unit + state.channel,
    //   paramPid: state.paramPid, // tab.base + pid,
    // });
  }

  setChannelPidEcphType = (state)=>{
/* now the hard part! we have the zone, unit, table, and column. Calculate the pid*/
    let tab = this.tableList[state.table];
    let pid = pi[tab.name][state.column];
    cl(tab.name, state.column, pid);
    state.paramZone = state.zone;
    state.paramChan = 192 + 8 * state.unit + state.index ;
    state.paramPid = tab.base + pid ;
    return [state.paramZone, state.paramChan, state.paramPid];
    // this.setState({
    //   paramChan: state.paramChan, // 192 + 8 * state.unit + state.index,
    //   paramPid: state.paramPid, // tab.base + pid,
    // });
  }

  setChannelPidEcphSensorType = (state)=>{
/* now the hard part! we have the zone, unit, table, and column. Calculate the pid*/
    let tab = this.tableList[state.table];
    let pid = pi[tab.name][state.column];
    cl(tab.name, state.column, pid);
    var offset;
    if (tab.indSize === 0){
      offset = 0;
    } else {
      offset = state.sensor * tab.indSize;
    }
    state.paramZone = state.zone;
    state.paramChan = 192 + 8 * state.unit + state.index ;
    state.paramPid = tab.base + pid + offset ;
    return [state.paramZone, state.paramChan, state.paramPid];
    // this.setState({
    //   paramChan: state.paramChan, // 192 + 8 * state.unit + state.index,
    //   paramPid: state.paramPid, // tab.base + pid + offset,
    // });
  }

  setChannelPidZoneType = (state)=>{
//     cloud.py  595: ['0-0-255-', 0, 1570891164, 0, 255, 4718, 1566661880]
// cloud.py  595: ['0-0-255-', 0, 1570891164, 0, 255, 4719, '1']
/* now the hard part! we have the zone, unit, table, and column. Calculate the pid*/
    let tab = this.tableList[state.table];
    let pid = pi[tab.name][state.column];
//     cl(tab.name, state.column, pid);
    var offset;
    if (tab.indSize === 0){
      offset = 0;
    } else {
      offset = state.index * tab.indSize;
    }
    // cl("set zone " + state.zone);
    // state.paramZone = state.zone;
    state.paramZone = state.zone;
    state.paramChan = 255 ;
    state.paramPid = tab.base + pid + offset ;
    return [state.paramZone, state.paramChan, state.paramPid];
    // this.setState({
    //   paramChan: state.paramChan, // 255,
    //   paramPid: state.paramPid, // tab.base + pid + offset,
    // });
  }


  setChannelPidZoneConfigType = (state)=>{
//     cloud.py  595: ['0-0-255-', 0, 1570891164, 0, 255, 4718, 1566661880]
// cloud.py  595: ['0-0-255-', 0, 1570891164, 0, 255, 4719, '1']
/* now the hard part! we have the zone, unit, table, and column. Calculate the pid*/
    let tab = this.tableList[state.table];
    let pid = pi[tab.name][state.column];
    cl(tab.name, state.column, pid);
    state.paramZone = state.zone;
    state.paramChan = 255 ;
    state.paramPid = tab.base + 2 * pid + 1 ;
    return [state.paramZone, state.paramChan, state.paramPid];
    // this.setState({
    //   paramChan: state.paramChan, // 255,
    //   paramPid: state.paramPid, // tab.base + 2 * pid + 1 // + offset,
    // });
  }

  setChannelPidControllerConfigType = (state)=>{
//     cloud.py  595: ['0-0-255-', 0, 1570891164, 0, 255, 4718, 1566661880]
// cloud.py  595: ['0-0-255-', 0, 1570891164, 0, 255, 4719, '1']
/* now the hard part! we have the zone, unit, table, and column. Calculate the pid*/
    let tab = this.tableList[state.table];
    let pid = pi[tab.name][state.column];
    cl(tab.name, state.column, pid);
    state.paramZone = state.zone;
    state.paramChan = 240 + state.unit ;
    state.paramPid = tab.base + 2 * pid + 1 ;
    return [state.paramZone, state.paramChan, state.paramPid];
    // this.setState({
    //   paramChan: state.paramChan, // 240 + state.unit,
    //   paramPid: state.paramPid, // tab.base + 2 * pid + 1 // + offset,
    // });
  }


  setChannelPidSiteType = (state)=>{
/* now the hard part! we have the zone, unit, table, and column. Calculate the pid*/
    let tab = this.tableList[state.table];
    let pid = pi[tab.name][state.column];
    cl(tab.name, state.column, pid);
    state.paramZone = 255 ;
    state.paramPid = tab.base + pid ;
    return [state.paramZone, state.paramChan, state.paramPid];
    // this.setState({
    //   paramZone: state.paramZone, // 255,
    //   paramPid: state.paramPid, // tab.base + pid,
    // });
  }

  getPidVals=(state)=>{// returns [z, c, i,]
    switch (this.tableList[state.table].type){
      case c.UNIT_TYPE:
        return this.setChannelPidUnitType(state);
      case c.CHANNEL_TYPE:
        return this.setChannelPidChannelType(state);
      case c.ECPH_TYPE:
        return this.setChannelPidEcphType(state);
      case c.ECPH_SENSOR_TYPE:
        return this.setChannelPidEcphSensorType(state);
      case c.ZONE_TYPE:
        return this.setChannelPidZoneType(state);
      case c.SITE_TYPE:
        return this.setChannelPidSiteType(state);
      case c.ZONE_CONFIG_TYPE:
        return this.setChannelPidZoneConfigType(state);
      case c.CONT_CONFIG_TYPE:
        return this.setChannelPidControllerConfigType(state);
      default:
        return null;
      }
  }

  setChannelPid = (state)=>{
    let ret = this.getPidVals(state);
    this.initTableValues(state);
//       this.tableList.push({name: e, base: pInd[e][0], type: pInd[e][1], indSize: pInd[e][2], indCnt: pInd[e][3], cols: pi[e]});

    // cl (this.tableList[state.table].type);
    // switch (this.tableList[state.table].type){
    //   case c.UNIT_TYPE:
    //     this.setChannelPidUnitType(state);
    //     break;
    //   case c.CHANNEL_TYPE:
    //     this.setChannelPidChannelType(state);
    //     break;
    //   case c.ECPH_TYPE:
    //     this.setChannelPidEcphType(state);
    //     break;
    //   case c.ECPH_SENSOR_TYPE:
    //     this.setChannelPidEcphSensorType(state);
    //     break;
    //   case c.ZONE_TYPE:
    //     this.setChannelPidZoneType(state);
    //     break;
    //   case c.SITE_TYPE:
    //     this.setChannelPidSiteType(state);
    //     break;
    //   case c.ZONE_CONFIG_TYPE:
    //     this.setChannelPidZoneConfigType(state);
    //     break;
    //   case c.CONT_CONFIG_TYPE:
    //     this.setChannelPidControllerConfigType(state);
    //     break;
    //   default:
    //     break;
    // }
//     cl("set ", state.paramZone, state.paramChan, state.paramPid);
//     cl(this.calcCurParam(state.paramZone, state.paramChan, state.paramPid));
    // cl("calc " + state.paramZone);
    // this.setState({paramParm: this.calcCurParam(state.paramZone, state.paramChan, state.paramPid)});
    // cl("this.set")
    this.setState({
      paramZone: ret[0],
      paramChan: ret[1],
      paramPid: ret[2],
      paramParm: this.calcCurParam(ret[0], ret[1], ret[2])});
  }

  onSelectChange = (e)=>{
    // cl(e.target.name);
    let val = e.target.value;
    let state = Object.assign({}, this.state);
    state.zone = state.paramZone;
    // cl(state.paramZone);
    // cl("this.set")
    switch (e.target.name){
      case "table_select":
        let col = Object.keys(this.tableList[val].cols)[0];
        this.setState({table: val, column: col});
        state.table = val;
        state.column = col;
        this.setChannelPid(state);
        break;
      case "zone_select":
        this.setState({paramZone: val});
        state.zone = val;
        this.setChannelPid(state);
        // cl("zone");
        break;
      case "unit_select":
        this.setState({unit: val});
        state.unit = val;
        this.setChannelPid(state);
        break;
      case "chan_select":
        this.setState({channel: val});
        state.channel = val;
        this.setChannelPid(state);
        break;
      case "column_select":
        this.setState({column: val});
        state.column = val;
        this.setChannelPid(state);
        break;
      case "index_select":
        this.setState({index: val});
        state.index = val;
        this.setChannelPid(state);
        break;
      case "sensor_select":
        this.setState({sensor: val});
        state.sensor = val;
        this.setChannelPid(state);
        break;
      default:
        break;
    }

  }

  onTableChange = (e)=>{
//     cl("change");
    cl(e.target.name);
//     cl(e.currentTarget);
    let val = e.target.value;
//     cl(val);
    this.setState({table: val});
//     this.initColumns(val);
  }

  onColumnChange = (e)=>{
    cl("change");
    let val = e.target.value;
    this.setState({column: val});
//     this.initColumns(val);
  }

//   PID_BASE_CONFIG_AUX_ALARMS = 1216 # 4 * 32
// PID_BASE_CONFIG_AUX_CONTROLS = 1344 # 17 * 64 no! 128!
// PID_BASE_CONFIG_AUX_PERSISTENT_VARIABLES = 3520 # 4 * 32
// PID_BASE_CONFIG_AUX_VARIABLES = 3640 # 4 * 64
// PID_BASE_CONFIG_EXPANSION_BOARDS = 3896 # 5 * 64
// PID_BASE_CONFIG_CONTROLLER_SETTINGS = 4216 # 145 * 2 - allow for 170, everything +50
// # the following have been +50, on 20190908
// PID_BASE_CONFIG_ECPH = 4556 # 11 - actually, 8 * 11
// PID_BASE_CONFIG_ECPH_SENSORS = 4567 # 3 * 23 = 69, actually, 8 * 69
// #4440#
// # zone wide
// PID_BASE_CONFIG_SETPOINTS = 4636 # 8 * 10
// PID_BASE_CONFIG_ZONE_SETTINGS = 4716 # 153 * 2!
// PID_BASE_CONFIG_ZONES = 5022 # 2
// PID_BASE_CONFIG_CONTROLLERS = 5024 # 4
// # site wide
// PID_BASE_CONFIG_COMM_STAT = 5028 # 3
//
// PID_ZONE_MIN = PID_BASE_CONFIG_ZONES # 5022
// PID_ZONE_MAX = PID_BASE_CONFIG_CONTROLLERS - 1 # 5023
// PID_SETPOINTS_MIN = PID_BASE_CONFIG_SETPOINTS # 4636
// PID_SETPOINTS_MAX = PID_BASE_CONFIG_ZONE_SETTINGS - 1 # 4715
// PID_CONTROLLERS_VERSION = PID_BASE_CONFIG_CONTROLLERS + 1 # 5025
//

  onNumericChange = (e)=>{
//     cl(e.target.id);
    let z = this.state.paramZone;
    let c = this.state.paramChan;
    let i = this.state.paramPid;
//     let d = this.state.paramParm;
    switch(e.target.id){
      case "site":
        this.setState({paramSite: e.target.value})
        break;
      case "zone":
        z = e.target.value;
        this.setState({paramZone: z, paramParm: this.calcCurParam(z, c, i)})
        break;
      case "chan":
        c = e.target.value;
        this.setState({paramChan: c, paramParm: this.calcCurParam(z, c, i)})
        break;
      case "pid":
        i = e.target.value;
        this.setState({paramPid: i, paramParm: this.calcCurParam(z, c, i)})
        break;
      case "parm":
        this.setState({paramParm: e.target.value})
        break;
      default:
        break;
    }
  }

  calcCurParam = (z, c, i)=>{
    // cl("calc cur parm: " + [z, c, i].toString())
    var val;
//     cl(this.state.paramZone, )
    try{
      val = dbVals.z[z][c][i] ;
    }
    catch{
      val = "---";
    }
    if (val === undefined) val = "---";
//     cl(val);
    return val;

  }

//         <td width="50">
//           <TextField
//             label="Site"
//             id="site"
//             value={this.state.paramSite}
//             onChange={this.onNumericChange}
//           />
//         </td>

  showNumericFields = ()=>{

//     if (!this.state.focused){
//
//     }
//     let val = this.calcCurParam();
    return(
      <div>Parameter Info:
        <form className={"container"}>
        <table><tbody>
        <tr>
        <td width="50">
          <TextField
            label="Zone"
            id="zone"
            value={this.state.paramZone}
            onChange={this.onNumericChange}
          />
        </td>
        <td width="50">
          <TextField
            label="Chan"
            id="chan"
            value={this.state.paramChan}
            onChange={this.onNumericChange}
          />
        </td>
        <td width="50">
          <TextField
            label="ParamId"
            id="pid"
            value={this.state.paramPid}
            onChange={this.onNumericChange}
          />
        </td>
        <td width="100">
          <TextField
            label="Param"
            id="parm"
            value={this.state.paramParm}
            onChange={this.onNumericChange}
            onFocus={e=>{this.setState({focused: 1})}}
            onBlur={e=>{this.setState({focused: 0})}}
          />
        </td>
        </tr>
        </tbody></table>
        </form>
      </div>
    );

  }

  // showTableChannelType = (table)=>{
  //   return(
  //     <div>
  //       <div style={{margin: 30}}>
  //         <FormControl className={"formControl"}>
  //           <InputLabel htmlFor="unknown">Column</InputLabel>
  //           <Select
  //             value={this.state.column}
  //             onChange={this.onColumnChange}
  //             inputProps={{
  //               name: 'Column',
  //               id: 'column_select',
  //             }}
  //             >
  //             {this.initColumns(this.state.table)}
  //           </Select>
  //         </FormControl>
  //       </div>
  //
  //       <div style={{margin: 30}}>
  //         <FormControl className={"formControl"}>
  //           <InputLabel htmlFor="unknown">Channel</InputLabel>
  //           <Select
  //             value={this.state.channel}
  //             onChange={this.onChannelChange}
  //             inputProps={{
  //               name: 'Column',
  //               id: 'column_select',
  //             }}
  //             >
  //             {this.initColumns(0)}
  //           </Select>
  //         </FormControl>
  //       </div>
  //     </div>
  //   );
  // }


//   showTableUnitType = (table)=>{
// //          {this.showTableType(this.tableList[this.state.table].type)}
//
//     return(
//       <div>
//         <div style={{margin: 30}}>
//           <FormControl className={"formControl"}>
//             <InputLabel htmlFor="unknown">Column</InputLabel>
//             <Select
//               value={this.state.column}
//               onChange={this.onSelectChange}
//               inputProps={{
//                 name: 'column_select',
//               }}
//               >
//               {this.initColumns(this.state.table)}
//             </Select>
//           </FormControl>
//         </div>
//
//       </div>
//
//     );
//   }


  // showTableType = (table)=>{
  //   switch (table){
  //     case c.UNIT_TYPE:
  //       return this.showTableUnitType(table);
  //     default:
  //       break;
  //   }
  // }


  barClick = (e)=>{
    cl(e.currentTarget.id);
    switch (e.currentTarget.id){
      case "home":
        history.push("/sa")
        break;
      default:
        break;
    }
  }

  saveValue=()=>{
/*    t, c, i, d in packs. command, site, user, zone, params,
 */
    cl("save it");
    dbVals.z[this.state.paramZone][this.state.paramChan][this.state.paramPid] = this.state.paramParm;
    let packs = [
      {
        t: Math.floor(getTime()),
        z: this.state.paramZone*1,
        c: this.state.paramChan*1,
        i: this.state.paramPid*1,
        d: this.state.paramParm,
      }
    ];

//     let pack = {
//       site: globs.siteid,
//       command: "data01",
//       user: "1",
//       zone: this.state.paramZone,
//     }
    sendPacks(this.state.paramZone, packs, globs.siteid, dbVals.user);

    cl(packs);
  }

  saveTable=()=>{
    cl("save table")
    this.state.tableValues.forEach(a=>{
      let val = dbVals.z[a[0]][a[1]][a[2]];
      if (val === undefined) val = '---';
      if (val !== a[3]){
        cl(a);
        cl(dbVals.z[a[0]][a[1]][a[2]]);
      }
    });
  }

  refreshTable=()=>{
    // cl("refresh table")
    let state = Object.assign({}, this.state)
    state.zone = state.paramZone;
    // cl(state);

    this.initTableValues(state);
  }

  onClick=(e)=>{
    switch(e){
      case "save":
        return this.saveValue();
      case "saveTable":
        return this.saveTable();
      case "refreshTable":
        return this.refreshTable();
      default:
        break;
    }
  }

  render = ()=>{
    if(this.state.loaded){
      // cl(this.state.column);
      return(
        <div>
            <MainBar home settings
            menu click={this.barClick} title="Parameter Editor" />

          <div style={{margin: 30}}>
            <FormControl className={"formControl"}>
              <InputLabel htmlFor="unknown">Zone</InputLabel>
              <Select
                value={this.state.paramZone}
                onChange={this.onSelectChange}
                inputProps={{
                  name: 'zone_select',
                }}
                >
                {this.initZones()}
              </Select>
            </FormControl>
          </div>

          <div style={{margin: 30}}>
            <FormControl className={"formControl"}>
              <InputLabel htmlFor="unknown">Unit</InputLabel>
              <Select
                value={this.state.unit}
                onChange={this.onSelectChange}
                inputProps={{
                  name: 'unit_select',
                }}
                >
                {this.initUnits()}
              </Select>
            </FormControl>
          </div>

          <div style={{margin: 30}}>
            <FormControl className={"formControl"}>
              <InputLabel htmlFor="unknown">Channel</InputLabel>
              <Select
                value={this.state.channel}
                onChange={this.onSelectChange}
                inputProps={{
                  name: 'chan_select',
                }}
                >
                {this.initChannels()}
              </Select>
            </FormControl>
          </div>

          <div style={{margin: 30}}>
            <FormControl className={"formControl"}>
              <InputLabel htmlFor="unknown">Index</InputLabel>
              <Select
                value={this.state.index}
                onChange={this.onSelectChange}
                inputProps={{
                  name: 'index_select',
                }}
                >
                {this.initIndexes()}
              </Select>
            </FormControl>
          </div>

          <div style={{margin: 30}}>
            <FormControl className={"formControl"}>
              <InputLabel htmlFor="unknown">Sensor</InputLabel>
              <Select
                value={this.state.sensor}
                onChange={this.onSelectChange}
                inputProps={{
                  name: 'sensor_select',
                }}
                >
                {this.initSensors()}
              </Select>
            </FormControl>
          </div>

          <div style={{margin: 30}}>
            <FormControl className={"formControl"}>
              <InputLabel htmlFor="unknown">Table</InputLabel>
              <Select
                id="table"
                value={this.state.table}
                onChange={this.onSelectChange}
                inputProps={{
                  name: 'table_select',
                }}
                >
                {this.initTables()}
              </Select>
            </FormControl>
          </div>

          <div style={{margin: 30}}>
            <FormControl className={"formControl"}>
              <InputLabel htmlFor="unknown">Column</InputLabel>
              <Select
                value={this.state.column}
                onChange={this.onSelectChange}
                inputProps={{
                  name: 'column_select',
                }}
                >
                {this.initColumns(this.state.table)}
              </Select>
            </FormControl>
          </div>
          {this.showNumericFields()}
          <div>
          <Button onClick={()=>this.onClick("save")} style={{margin: 20}} variant="contained" color="primary" className={"button"}>
            Save
          </Button>
          </div>
          <div style={{width: 500, height: 300, overflowX: "auto"}}>
          {this.showColumns(this.state.table)}
          <table><tbody>
          <tr><td>
            <Button onClick={()=>this.onClick("saveTable")} style={{margin: 20}} variant="contained" color="primary" className={"button"}>
              Save
            </Button>
          </td>
          <td>
            <Button onClick={()=>this.onClick("refreshTable")} style={{margin: 20}} variant="contained" color="primary" className={"button"}>
              Refresh
            </Button>
          </td>
          </tr>
          </tbody></table>
          </div>




        </div>
      );

    } else{
      return(
        <div>
        <MainBar home settings
        menu click={this.barClick} title="Parameter Editor" />
        {this.state.loadMsg}
        </div>
      );

    }
  }
}


  export default Param ;
