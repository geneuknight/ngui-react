import { createMuiTheme } from '@material-ui/core/styles';

export default createMuiTheme({
  typography: {
    useNextVariants: true,
  },
 palette: {
      primary: {
        main: '#006060',
      },
      secondary:  {
        main: '#FFFF00',
      },
      error:  {// doesn't work
        main: '#FF00FF',
      },
 }
});
