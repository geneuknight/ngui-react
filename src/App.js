import React, { Component } from 'react';
// import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';
import {ThemeProvider as MuiThemeProvider} from '@material-ui/core/styles';
import theme from './theme';
import { Router, Route } from "react-router-dom";// , Link
// import logo from './logo.svg';
// import {getParamIds} from './components/utils/paramIds';
import Floater from './components/Floater';
import MainOne from './components/MainOne';
import Login from './components/temps/Login00'
import CreateUser from './components/temps/CreateUser00'
import SiteArrayParent from './components/SiteArrayParent'
import ZoneArray from './components/ZoneArray'
import UnitArray from './components/UnitArray'
import Param from './components/Param00'
import ParamTable from './components/ParamTable00'
import ConfigTest from './components/configs/ConfigTest';
import ZoneSettings from './components/ZoneSettings';
import Channel from './components/Channel';
import SelectFui from './fui/SelectFui';
import EditFui from './fui/EditFui';
import EditControlFui from './fui/EditControlFui';
import LiveFui from './fui/LiveFui';

// import Log from './components/Log';
import Log from './components/viewlog/ViewLog';
import history from "./history"
import './App.css';

class App extends Component {
  constructor(props){
    super(props);
    // getParamIds();
    // loadTextures();
    // loadTables().then(r=>{
    //   // cl("loaded");
    //   this.setState({loaded: true});
    // });
    this.state = {
      loaded: true,
    }
  }

  setupRouter(){
    return (
      <div className="App">
      <MuiThemeProvider theme={theme}>
      <Router history={history}>
        <div>
          <Route path="/ce" exact component={MainOne} />
          <Route path="/li" exact component={Login} />
          <Route path="/cu" exact component={CreateUser} />
          <Route path="/sa" exact component={SiteArrayParent} />
          <Route path="/za/:id" exact component={ZoneArray} />
          <Route path="/ua/:zid/:uid" exact component={UnitArray} />
          <Route path="/ch/:zuc" exact component={Channel} />

          <Route path="/log" exact component={Log} />
          <Route path="/params" exact component={Param} />
          <Route path="/paramtab" exact component={ParamTable} />
          <Route path="/zs/z/:zone/:page" exact component={ZoneSettings} />
          <Route path="/fui" exact component={SelectFui} />
          <Route path="/fui/:pageType" exact component={SelectFui} />
          <Route path="/fui/edit/:pageType" exact component={EditFui} />
          <Route path="/fui/controledit/:pageType/:controlId" exact component={EditControlFui} />
          <Route path="/fui/controledit/:pageType/" exact component={EditControlFui} />
          <Route path="/fui/live/:pageType/:zuci" exact component={LiveFui} />

          <Route path="/co/z/:zid/c/:cid" exact component={ConfigTest} />
        </div>
      </Router>
      </MuiThemeProvider>
      <Floater/>
      <Floater/>
      </div>
    )
  }

  loadingPage(){
    return(
      <div>loading</div>
    )
  }

  render() {
    // cl(getScreenSize());
// Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36
    if(this.state.loaded){
      return this.setupRouter();
    } else {
      return this.loadingPage();
    }
    // return (
    // );
  }
}

export default App;
